﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="MantenimientoBodega.aspx.vb" Inherits="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
    <style type="text/css">

        .style1
        {
            height: 18px;
        }
        .style2
        {
            height: 36px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <p>
<asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <table id="TableForm" align="center" cellpadding="2" cellspacing="0">
        <tr>
            <td class="Tema">
                Mantenimiento Bodega</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                <asp:HiddenField ID="hfBodega" runat="server" />
            </td>
            <td colspan="2">
                <asp:Button ID="btnAgregar" runat="server" CssClass="css3button" 
                    Text="Agregar" />
                <asp:Button ID="btnGuardar" runat="server" CssClass="css3button" Text="Guardar" 
                    ValidationGroup="1" />
                <asp:Button ID="btnModificar" runat="server" CssClass="css3button" 
                    Text="Modificar" />
                <asp:Button ID="btnCancelar" runat="server" CssClass="css3button" 
                    Text="Cancelar" />
                <asp:Button ID="btnBuscar" runat="server" CssClass="css3button" Text="Buscar" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="celdaForm" colspan="2" rowspan="3">
                <div class="redondesdas">
                    <table id="TableForm0" align="center" cellpadding="2" cellspacing="0">
                        <tr>
                            <td>
                                Codigo: 
                            </td>
                            <td>
                                <asp:TextBox ID="txtIdBodega" runat="server" ReadOnly="True"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Nombre de bodega:<asp:RequiredFieldValidator ID="RequiredFieldValidator1" 
                                    runat="server" ControlToValidate="txtNombre" 
                                    ErrorMessage="Favor ingresar nombre del estado" ValidationGroup="1" 
                                    ForeColor="Red">*</asp:RequiredFieldValidator>
                            </td>
                            <td>
                                <asp:TextBox ID="txtNombre" runat="server"></asp:TextBox>
                                &nbsp;<asp:Button ID="btnBuscarNombre" runat="server" CssClass="css3button" 
                                    Text="Buscar" />
                            </td>
                        </tr>

                        <tr>
                            <td class="style1">
                                Cantidad</td>
                            <td class="style1">
                                <asp:Label ID="lblCant" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
            <td class="style2">
                </td>
            <td class="style2">
                </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                </td>
            <td>
                </td>
        </tr>
        <tr>
            <td colspan="4" style="text-align: center; margin-left: 80px">
                * Campos requeridos.<asp:ValidationSummary ID="ValidationSummary1" 
                    runat="server" ShowMessageBox="True" ShowSummary="False" ValidationGroup="1" />
            </td>
        </tr>
        <tr>
            <td colspan="4" style="text-align: center; margin-left: 80px">
                <asp:GridView ID="dgDatos" runat="server" AllowPaging="True" 
                    AutoGenerateColumns="False" CssClass="mGrid">
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
    </table>
</p>
</asp:Content>

