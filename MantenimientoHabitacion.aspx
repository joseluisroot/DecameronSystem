﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="MantenimientoHabitacion.aspx.vb" Inherits="MantenimientoHabitacion" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <table id="TableForm" align="center" cellpadding="2" cellspacing="0">
        <tr>
            <td class="Tema">
                Mantenimiento Habitacion</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                <asp:HiddenField ID="hfHabitacion" runat="server" />
            </td>
            <td colspan="2">
                <asp:Button ID="btnAgregar" runat="server" CssClass="css3button" 
                    Text="Agregar" />
                <asp:Button ID="btnGuardar" runat="server" CssClass="css3button" Text="Guardar" 
                    ValidationGroup="1" />
                <asp:Button ID="btnModificar" runat="server" CssClass="css3button" 
                    Text="Modificar" />
                <asp:Button ID="btnCancelar" runat="server" CssClass="css3button" 
                    Text="Cancelar" />
                <asp:Button ID="btnDeschabilitar" runat="server" CssClass="css3button" 
                    Text="Deshabilitar" />
                <asp:Button ID="btnBuscar" runat="server" CssClass="css3button" Text="Buscar" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="celdaForm" colspan="2" rowspan="3">
                <div class="redondesdas">
                    <table id="TableForm0" align="center" cellpadding="2" cellspacing="0">
                        <tr>
                            <td>
                                Codigo: 
                            </td>
                            <td>
                                <asp:TextBox ID="txtIdHabitacion" runat="server" ReadOnly="True"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Tipo Habitacion:<asp:RequiredFieldValidator ID="RequiredFieldValidator2" 
                                    runat="server" ControlToValidate="ddlbTipoHabitacion" 
                                    ErrorMessage="Por Favor Seleccione Un Tipo de Habitacion" ForeColor="Red" 
                                    InitialValue="0" ValidationGroup="1">*</asp:RequiredFieldValidator>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlbTipoHabitacion" runat="server" ValidationGroup="1">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Nombre habitacion:<asp:RequiredFieldValidator ID="RequiredFieldValidator1" 
                                    runat="server" ControlToValidate="txtNombreHabitacion" 
                                    ErrorMessage="Favor Ingresar Nombre de Habitacion" ValidationGroup="1" 
                                    ForeColor="Red">*</asp:RequiredFieldValidator>
                            </td>
                            <td>
                                <asp:TextBox ID="txtNombreHabitacion" runat="server"></asp:TextBox>
                                &nbsp;<asp:Button ID="btnBuscarNombre" runat="server" CssClass="css3button" 
                                    Text="Buscar" />
                            </td>
                        </tr>

                        <tr>
                            <td>
                                Precio:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                                $<asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                                    ControlToValidate="txtPrecio" ErrorMessage="Favor Ingrese El Precio" 
                                    ForeColor="Red" ValidationGroup="1">*</asp:RequiredFieldValidator>
                            </td>
                            <td>
                                <asp:TextBox ID="txtPrecio" runat="server" ValidationGroup="1"></asp:TextBox>
                            </td>
                        </tr>
                       
                        <tr>
                            <td>
                                Descuento:&nbsp; %</td>
                            <td>
                                <asp:TextBox ID="txtDescuento" runat="server" ValidationGroup="1"></asp:TextBox>
                            </td>
                        </tr>
                       
                        <tr>
                            <td>
                                Cantidad</td>
                            <td>
                                <asp:Label ID="lblCant" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td colspan="4" style="text-align: center; margin-left: 80px">
                * Campos requeridos.<br />
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" 
                    ShowMessageBox="True" ShowSummary="False" ValidationGroup="1" />
            </td>
        </tr>
        <tr>
            <td colspan="4" style="text-align: center; margin-left: 80px">
                <asp:GridView ID="dgDatos" runat="server" AllowPaging="True" 
                    AutoGenerateColumns="False" CssClass="mGrid">
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
    </table>
</asp:Content>

