﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="MantenimientoPlan.aspx.vb" Inherits="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <p>
        <br />
    <table id="TableForm" align="center" cellpadding="2" cellspacing="0">
        <tr>
            <td class="Tema">
                Mantenimiento Plan</td>
            <td class="style21">
                &nbsp;</td>
            <td class="style19">
                &nbsp;</td>
            <td class="style8">
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td class="style21">
                &nbsp;</td>
            <td colspan="2">
                <asp:Button ID="btnAgregar" runat="server" CssClass="css3button" 
                    Text="Agregar" />
                <asp:Button ID="btnGuardar" runat="server" CssClass="css3button" Text="Guardar" 
                    ValidationGroup="1" />
                <asp:Button ID="btnModificar" runat="server" CssClass="css3button" 
                    Text="Modificar" />
                <asp:Button ID="btnCancelar" runat="server" CssClass="css3button" 
                    Text="Cancelar" />
                <asp:Button ID="btnDeschabilitar" runat="server" CssClass="css3button" 
                    Text="Deshabilitar" />
                <asp:Button ID="btnBuscar" runat="server" CssClass="css3button" Text="Buscar" 
                    Width="61px" />
            </td>
        </tr>
        <tr>
            <td class="style17">
                </td>
            <td class="style22">
                </td>
            <td class="style20">
                </td>
            <td class="style18">
                &nbsp;</td>
        </tr>
        <tr>
            <td class="celdaForm" colspan="2" rowspan="3">
                <div class="redondesdas">
                    <table id="TableForm0" align="center" cellpadding="2" cellspacing="0">
                        <tr>
                            <td class="style25">
                                Codigo: 
                            </td>
                            <td class="style36">
                                <asp:TextBox ID="txtIdPlan" runat="server" ReadOnly="True"></asp:TextBox>
                            </td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td class="style25">
                                Nombre plan:</td>
                            <td class="style36">
                                <asp:TextBox ID="txtNombre_plan" runat="server"></asp:TextBox>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                <asp:Button ID="btnBuscarNombre" runat="server" CssClass="css3button" 
                                    Text="Buscar" />
                            </td>
                        </tr>

                        <tr>
                            <td class="style23">
                                Valor:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                                $</td>
                            <td class="style24">
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
                                    ControlToValidate="txtValor_plan" ErrorMessage="Solo numeros" 
                                    ValidationExpression="\d*\.?\d*"></asp:RegularExpressionValidator>
                                <asp:TextBox ID="txtValor_plan" runat="server" Width="119px" 
                                    AutoPostBack="True"></asp:TextBox>
                            </td>
                            <td class="style33">
                                &nbsp;</td>
                            <td class="style33">
                                &nbsp;</td>
                        </tr>
                       
                        <tr>
                            <td class="style26">
                                </td>
                            <td class="style37">
                                &nbsp;</td>
                            <td class="style30">
                                &nbsp;</td>
                            <td class="style30">
                                &nbsp;</td>
                        </tr>
                       
                        <tr>
                            <td class="style25">
                                &nbsp;</td>
                            <td class="style36">
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                       
                        <tr>
                            <td class="style28">
                                &nbsp;</td>
                            <td class="style38">
                                </td>
                            <td class="style31">
                                &nbsp;</td>
                            <td class="style31">
                                &nbsp;</td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
        <tr>
            <td class="style11">
                </td>
            <td class="style12">
                    <table id="TableForm1" align="center" cellpadding="2" cellspacing="0">
                        <tr>
                            <td class="style31">
                                Tipo habitación:</td>
                            <td class="style29">
                                <asp:DropDownList ID="dtlTipoHabitacion" runat="server" Height="36px" 
                                    Width="134px" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Valor descuento:</td>
                            <td class="style16">
                                <asp:TextBox ID="txtValor_descuento" runat="server" Width="126px"></asp:TextBox>
                                &nbsp;</td>
                        </tr>

                        <tr>
                            <td>
                                Valor neto:</td>
                            <td class="style16">
                                <asp:TextBox ID="txtValor_real" runat="server" Width="128px"></asp:TextBox>
                            </td>
                        </tr>
                       
                        <tr>
                            <td>
                                Monto IVA:</td>
                            <td class="style16">
                                <asp:TextBox ID="txtmonto_iva" runat="server" Width="128px"></asp:TextBox>
                            </td>
                        </tr>
                       
                        <tr>
                            <td class="style30">
                                Valor total plan:</td>
                            <td class="style27">
                                <asp:TextBox ID="txtValor_total" runat="server" Width="128px"></asp:TextBox>
                            </td>
                        </tr>
                       
                        </table>
                </td>
        </tr>
        <tr>
            <td class="style19">
                &nbsp;</td>
            <td class="style8">
                &nbsp;</td>
        </tr>
        <tr>
            <td colspan="4" style="text-align: center; margin-left: 80px">
                * Campos requeridos.</td>
        </tr>
        <tr>
            <td colspan="4" style="text-align: center; margin-left: 80px">
                <asp:GridView ID="dgvPlanes" runat="server" AllowPaging="True" 
                    AutoGenerateColumns="False" CssClass="mGrid">
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td class="style21">
                &nbsp;</td>
            <td class="style19">
                &nbsp;</td>
            <td class="style8">
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td class="style21">
                &nbsp;</td>
            <td class="style19">
                &nbsp;</td>
            <td class="style8">
                &nbsp;</td>
        </tr>
    </table>
    </p>
    <p>
    </p>
</asp:Content>

<asp:Content ID="Content2" runat="server" contentplaceholderid="HeadContent">
    <style type="text/css">
        .style8
        {
            width: 447px;
        }
    .style11
    {
        height: 22px;
        width: 14px;
    }
    .style12
    {
        width: 447px;
        height: 22px;
    }
    .style16
    {
        width: 140px;
    }
    .style17
    {
        height: 8px;
    }
    .style18
    {
        width: 447px;
        height: 8px;
    }
    .style19
    {
        width: 14px;
    }
    .style20
    {
        height: 8px;
        width: 14px;
    }
    .style21
    {
        width: 27px;
    }
    .style22
    {
        height: 8px;
        width: 27px;
    }
    .style23
    {
        height: 26px;
        width: 84px;
    }
    .style24
    {
        width: 133px;
        height: 26px;
    }
    .style25
    {
        width: 84px;
    }
    .style26
    {
        width: 84px;
        height: 22px;
    }
    .style27
    {
        width: 140px;
        height: 22px;
    }
    .style28
    {
        height: 30px;
        width: 84px;
    }
    .style29
    {
        width: 140px;
        height: 30px;
    }
    .style30
    {
        height: 22px;
    }
        .style31
        {
            height: 30px;
        }
        .style33
        {
            height: 26px;
        }
        .style36
        {
            width: 133px;
        }
        .style37
        {
            width: 133px;
            height: 22px;
        }
        .style38
        {
            width: 133px;
            height: 30px;
        }
        .css3button
        {}
    </style>
</asp:Content>


