﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="Login.aspx.vb" Inherits="Login" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table align="center" style="width: 300px">
        <tr>
            <td>
                Usuario:<asp:RequiredFieldValidator 
                    ID="RequiredFieldValidator1" runat="server" 
                    ControlToValidate="txtUsuario" ErrorMessage="Ingrese Usuario" 
                    ForeColor="Red" ValidationGroup="1">*</asp:RequiredFieldValidator>
            </td>
            <td>
                <asp:TextBox ID="txtUsuario" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                Clave:<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                    ControlToValidate="txtClave" ErrorMessage="Falta ingresar Clave" 
                    ForeColor="Red" ValidationGroup="1">*</asp:RequiredFieldValidator>
            </td>
            <td>
                <asp:TextBox ID="txtClave" runat="server" TextMode="Password"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ForeColor="Red" 
                    ValidationGroup="1" />
            </td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: center">
                <asp:Button ID="btnIngresar" runat="server" Text="Ingresar" 
                    ValidationGroup="1" />
            </td>
        </tr>
    </table>
</asp:Content>

