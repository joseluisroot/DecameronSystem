﻿
Partial Class Principal
    Inherits cBase

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        finSession()

        If Not IsPostBack Then

            Me.lblUsuario.Text = Session("NombreUsuario")

        End If

    End Sub

    Protected Sub lknSalir_Click(sender As Object, e As System.EventArgs) Handles lknSalir.Click

        Me.CerrarSesion()

        Response.Redirect("index.aspx")


    End Sub
End Class
