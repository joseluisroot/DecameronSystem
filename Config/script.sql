USE [Decameron]
GO
/****** Object:  Table [dbo].[TipoHabitacion]    Script Date: 11/13/2014 21:53:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TipoHabitacion](
	[IdTipoHabitacion] [int] NOT NULL,
	[NombreHabitacion] [varchar](150) NULL,
	[idEstado] [int] NULL,
 CONSTRAINT [PK_TipoHabitacion] PRIMARY KEY CLUSTERED 
(
	[IdTipoHabitacion] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Estado]    Script Date: 11/13/2014 21:53:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Estado](
	[IdEstado] [int] NOT NULL,
	[NombreEstado] [varchar](150) NULL,
 CONSTRAINT [PK_Estados] PRIMARY KEY CLUSTERED 
(
	[IdEstado] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Bodega]    Script Date: 11/13/2014 21:53:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Bodega](
	[IdBodega] [int] IDENTITY(1,1) NOT NULL,
	[NombreBodega] [varchar](50) NULL,
 CONSTRAINT [PK_Bodega] PRIMARY KEY CLUSTERED 
(
	[IdBodega] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Cliente]    Script Date: 11/13/2014 21:53:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Cliente](
	[IdCliente] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](250) NULL,
	[Apellidos] [varchar](250) NULL,
	[Sexo] [char](1) NULL,
	[FechaIngreso] [datetime] NULL,
	[FechaNacimiento] [date] NULL,
	[IdEstado] [int] NULL,
 CONSTRAINT [PK_Cliente] PRIMARY KEY CLUSTERED 
(
	[IdCliente] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Perfil]    Script Date: 11/13/2014 21:53:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Perfil](
	[IdPerfil] [int] IDENTITY(1,1) NOT NULL,
	[NombrePerfil] [varchar](50) NULL,
 CONSTRAINT [PK_Perfil] PRIMARY KEY CLUSTERED 
(
	[IdPerfil] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Mobiliario]    Script Date: 11/13/2014 21:53:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Mobiliario](
	[IdMobiliario] [int] NOT NULL,
	[Nombre] [int] NULL,
 CONSTRAINT [PK_Mobiliario] PRIMARY KEY CLUSTERED 
(
	[IdMobiliario] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Insumos]    Script Date: 11/13/2014 21:53:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Insumos](
	[IdInsumo] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [nchar](10) NULL,
 CONSTRAINT [PK_Insumos] PRIMARY KEY CLUSTERED 
(
	[IdInsumo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Habitacion]    Script Date: 11/13/2014 21:53:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Habitacion](
	[IdHabitacion] [int] IDENTITY(1,1) NOT NULL,
	[NombreHabitacion] [varchar](250) NULL,
	[Precio] [float] NULL,
	[IdTipoHabitacion] [int] NULL,
 CONSTRAINT [PK_Habitacion] PRIMARY KEY CLUSTERED 
(
	[IdHabitacion] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Checkin]    Script Date: 11/13/2014 21:53:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Checkin](
	[IdCliente] [int] NULL,
	[IdChekin] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_Checkin] PRIMARY KEY CLUSTERED 
(
	[IdChekin] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DetalleInsumosPorBodega]    Script Date: 11/13/2014 21:53:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DetalleInsumosPorBodega](
	[IdDetalleBodega] [int] NULL,
	[IdBodega] [int] NULL,
	[idInsumo] [int] NULL,
	[Cant] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Reserva]    Script Date: 11/13/2014 21:53:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Reserva](
	[idReserva] [int] IDENTITY(1,1) NOT NULL,
	[IdCliente] [int] NULL,
 CONSTRAINT [PK_Reserva] PRIMARY KEY CLUSTERED 
(
	[idReserva] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DetalleMobiliarioPorBodega]    Script Date: 11/13/2014 21:53:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DetalleMobiliarioPorBodega](
	[IdDetalleBodega] [int] NOT NULL,
	[IdBodega] [int] NOT NULL,
	[IdMobiliario] [int] NOT NULL,
	[Cant] [int] NOT NULL,
 CONSTRAINT [PK_DetalleMobiliarioPorBodega_1] PRIMARY KEY CLUSTERED 
(
	[IdDetalleBodega] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Empleado]    Script Date: 11/13/2014 21:53:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Empleado](
	[IdEmpleado] [int] NULL,
	[Nombre] [varchar](50) NULL,
	[Apellido] [varchar](50) NULL,
	[Usuario] [varchar](50) NULL,
	[Clave] [varchar](50) NULL,
	[IdEstado] [varchar](50) NULL,
	[IdPerfil] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ServiciosPorCliente]    Script Date: 11/13/2014 21:53:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ServiciosPorCliente](
	[IdServicioCliente] [int] NOT NULL,
	[IdCliente] [int] NULL,
	[IdServicio] [int] NULL,
 CONSTRAINT [PK_ServiciosPorCliente] PRIMARY KEY CLUSTERED 
(
	[IdServicioCliente] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Servicios]    Script Date: 11/13/2014 21:53:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Servicios](
	[IdServicio] [int] NULL,
	[Nombre] [varchar](250) NULL,
	[precio] [float] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DetalleReserva]    Script Date: 11/13/2014 21:53:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DetalleReserva](
	[IdDetalleReserva] [int] NULL,
	[IdHabitacion] [int] NULL,
	[IdReserva] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DetalleMobiliario]    Script Date: 11/13/2014 21:53:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DetalleMobiliario](
	[IdDetalleMobiliario] [int] IDENTITY(1,1) NOT NULL,
	[idHabitacion] [int] NULL,
	[IdMobiliario] [int] NULL,
	[cant] [int] NULL,
	[fechaAsignacion] [datetime] NULL,
 CONSTRAINT [PK_DetalleMobiliario] PRIMARY KEY CLUSTERED 
(
	[IdDetalleMobiliario] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DetalleInsumos]    Script Date: 11/13/2014 21:53:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DetalleInsumos](
	[IdDetalleInsumos] [int] NOT NULL,
	[IdHabitacion] [int] NULL,
	[IdInsumo] [int] NULL,
	[Cant] [int] NULL,
	[fechaAsignacion] [datetime] NULL,
 CONSTRAINT [PK_DetalleInsumos] PRIMARY KEY CLUSTERED 
(
	[IdDetalleInsumos] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DetalleCheckin]    Script Date: 11/13/2014 21:53:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DetalleCheckin](
	[IdDetalleCheckin] [int] NOT NULL,
	[IdCheckin] [int] NULL,
 CONSTRAINT [PK_DetalleCheckin_1] PRIMARY KEY CLUSTERED 
(
	[IdDetalleCheckin] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  ForeignKey [FK_Habitacion_TipoHabitacion]    Script Date: 11/13/2014 21:53:20 ******/
ALTER TABLE [dbo].[Habitacion]  WITH CHECK ADD  CONSTRAINT [FK_Habitacion_TipoHabitacion] FOREIGN KEY([IdTipoHabitacion])
REFERENCES [dbo].[TipoHabitacion] ([IdTipoHabitacion])
GO
ALTER TABLE [dbo].[Habitacion] CHECK CONSTRAINT [FK_Habitacion_TipoHabitacion]
GO
/****** Object:  ForeignKey [FK_Checkin_Cliente]    Script Date: 11/13/2014 21:53:20 ******/
ALTER TABLE [dbo].[Checkin]  WITH CHECK ADD  CONSTRAINT [FK_Checkin_Cliente] FOREIGN KEY([IdCliente])
REFERENCES [dbo].[Cliente] ([IdCliente])
GO
ALTER TABLE [dbo].[Checkin] CHECK CONSTRAINT [FK_Checkin_Cliente]
GO
/****** Object:  ForeignKey [FK_DetalleBodega_Bodega]    Script Date: 11/13/2014 21:53:20 ******/
ALTER TABLE [dbo].[DetalleInsumosPorBodega]  WITH CHECK ADD  CONSTRAINT [FK_DetalleBodega_Bodega] FOREIGN KEY([IdBodega])
REFERENCES [dbo].[Bodega] ([IdBodega])
GO
ALTER TABLE [dbo].[DetalleInsumosPorBodega] CHECK CONSTRAINT [FK_DetalleBodega_Bodega]
GO
/****** Object:  ForeignKey [FK_DetalleInsumosPorBodega_Insumos]    Script Date: 11/13/2014 21:53:20 ******/
ALTER TABLE [dbo].[DetalleInsumosPorBodega]  WITH CHECK ADD  CONSTRAINT [FK_DetalleInsumosPorBodega_Insumos] FOREIGN KEY([idInsumo])
REFERENCES [dbo].[Insumos] ([IdInsumo])
GO
ALTER TABLE [dbo].[DetalleInsumosPorBodega] CHECK CONSTRAINT [FK_DetalleInsumosPorBodega_Insumos]
GO
/****** Object:  ForeignKey [FK_Reserva_Cliente]    Script Date: 11/13/2014 21:53:20 ******/
ALTER TABLE [dbo].[Reserva]  WITH CHECK ADD  CONSTRAINT [FK_Reserva_Cliente] FOREIGN KEY([IdCliente])
REFERENCES [dbo].[Cliente] ([IdCliente])
GO
ALTER TABLE [dbo].[Reserva] CHECK CONSTRAINT [FK_Reserva_Cliente]
GO
/****** Object:  ForeignKey [FK_DetalleMobiliarioPorBodega_Bodega]    Script Date: 11/13/2014 21:53:20 ******/
ALTER TABLE [dbo].[DetalleMobiliarioPorBodega]  WITH CHECK ADD  CONSTRAINT [FK_DetalleMobiliarioPorBodega_Bodega] FOREIGN KEY([IdBodega])
REFERENCES [dbo].[Bodega] ([IdBodega])
GO
ALTER TABLE [dbo].[DetalleMobiliarioPorBodega] CHECK CONSTRAINT [FK_DetalleMobiliarioPorBodega_Bodega]
GO
/****** Object:  ForeignKey [FK_DetalleMobiliarioPorBodega_Mobiliario]    Script Date: 11/13/2014 21:53:20 ******/
ALTER TABLE [dbo].[DetalleMobiliarioPorBodega]  WITH CHECK ADD  CONSTRAINT [FK_DetalleMobiliarioPorBodega_Mobiliario] FOREIGN KEY([IdMobiliario])
REFERENCES [dbo].[Mobiliario] ([IdMobiliario])
GO
ALTER TABLE [dbo].[DetalleMobiliarioPorBodega] CHECK CONSTRAINT [FK_DetalleMobiliarioPorBodega_Mobiliario]
GO
/****** Object:  ForeignKey [FK_Empleado_Perfil]    Script Date: 11/13/2014 21:53:20 ******/
ALTER TABLE [dbo].[Empleado]  WITH CHECK ADD  CONSTRAINT [FK_Empleado_Perfil] FOREIGN KEY([IdPerfil])
REFERENCES [dbo].[Perfil] ([IdPerfil])
GO
ALTER TABLE [dbo].[Empleado] CHECK CONSTRAINT [FK_Empleado_Perfil]
GO
/****** Object:  ForeignKey [FK_ServiciosPorCliente_Cliente]    Script Date: 11/13/2014 21:53:20 ******/
ALTER TABLE [dbo].[ServiciosPorCliente]  WITH CHECK ADD  CONSTRAINT [FK_ServiciosPorCliente_Cliente] FOREIGN KEY([IdCliente])
REFERENCES [dbo].[Cliente] ([IdCliente])
GO
ALTER TABLE [dbo].[ServiciosPorCliente] CHECK CONSTRAINT [FK_ServiciosPorCliente_Cliente]
GO
/****** Object:  ForeignKey [FK_Servicios_ServiciosPorCliente]    Script Date: 11/13/2014 21:53:20 ******/
ALTER TABLE [dbo].[Servicios]  WITH CHECK ADD  CONSTRAINT [FK_Servicios_ServiciosPorCliente] FOREIGN KEY([IdServicio])
REFERENCES [dbo].[ServiciosPorCliente] ([IdServicioCliente])
GO
ALTER TABLE [dbo].[Servicios] CHECK CONSTRAINT [FK_Servicios_ServiciosPorCliente]
GO
/****** Object:  ForeignKey [FK_DetalleReserva_Habitacion]    Script Date: 11/13/2014 21:53:20 ******/
ALTER TABLE [dbo].[DetalleReserva]  WITH CHECK ADD  CONSTRAINT [FK_DetalleReserva_Habitacion] FOREIGN KEY([IdHabitacion])
REFERENCES [dbo].[Habitacion] ([IdHabitacion])
GO
ALTER TABLE [dbo].[DetalleReserva] CHECK CONSTRAINT [FK_DetalleReserva_Habitacion]
GO
/****** Object:  ForeignKey [FK_DetalleReserva_Reserva]    Script Date: 11/13/2014 21:53:20 ******/
ALTER TABLE [dbo].[DetalleReserva]  WITH CHECK ADD  CONSTRAINT [FK_DetalleReserva_Reserva] FOREIGN KEY([IdReserva])
REFERENCES [dbo].[Reserva] ([idReserva])
GO
ALTER TABLE [dbo].[DetalleReserva] CHECK CONSTRAINT [FK_DetalleReserva_Reserva]
GO
/****** Object:  ForeignKey [FK_DetalleMobiliario_Habitacion]    Script Date: 11/13/2014 21:53:20 ******/
ALTER TABLE [dbo].[DetalleMobiliario]  WITH CHECK ADD  CONSTRAINT [FK_DetalleMobiliario_Habitacion] FOREIGN KEY([idHabitacion])
REFERENCES [dbo].[Habitacion] ([IdHabitacion])
GO
ALTER TABLE [dbo].[DetalleMobiliario] CHECK CONSTRAINT [FK_DetalleMobiliario_Habitacion]
GO
/****** Object:  ForeignKey [FK_DetalleMobiliario_Mobiliario]    Script Date: 11/13/2014 21:53:20 ******/
ALTER TABLE [dbo].[DetalleMobiliario]  WITH CHECK ADD  CONSTRAINT [FK_DetalleMobiliario_Mobiliario] FOREIGN KEY([IdMobiliario])
REFERENCES [dbo].[Mobiliario] ([IdMobiliario])
GO
ALTER TABLE [dbo].[DetalleMobiliario] CHECK CONSTRAINT [FK_DetalleMobiliario_Mobiliario]
GO
/****** Object:  ForeignKey [FK_DetalleInsumos_Habitacion]    Script Date: 11/13/2014 21:53:20 ******/
ALTER TABLE [dbo].[DetalleInsumos]  WITH CHECK ADD  CONSTRAINT [FK_DetalleInsumos_Habitacion] FOREIGN KEY([IdHabitacion])
REFERENCES [dbo].[Habitacion] ([IdHabitacion])
GO
ALTER TABLE [dbo].[DetalleInsumos] CHECK CONSTRAINT [FK_DetalleInsumos_Habitacion]
GO
/****** Object:  ForeignKey [FK_DetalleInsumos_Insumos]    Script Date: 11/13/2014 21:53:20 ******/
ALTER TABLE [dbo].[DetalleInsumos]  WITH CHECK ADD  CONSTRAINT [FK_DetalleInsumos_Insumos] FOREIGN KEY([IdInsumo])
REFERENCES [dbo].[Insumos] ([IdInsumo])
GO
ALTER TABLE [dbo].[DetalleInsumos] CHECK CONSTRAINT [FK_DetalleInsumos_Insumos]
GO
/****** Object:  ForeignKey [FK_DetalleCheckin_Checkin]    Script Date: 11/13/2014 21:53:20 ******/
ALTER TABLE [dbo].[DetalleCheckin]  WITH CHECK ADD  CONSTRAINT [FK_DetalleCheckin_Checkin] FOREIGN KEY([IdCheckin])
REFERENCES [dbo].[Checkin] ([IdChekin])
GO
ALTER TABLE [dbo].[DetalleCheckin] CHECK CONSTRAINT [FK_DetalleCheckin_Checkin]
GO
