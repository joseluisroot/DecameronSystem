﻿Imports System.Data

Partial Class _MantenimientoEstado
    Inherits cBase

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        Me.finSession()

        If Me.IsPostBack = False Then

            'CargarDatos

            Dim dsDatos As New DataSet
            Dim cControl As New dbEstado

            dsDatos = cControl.RecuperarEstados

            If dsDatos.Tables(0).Rows.Count > 0 Then
                LlenarGrid(dsDatos)
            Else
                Me.lblCant.Text = dsDatos.Tables(0).Rows.Count & " Estado"
            End If

            Iniciar()
            DeshabilitarForm()

        End If


    End Sub

    Sub reiniciar()

        Me.btnCancelar.Visible = False
        Me.btnDeschabilitar.Visible = False
        Me.btnModificar.Visible = False
        Me.btnGuardar.Visible = False


    End Sub


    Sub LlenarGrid(ByVal Datos As DataSet)

        If Datos.Tables(0).Rows.Count > 0 Then

            Me.dgDatos.Columns.Clear()
            Me.dgDatos.DataSource = Datos.Tables(0)

            Dim btnSel As New CommandField
            btnSel.ShowSelectButton = True
            btnSel.SelectText = "Seleccionar"
            Me.dgDatos.Columns.Add(btnSel)

            Dim IdEstado As New BoundField
            IdEstado.DataField = "IdEstado"
            IdEstado.HeaderText = "Codigo de Estado"
            Me.dgDatos.Columns.Add(IdEstado)

            Dim NombreEstado As New BoundField
            NombreEstado.DataField = "NombreEstado"
            NombreEstado.HeaderText = "Nombre de Estado"
            Me.dgDatos.Columns.Add(NombreEstado)

            Me.dgDatos.DataBind()
            Me.lblCant.Text = Datos.Tables(0).Rows.Count & " Estado"
        End If

    End Sub

    Sub Iniciar()

        Me.btnGuardar.Visible = False
        Me.btnCancelar.Visible = False
        Me.btnModificar.Visible = False
        Me.btnDeschabilitar.Visible = False
        Me.btnBuscarNombre.Visible = False

    End Sub


    Sub HabilitaForm()

        Me.txtIdEstado.ReadOnly = True
        Me.txtNombre.Enabled = True


    End Sub

    Sub DeshabilitarForm()

        Me.txtIdEstado.ReadOnly = True
        Me.txtNombre.Enabled = False

    End Sub

    Sub LimpiarForm()

        Me.txtIdEstado.Text = ""
        Me.txtNombre.Text = ""

    End Sub

    Protected Sub btnAgregar_Click(sender As Object, e As System.EventArgs) Handles btnAgregar.Click
        Me.Session.Item("Accion") = "Agregar"

        Me.btnAgregar.Visible = False
        Me.btnGuardar.Visible = True
        Me.btnCancelar.Visible = True

        Me.btnBuscar.Visible = False

        HabilitaForm()
    End Sub

    Protected Sub btnCancelar_Click(sender As Object, e As System.EventArgs) Handles btnCancelar.Click

        Select Case Me.Session.Item("Accion")
            Case "Buscar"

                Me.btnAgregar.Visible = True
                Me.btnBuscar.Visible = True

                Me.btnBuscarNombre.Visible = False
                Me.btnCancelar.Visible = False

                Me.txtNombre.Enabled = False

                Dim cControl As New dbEstado
                Me.LlenarGrid(cControl.RecuperarEstados)

            Case "Modificar"


                Me.btnModificar.Visible = False
                Me.btnCancelar.Visible = False

                Me.btnGuardar.Visible = False
                Me.btnBuscarNombre.Visible = False


                Me.btnAgregar.Visible = True
                Me.btnBuscar.Visible = True

                Me.LimpiarForm()
                Me.DeshabilitarForm()

                Dim cControl As New dbEstado
                Me.LlenarGrid(cControl.RecuperarEstados)

            Case "Agregar"

                Me.DeshabilitarForm()

                Me.btnAgregar.Visible = True
                Me.btnBuscar.Visible = True

                Me.btnGuardar.Visible = False
                Me.btnCancelar.Visible = False

            Case "Seleccion"

                Me.btnModificar.Visible = False
                Me.btnCancelar.Visible = False
                Me.btnBuscarNombre.Visible = False

                Me.btnAgregar.Visible = True
                Me.btnBuscar.Visible = True

                Dim cControl As New dbEstado
                Me.LlenarGrid(cControl.RecuperarEstados)

                Me.LimpiarForm()
                Me.DeshabilitarForm()

        End Select

    End Sub

    Protected Sub btnModificar_Click(sender As Object, e As System.EventArgs) Handles btnModificar.Click
        Me.Session.Item("Accion") = "Modificar"

        Me.HabilitaForm()

        Me.txtIdEstado.ReadOnly = True

        Me.btnGuardar.Visible = True
        Me.btnCancelar.Visible = True

        Me.btnModificar.Visible = False
        Me.btnBuscarNombre.Visible = False
    End Sub

    Protected Sub btnBuscar_Click(sender As Object, e As System.EventArgs) Handles btnBuscar.Click

        Me.Session.Item("Accion") = "Buscar"

        Me.btnBuscarNombre.Visible = True
        Me.txtNombre.Enabled = True
        Me.btnCancelar.Visible = True

        Me.btnBuscar.Visible = False
        Me.btnAgregar.Visible = False

    End Sub

    Protected Sub btnBuscarNombre_Click(sender As Object, e As System.EventArgs) Handles btnBuscarNombre.Click

        Dim dsDatos As New DataSet
        Dim cControl As New dbEstado

        dsDatos = cControl.BuscarEstadoPorNombre(Me.txtNombre.Text)

        If dsDatos.Tables(0).Rows.Count > 0 Then

            Me.LlenarGrid(dsDatos)
            Me.lblCant.Text = dsDatos.Tables(0).Rows.Count & " estado"
        Else

            Me.dgDatos.EmptyDataText = "Datos no encontrado con esos parametros"
            Me.dgDatos.Columns.Clear()
            Me.dgDatos.DataSource = Nothing
            Me.dgDatos.DataBind()

            'Mensaje

        End If

        Me.LimpiarForm()
        Me.btnModificar.Visible = False

    End Sub

    Protected Sub btnGuardar_Click(sender As Object, e As System.EventArgs) Handles btnGuardar.Click

        Dim lEntidad As New Estado
        Dim cControl As New dbEstado
        Dim dsDatos As New DataSet

        If Me.Session.Item("Accion") = "Agregar" Then
            If String.IsNullOrEmpty(Me.hfEstado.Value) = True Then

                lEntidad.NombreEstado = Me.txtNombre.Text

                If cControl.GuardarEstado(lEntidad) > 0 Then

                    DeshabilitarForm()
                    Iniciar()

                    LlenarGrid(cControl.RecuperarEstados)

                    Me.LimpiarForm()
                    Me.Session.Item("Accion") = ""

                    Me.btnAgregar.Visible = True
                    Me.btnBuscar.Visible = True


                Else



                End If

            End If

        ElseIf Me.Session.Item("Accion") = "Modificar" Then

            lEntidad.IdEstado = Me.txtIdEstado.Text
            lEntidad.NombreEstado = Me.txtNombre.Text

            If cControl.ModificarEstado(lEntidad) > 0 Then


                DeshabilitarForm()
                Iniciar()

                LlenarGrid(cControl.RecuperarEstados)

                Me.LimpiarForm()
                Me.Session.Item("Accion") = ""

                Me.btnAgregar.Visible = True
                Me.btnBuscar.Visible = True



            Else



            End If

        End If

    End Sub

   
    Protected Sub dgDatos_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles dgDatos.SelectedIndexChanged

        Dim dsDatos As New DataSet
        Dim cControl As New dbEstado

        Me.Session.Item("Accion") = "Seleccion"

        dsDatos = cControl.RecuperarEstadoPorId(Me.dgDatos.SelectedRow.Cells(1).Text)

        If dsDatos.Tables(0).Rows.Count > 0 Then

            Me.LlenarGrid(dsDatos)

            'llenar Datos para Modificar
            Me.txtIdEstado.Text = dsDatos.Tables(0).Rows(0).Item("IdEstado").ToString
            Me.txtNombre.Text = dsDatos.Tables(0).Rows(0).Item("NombreEstado").ToString

            Me.lblCant.Text = dsDatos.Tables(0).Rows.Count & " estado"

            Me.btnAgregar.Visible = False
            Me.btnBuscar.Visible = False
            Me.btnGuardar.Visible = False

            Me.btnModificar.Visible = True
            Me.btnCancelar.Visible = True

            Me.btnBuscarNombre.Visible = False

        End If

        Me.DeshabilitarForm()

    End Sub
End Class
