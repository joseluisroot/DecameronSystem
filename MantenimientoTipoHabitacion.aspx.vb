﻿Imports System.Data

Partial Class MantenimientoTipoHabitacion
    Inherits cBase
    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        Me.finSession()

        If Me.IsPostBack = False Then

            'CargarDatos

            Dim dsDatos As New DataSet
            Dim cControl As New dbTipoHabitacion


            dsDatos = cControl.RecuperarTipoHabitacion


            If dsDatos.Tables(0).Rows.Count > 0 Then
                LlenarGrid(dsDatos)
            Else
                Me.lblCant.Text = dsDatos.Tables(0).Rows.Count & "Tipo Habitacion"
            End If

            Iniciar()
            DeshabilitarForm()
            llenarEstado()

        End If


    End Sub

    Sub llenarEstado()


        Dim cControl As New dbEstado
        Dim dsDatos As New DataSet

        dsDatos = cControl.RecuperarEstados()

        If dsDatos.Tables(0).Rows.Count > 0 Then

            Me.ddlbEstado.DataSource = dsDatos.Tables(0)
            Me.ddlbEstado.DataTextField = "NombreEstado"
            Me.ddlbEstado.DataValueField = "IdEstado"
            Me.ddlbEstado.DataBind()
        End If
        Me.ddlbEstado.Items.Insert("0", New ListItem("Seleccionar...", "0"))



    End Sub


    Sub reiniciar()

        Me.btnCancelar.Visible = False
        Me.btnDeschabilitar.Visible = False
        Me.btnModificar.Visible = False
        Me.btnGuardar.Visible = False


    End Sub


    Sub LlenarGrid(ByVal Datos As DataSet)

        If Datos.Tables(0).Rows.Count > 0 Then

            Me.dgDatos.Columns.Clear()
            Me.dgDatos.DataSource = Datos.Tables(0)

            Dim btnSel As New CommandField
            btnSel.ShowSelectButton = True
            btnSel.SelectText = "Seleccionar"
            Me.dgDatos.Columns.Add(btnSel)

            Dim IdTipoHabitacion As New BoundField
            IdTipoHabitacion.DataField = "IdTipoHabitacion"
            IdTipoHabitacion.HeaderText = "Codigo de Tipo Habitacion"
            Me.dgDatos.Columns.Add(IdTipoHabitacion)

            Dim NombreHabitacion As New BoundField
            NombreHabitacion.DataField = "NombreHabitacion"
            NombreHabitacion.HeaderText = "Nombre de Tipo Habitacion"
            Me.dgDatos.Columns.Add(NombreHabitacion)

            Dim Porcentaje_descuento As New BoundField
            Porcentaje_descuento.DataField = "porcentaje_descuento"
            Porcentaje_descuento.HeaderText = "% descuento"
            Me.dgDatos.Columns.Add(Porcentaje_descuento)

            Me.dgDatos.DataBind()
            Me.lblCant.Text = Datos.Tables(0).Rows.Count & " Tipo de Habitacion"
        End If

    End Sub

    Sub Iniciar()

        Me.btnGuardar.Visible = False
        Me.btnCancelar.Visible = False
        Me.btnModificar.Visible = False
        Me.btnDeschabilitar.Visible = False
        Me.btnBuscarNombre.Visible = False

    End Sub


    Sub HabilitaForm()

        Me.ddlbEstado.Enabled = True
        Me.txtIdTipoHabitacion.ReadOnly = True
        Me.txtNombre.Enabled = True
        Me.txtPorcentaje_descuento.Enabled = True


    End Sub

    Sub DeshabilitarForm()
        Me.ddlbEstado.Enabled = False
        Me.txtIdTipoHabitacion.ReadOnly = True
        Me.txtNombre.Enabled = False
        Me.txtPorcentaje_descuento.Enabled = False
    End Sub

    Sub LimpiarForm()

        Me.txtIdTipoHabitacion.Text = ""
        Me.txtNombre.Text = ""
        Me.txtPorcentaje_descuento.Text = ""
        Me.ddlbEstado.ClearSelection()

    End Sub

    Protected Sub btnAgregar_Click(sender As Object, e As System.EventArgs) Handles btnAgregar.Click
        Me.Session.Item("Accion") = "Agregar"

        Me.btnAgregar.Visible = False
        Me.btnGuardar.Visible = True
        Me.btnCancelar.Visible = True

        Me.btnBuscar.Visible = False

        HabilitaForm()
    End Sub

    Protected Sub btnCancelar_Click(sender As Object, e As System.EventArgs) Handles btnCancelar.Click

        Select Case Me.Session.Item("Accion")
            Case "Buscar"

                Me.btnAgregar.Visible = True
                Me.btnBuscar.Visible = True

                Me.btnBuscarNombre.Visible = False
                Me.btnCancelar.Visible = False

                Me.txtNombre.Enabled = False
                Me.txtPorcentaje_descuento.Enabled = False

                Dim cControl As New dbTipoHabitacion
                Me.LlenarGrid(cControl.RecuperarTipoHabitacion)

            Case "Modificar"


                Me.btnModificar.Visible = False
                Me.btnCancelar.Visible = False

                Me.btnGuardar.Visible = False
                Me.btnBuscarNombre.Visible = False


                Me.btnAgregar.Visible = True
                Me.btnBuscar.Visible = True

                Me.LimpiarForm()
                Me.DeshabilitarForm()

                Dim cControl As New dbTipoHabitacion
                Me.LlenarGrid(cControl.RecuperarTipoHabitacion)

            Case "Agregar"

                Me.DeshabilitarForm()

                Me.btnAgregar.Visible = True
                Me.btnBuscar.Visible = True

                Me.btnGuardar.Visible = False
                Me.btnCancelar.Visible = False

            Case "Seleccion"

                Me.btnModificar.Visible = False
                Me.btnCancelar.Visible = False
                Me.btnBuscarNombre.Visible = False

                Me.btnAgregar.Visible = True
                Me.btnBuscar.Visible = True

                Dim cControl As New dbTipoHabitacion
                Me.LlenarGrid(cControl.RecuperarTipoHabitacion)



                Me.LimpiarForm()
                Me.DeshabilitarForm()

        End Select

    End Sub

    Protected Sub btnModificar_Click(sender As Object, e As System.EventArgs) Handles btnModificar.Click
        Me.Session.Item("Accion") = "Modificar"

        Me.HabilitaForm()

        Me.txtIdTipoHabitacion.ReadOnly = True

        Me.btnGuardar.Visible = True
        Me.btnCancelar.Visible = True

        Me.btnModificar.Visible = False
        Me.btnBuscarNombre.Visible = False
    End Sub

    Protected Sub btnBuscar_Click(sender As Object, e As System.EventArgs) Handles btnBuscar.Click

        Me.Session.Item("Accion") = "Buscar"

        Me.btnBuscarNombre.Visible = True
        Me.txtNombre.Enabled = True
        Me.txtPorcentaje_descuento.Enabled = True
        Me.btnCancelar.Visible = True

        Me.btnBuscar.Visible = False
        Me.btnAgregar.Visible = False

    End Sub

    Protected Sub btnBuscarNombre_Click(sender As Object, e As System.EventArgs) Handles btnBuscarNombre.Click

        Dim dsDatos As New DataSet
        Dim cControl As New dbTipoHabitacion

        dsDatos = cControl.BuscarTipoHabitacionPorNombre(Me.txtNombre.Text)

        If dsDatos.Tables(0).Rows.Count > 0 Then

            Me.LlenarGrid(dsDatos)
            Me.lblCant.Text = dsDatos.Tables(0).Rows.Count & " Tipo Habitacion"
        Else

            Me.dgDatos.EmptyDataText = "Datos no encontrado con esos parametros"
            Me.dgDatos.Columns.Clear()
            Me.dgDatos.DataSource = Nothing
            Me.dgDatos.DataBind()

            'Mensaje

        End If

        Me.LimpiarForm()
        Me.btnModificar.Visible = False

    End Sub

    Protected Sub btnGuardar_Click(sender As Object, e As System.EventArgs) Handles btnGuardar.Click

        Dim lEntidad As New TipoHabitacion
        Dim cControl As New dbTipoHabitacion
        Dim dsDatos As New DataSet

        If Me.Session.Item("Accion") = "Agregar" Then
            If String.IsNullOrEmpty(Me.hfTipoHabitacion.Value) = True Then

                lEntidad.IdEstado = Me.ddlbEstado.SelectedValue
                lEntidad.NombreHabitacion = Me.txtNombre.Text
                lEntidad.Porcentaje_descuento = Val(Me.txtPorcentaje_descuento.Text)
                If cControl.GuardarTipoHabitacion(lEntidad) > 0 Then

                    DeshabilitarForm()
                    Iniciar()

                    LlenarGrid(cControl.RecuperarTipoHabitacion)

                    Me.LimpiarForm()
                    Me.Session.Item("Accion") = ""

                    Me.btnAgregar.Visible = True
                    Me.btnBuscar.Visible = True


                Else

                End If

            End If

        ElseIf Me.Session.Item("Accion") = "Modificar" Then

            lEntidad.IdTipoHabitacion = Me.txtIdTipoHabitacion.Text
            lEntidad.NombreHabitacion = Me.txtNombre.Text
            lEntidad.Porcentaje_descuento = Val(Me.txtPorcentaje_descuento.Text)
            lEntidad.IdEstado = Me.ddlbEstado.Text

            If cControl.ModificarTipoHabitacion(lEntidad) > 0 Then


                DeshabilitarForm()
                Iniciar()

                LlenarGrid(cControl.RecuperarTipoHabitacion)

                Me.LimpiarForm()
                Me.Session.Item("Accion") = ""

                Me.btnAgregar.Visible = True
                Me.btnBuscar.Visible = True



            Else



            End If

        End If

    End Sub

    Protected Sub dgDatos_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles dgDatos.SelectedIndexChanged

        Dim dsDatos As New DataSet
        Dim cControl As New dbTipoHabitacion

        Me.Session.Item("Accion") = "Seleccion"

        dsDatos = cControl.RecuperarTipoHabitacionPorId(Me.dgDatos.SelectedRow.Cells(1).Text)

        If dsDatos.Tables(0).Rows.Count > 0 Then

            Me.LlenarGrid(dsDatos)

            'llenar Datos para Modificar
            Me.txtIdTipoHabitacion.Text = dsDatos.Tables(0).Rows(0).Item("IdTipoHabitacion").ToString
            Me.txtNombre.Text = dsDatos.Tables(0).Rows(0).Item("NombreHabitacion").ToString
            Me.txtPorcentaje_descuento.Text = dsDatos.Tables(0).Rows(0).Item("porcentaje_descuento").ToString
            Me.ddlbEstado.Text = dsDatos.Tables(0).Rows(0).Item("idEstado").ToString

            Me.lblCant.Text = dsDatos.Tables(0).Rows.Count & " Tipo Habitacion"

            Me.btnAgregar.Visible = False
            Me.btnBuscar.Visible = False
            Me.btnGuardar.Visible = False

            Me.btnModificar.Visible = True
            Me.btnCancelar.Visible = True

            Me.btnBuscarNombre.Visible = False

        End If

        Me.DeshabilitarForm()

    End Sub
End Class
