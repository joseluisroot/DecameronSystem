USE [Decameron]
GO
/****** Object:  Table [dbo].[Cliente]    Script Date: 16/11/2014 23:57:13 ******/
DROP TABLE [dbo].[Cliente]
GO
/****** Object:  Table [dbo].[Cliente]    Script Date: 16/11/2014 23:57:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Cliente](
	[IdCliente] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](250) NULL,
	[Apellidos] [varchar](250) NULL,
	[Sexo] [char](1) NULL,
	[Edad] [int] NULL,
	[FechaIngreso] [datetime] NULL,
	[Direccion] [varchar](max) NULL,
	[Telefono] [int] NULL,
	[Email] [varchar](50) NULL,
 CONSTRAINT [PK_Cliente] PRIMARY KEY CLUSTERED 
(
	[IdCliente] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Cliente] ON 

INSERT [dbo].[Cliente] ([IdCliente], [Nombre], [Apellidos], [Sexo], [Edad], [FechaIngreso], [Direccion], [Telefono], [Email]) VALUES (1, N'JUAN', N'PEREZ', N'M', 40, CAST(0x0000A3CA00000000 AS DateTime), N'APOPA', 21210000, NULL)
INSERT [dbo].[Cliente] ([IdCliente], [Nombre], [Apellidos], [Sexo], [Edad], [FechaIngreso], [Direccion], [Telefono], [Email]) VALUES (3, N'JOAQUIN', N'SANCHEZ', N'M', 25, CAST(0x0000A3E501866AD5 AS DateTime), N'NEJAPA', 212102020, N'tec.jesa1989@gmail.com')
INSERT [dbo].[Cliente] ([IdCliente], [Nombre], [Apellidos], [Sexo], [Edad], [FechaIngreso], [Direccion], [Telefono], [Email]) VALUES (4, N'LINDA', N'MENDOZA', N'F', 26, CAST(0x0000A3E501871D2A AS DateTime), N'QUEZALTEPEQUE', 23109999, N'LIK@LIKE.COM')
INSERT [dbo].[Cliente] ([IdCliente], [Nombre], [Apellidos], [Sexo], [Edad], [FechaIngreso], [Direccion], [Telefono], [Email]) VALUES (5, N'jessica', N'alba', N'f', 29, CAST(0x0000A3E5018795A1 AS DateTime), N'EUA', 21211010, N'li@lo.com')
SET IDENTITY_INSERT [dbo].[Cliente] OFF
