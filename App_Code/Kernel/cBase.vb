﻿Imports Microsoft.VisualBasic

Public Class cBase
    Inherits FunctionKit

    Public Sub finSession()
        If Session("activa") = "" Then
            CerrarSesion()
            Redirect("Interno")
        End If
    End Sub

    Public Sub LogInOn()
        If Session("activa") = "" Then
            Response.Redirect("Login.aspx")
        End If
    End Sub


    Public Sub CerrarSesion()
        Me.Session.Item("activa") = ""
        Me.Session.Item("NombreUsuario") = ""
        Me.Session.Item("IdUsuario") = ""
        Me.Session.Item("Accion") = ""

    End Sub

    Public Sub Redirect(Tipo As String)
        If Tipo = "Salto" Then
            Response.Redirect("CerrarSession.aspx?Sesion=out", False)

        ElseIf Tipo = "Interno" Then

            Response.Redirect("CerrarSession.aspx?Sesion=out", False)
        End If

    End Sub

End Class
