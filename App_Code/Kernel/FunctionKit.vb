﻿Imports Microsoft.VisualBasic
Imports System.Security.Cryptography

Public Class FunctionKit
    Inherits System.Web.UI.Page

    Public Shared Function GetSHA1(str As String) As String
        Dim sha1 As SHA1 = SHA1Managed.Create()
        Dim encoding As New ASCIIEncoding()
        Dim stream As Byte() = Nothing
        Dim sb As New StringBuilder()
        stream = sha1.ComputeHash(encoding.GetBytes(str))
        For i As Integer = 0 To stream.Length - 1
            sb.AppendFormat("{0:x2}", stream(i))
        Next
        Return sb.ToString()
    End Function

End Class
