﻿Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports System.Data

Public Class dbCliente
    Public Function RecuperarClientes() As DataSet

        Dim myCnnString As String
        Dim mySelectText As String = Resources.rsCliente.RecuperarClientes

        Dim cnn As New cnnGenerate
        myCnnString = cnn.GeneradorcnnString(AppSettings("cnnString"))

        Dim myDataAdapter As New SqlDataAdapter(mySelectText, myCnnString)
        Dim ldsMiDataSet As New DataSet

        Try
            myDataAdapter.Fill(ldsMiDataSet)

        Catch ex As Exception

        End Try

        Return ldsMiDataSet

    End Function

    Public Function GuardarCliente(ByVal eCliente As Cliente) As Integer

        Dim myCnnString As String
        Dim cnn As New cnnGenerate

        myCnnString = cnn.GeneradorcnnString(AppSettings("cnnString"))
        Dim myConnection As New SqlConnection(myCnnString)

        Dim lsCommandstring As String = Resources.rsCliente.GuardarCliente

        Dim command As New SqlCommand(lsCommandstring, myConnection)

        Try
            myConnection.Open()

            command.Parameters.Add("@Nombre", SqlDbType.VarChar).Value = eCliente.NombreCliente
            command.Parameters.Add("@Apellidos", SqlDbType.VarChar).Value = eCliente.ApellidosCliente
            command.Parameters.Add("@Edad", SqlDbType.Int).Value = eCliente.Edad
            command.Parameters.Add("@FechaIngreso", SqlDbType.DateTime).Value = eCliente.FechaInfreso
            command.Parameters.Add("@Direccion", SqlDbType.VarChar).Value = eCliente.Direccion
            command.Parameters.Add("@Telefono", SqlDbType.Int).Value = eCliente.Telefono
            command.Parameters.Add("@Sexo", SqlDbType.VarChar).Value = eCliente.Sexo
            command.Parameters.Add("@Email", SqlDbType.VarChar).Value = eCliente.Email
            command.ExecuteNonQuery()
            myConnection.Close()
            Return 1
        Catch ex As Exception
            myConnection.Close()
            MsgBox("Error al guardar la información", MsgBoxStyle.Exclamation, "Sistema Decameron")
            Return 0
        End Try

    End Function

    Public Function ModificarCliente(ByVal eCliente As Cliente) As Integer
        Dim myCnnString As String
        Dim cnn As New cnnGenerate

        myCnnString = cnn.GeneradorcnnString(AppSettings("cnnString"))
        Dim myConnection As New SqlConnection(myCnnString)

        Dim lsCommandstring As String = Resources.rsCliente.ModificarCliente
        Dim command As New SqlCommand(lsCommandstring, myConnection)

        Try
            myConnection.Open()
            command.Parameters.Add("@IdCliente", SqlDbType.Int).Value = eCliente.IdCliente
            command.Parameters.Add("@Nombre", SqlDbType.Int).Value = eCliente.NombreCliente
            command.Parameters.Add("@Apellidos", SqlDbType.VarChar).Value = eCliente.ApellidosCliente
            command.Parameters.Add("@Sexo", SqlDbType.VarChar).Value = eCliente.Sexo
            command.Parameters.Add("@Direccion", SqlDbType.VarChar).Value = eCliente.Direccion
            command.Parameters.Add("@Telefono", SqlDbType.Int).Value = eCliente.Telefono
            command.Parameters.Add("@Email", SqlDbType.VarChar).Value = eCliente.Email
            command.ExecuteNonQuery()
            myConnection.Close()

            Return 1

        Catch ex As Exception
            myConnection.Close()
            Return 0
        End Try

    End Function

    Public Function RecuperarClientePorId(ByVal idCliente As String) As DataSet
        Dim myCnnString As String
        Dim cnn As New cnnGenerate

        myCnnString = cnn.GeneradorcnnString(AppSettings("cnnString"))
        Dim myConnection As New SqlConnection(myCnnString)

        Dim mySelectText As String = Resources.rsCliente.RecuperarClientePorId

        Dim myDataAdapter As New SqlDataAdapter(mySelectText, myCnnString)
        Dim ldsMiDataSet As New DataSet

        Try
            myDataAdapter.SelectCommand.Parameters.Add("@idCliente", SqlDbType.Int).Value = idCliente
            myDataAdapter.Fill(ldsMiDataSet)

        Catch ex As Exception

        End Try

        Return ldsMiDataSet
    End Function

    Public Function BuscarCliente(ByVal nombre As String) As DataSet

        Dim myCnnString As String
        Dim cnn As New cnnGenerate

        myCnnString = cnn.GeneradorcnnString(AppSettings("cnnString"))
        Dim myConnection As New SqlConnection(myCnnString)

        Dim mySelectText As String = Resources.rsCliente.BuscarCliente

        Dim myDataAdapter As New SqlDataAdapter(mySelectText, myCnnString)
        Dim ldsMiDataSet As New DataSet

        Try
            myDataAdapter.SelectCommand.Parameters.Add("@nombre", SqlDbType.VarChar).Value = nombre
            myDataAdapter.Fill(ldsMiDataSet)

        Catch ex As Exception

        End Try

        Return ldsMiDataSet
    End Function
End Class
