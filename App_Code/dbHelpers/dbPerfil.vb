﻿Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports System.Data

Public Class dbPerfil

    Public Function RecuperarPerfil() As DataSet

        Dim myCnnString As String
        Dim mySelectText As String = Resources.rsPerfil.RecuperarPerfil

        Dim cnn As New cnnGenerate
        myCnnString = cnn.GeneradorcnnString(AppSettings("cnnString"))

        Dim myDataAdapter As New SqlDataAdapter(mySelectText, myCnnString)

        Dim ldsMiDataSet As New DataSet

        Try

            myDataAdapter.Fill(ldsMiDataSet)

        Catch ex As Exception

        End Try

        Return ldsMiDataSet

    End Function

    Public Function GuardarPerfil(ByVal ePerfil As Perfil) As Integer

        Dim myCnnString As String
        Dim cnn As New cnnGenerate

        myCnnString = cnn.GeneradorcnnString(AppSettings("cnnString"))
        Dim myConnection As New SqlConnection(myCnnString)

        Dim lsCommandstring As String = Resources.rsPerfil.GuardarPerfil

        Dim command As New SqlCommand(lsCommandstring, myConnection)

        Try
            myConnection.Open()

            command.Parameters.Add("@NombrePerfil", SqlDbType.VarChar).Value = ePerfil.NombrePerfil

            command.ExecuteNonQuery()
            myConnection.Close()

            Return 1

        Catch ex As Exception
            myConnection.Close()
            Return 0
        End Try

    End Function

    Public Function RecuperarPerfilPorId(ByVal IdPerfil As String) As DataSet

        Dim myCnnString As String
        Dim cnn As New cnnGenerate

        myCnnString = cnn.GeneradorcnnString(AppSettings("cnnString"))
        Dim myConnection As New SqlConnection(myCnnString)


        Dim mySelectText As String = Resources.rsPerfil.RecuperarPerfilPorId

        Dim myDataAdapter As New SqlDataAdapter(mySelectText, myCnnString)
        Dim ldsMiDataSet As New DataSet

        Try

            myDataAdapter.SelectCommand.Parameters.Add("@IdPerfil", SqlDbType.Int).Value = IdPerfil
            myDataAdapter.Fill(ldsMiDataSet)

        Catch ex As Exception



        End Try

        Return ldsMiDataSet
    End Function

    Public Function BuscarPerfilPorNombre(ByVal NombrePerfil As String) As DataSet

        Dim myCnnString As String
        Dim cnn As New cnnGenerate

        myCnnString = cnn.GeneradorcnnString(AppSettings("cnnString"))
        Dim myConnection As New SqlConnection(myCnnString)

        Dim mySelectText As String = Resources.rsPerfil.BuscarPerfilPorNombre

        Dim myDataAdapter As New SqlDataAdapter(mySelectText, myCnnString)
        Dim ldsMiDataSet As New DataSet

        Try

            myDataAdapter.SelectCommand.Parameters.Add("@NombrePerfil", SqlDbType.VarChar).Value = NombrePerfil
            myDataAdapter.Fill(ldsMiDataSet)

        Catch ex As Exception

        End Try

        Return ldsMiDataSet

    End Function

    Public Function ModificarPerfil(ByVal ePerfil As Perfil) As Integer


        Dim myCnnString As String
        Dim cnn As New cnnGenerate

        myCnnString = cnn.GeneradorcnnString(AppSettings("cnnString"))
        Dim myConnection As New SqlConnection(myCnnString)

        Dim lsCommandstring As String = Resources.rsPerfil.ModificarPerfil
        Dim command As New SqlCommand(lsCommandstring, myConnection)

        Try
            myConnection.Open()

            command.Parameters.Add("@IdPerfil", SqlDbType.Int).Value = ePerfil.IdPerfil
            command.Parameters.Add("@NombrePerfil", SqlDbType.VarChar).Value = ePerfil.NombrePerfil

            command.ExecuteNonQuery()
            myConnection.Close()

            Return 1

        Catch ex As Exception
            myConnection.Close()
            Return 0
        End Try

    End Function



End Class
