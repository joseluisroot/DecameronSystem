﻿Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports System.Data

Public Class dbPlan
    Public Function RecuperarPlan() As DataSet

        Dim myCnnString As String
        Dim mySelectText As String = Resources.rsPlan.RecuperarPlan

        Dim cnn As New cnnGenerate
        myCnnString = cnn.GeneradorcnnString(AppSettings("cnnString"))

        Dim myDataAdapter As New SqlDataAdapter(mySelectText, myCnnString)
        Dim ldsMiDataSet As New DataSet

        Try
            myDataAdapter.Fill(ldsMiDataSet)

        Catch ex As Exception

        End Try

        Return ldsMiDataSet

    End Function

    Public Function GuardarPlan(ByVal ePlan As Plan) As Integer

        Dim myCnnString As String
        Dim cnn As New cnnGenerate

        myCnnString = cnn.GeneradorcnnString(AppSettings("cnnString"))
        Dim myConnection As New SqlConnection(myCnnString)

        Dim lsCommandstring As String = Resources.rsPlan.GuardarPlan
        Dim command As New SqlCommand(lsCommandstring, myConnection)

        Try
            myConnection.Open()

            command.Parameters.Add("@IdTipoHabitacion", SqlDbType.Int).Value = ePlan.IdTipoHabitacion
            command.Parameters.Add("@Nombre_plan", SqlDbType.VarChar).Value = ePlan.Nombre_plan
            command.Parameters.Add("@Valor_plan", SqlDbType.Decimal).Value = ePlan.Valor_plan
            command.Parameters.Add("@Valor_descuento", SqlDbType.Decimal).Value = ePlan.Valor_descuento
            command.Parameters.Add("@Valor_real", SqlDbType.Decimal).Value = ePlan.Valor_real
            command.Parameters.Add("@monto_iva", SqlDbType.Decimal).Value = ePlan.monto_iva
            command.Parameters.Add("@Valor_total", SqlDbType.Decimal).Value = ePlan.Valor_total
            command.ExecuteNonQuery()
            myConnection.Close()
            Return 1
        Catch ex As Exception
            myConnection.Close()
            MsgBox(ex.ToString, MsgBoxStyle.Exclamation, "Sistema Decamero")
            MsgBox("Error al guardar la información", MsgBoxStyle.Exclamation, "Sistema Decameron")
            Return 0
        End Try

    End Function

    Public Function ModificarPlan(ByVal ePlan As Plan) As Integer
        Dim myCnnString As String
        Dim cnn As New cnnGenerate

        myCnnString = cnn.GeneradorcnnString(AppSettings("cnnString"))
        Dim myConnection As New SqlConnection(myCnnString)

        Dim lsCommandstring As String = Resources.rsPlan.ModificarPlan
        Dim command As New SqlCommand(lsCommandstring, myConnection)

        Try
            myConnection.Open()
            command.Parameters.Add("@idPlan", SqlDbType.Int).Value = ePlan.IdPlan
            command.Parameters.Add("@IdTipoHabitacion", SqlDbType.Int).Value = ePlan.IdTipoHabitacion
            command.Parameters.Add("@Nombre_plan", SqlDbType.VarChar).Value = ePlan.Nombre_plan
            command.Parameters.Add("@Valor_plan", SqlDbType.Decimal).Value = ePlan.Valor_plan
            command.Parameters.Add("@Valor_descuento", SqlDbType.Decimal).Value = ePlan.Valor_descuento
            command.Parameters.Add("@Valor_real", SqlDbType.Decimal).Value = ePlan.Valor_real
            command.Parameters.Add("@monto_iva", SqlDbType.Decimal).Value = ePlan.monto_iva
            command.Parameters.Add("@Valor_total", SqlDbType.Decimal).Value = ePlan.Valor_total
            command.ExecuteNonQuery()
            myConnection.Close()

            Return 1

        Catch ex As Exception
            myConnection.Close()
            MsgBox(ex.ToString, MsgBoxStyle.Exclamation, "Sistema Decamero")
            MsgBox("Error al guardar la información", MsgBoxStyle.Exclamation, "Sistema Decameron")
            Return 0
        End Try

    End Function

    Public Function RecuperarPlanPor(ByVal idValor As Integer) As DataSet
        Dim myCnnString As String
        Dim cnn As New cnnGenerate

        myCnnString = cnn.GeneradorcnnString(AppSettings("cnnString"))
        Dim myConnection As New SqlConnection(myCnnString)

        Dim mySelectText As String = Resources.rsPlan.RecuperarPlan

        Dim myDataAdapter As New SqlDataAdapter(mySelectText, myCnnString)
        Dim ldsMiDataSet As New DataSet

        Try
            myDataAdapter.SelectCommand.Parameters.Add("@IdPlan", SqlDbType.VarChar).Value = "WHERE idValor"
            myDataAdapter.Fill(ldsMiDataSet)

        Catch ex As Exception

        End Try

        Return ldsMiDataSet
    End Function

    Public Function RecuperarPlanPorid(ByVal IdPlan As Integer) As DataSet
        Dim myCnnString As String
        Dim cnn As New cnnGenerate

        myCnnString = cnn.GeneradorcnnString(AppSettings("cnnString"))
        Dim myConnection As New SqlConnection(myCnnString)

        Dim mySelectText As String = Resources.rsPlan.RecuperarPlanPorId

        Dim myDataAdapter As New SqlDataAdapter(mySelectText, myCnnString)
        Dim ldsMiDataSet As New DataSet

        Try
            myDataAdapter.SelectCommand.Parameters.Add("@IdPlan", SqlDbType.VarChar).Value = IdPlan
            myDataAdapter.Fill(ldsMiDataSet)

        Catch ex As Exception

        End Try

        Return ldsMiDataSet
    End Function
End Class
