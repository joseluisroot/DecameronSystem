﻿Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports System.Data

Public Class dbHabitacion
    Public Function RecuperarHabitacion() As DataSet

        Dim myCnnString As String
        Dim mySelectText As String = Resources.rsHabitacion.RecuperarHabitacion

        Dim cnn As New cnnGenerate

        myCnnString = cnn.GeneradorcnnString(AppSettings("cnnString"))

        Dim myDataAdapter As New SqlDataAdapter(mySelectText, myCnnString)
        Dim ldsMiDataSet As New DataSet

        Try
            myDataAdapter.Fill(ldsMiDataSet)
        Catch ex As Exception


        End Try
        Return ldsMiDataSet

    End Function

    Public Function GuardarHabitacion(ByVal eHabitacion As Habitacion) As Integer

        Dim myCnnString As String
        Dim cnn As New cnnGenerate

        myCnnString = cnn.GeneradorcnnString(AppSettings("cnnString"))

        Dim MyConnection As New SqlConnection(myCnnString)
        Dim lsCommandstring As String = Resources.rsHabitacion.GuardarHabitacion
        Dim command As New SqlCommand(lsCommandstring, MyConnection)

        Try
            MyConnection.Open()
            command.Parameters.Add("@NombreHabitacion", SqlDbType.VarChar).Value = eHabitacion.NombreHabitacion
            command.Parameters.Add("@Precio", SqlDbType.Float).Value = eHabitacion.Precio
            command.Parameters.Add("@IdTipoHabitacion", SqlDbType.Int).Value = eHabitacion.IdTipoHabitacion
            command.Parameters.Add("@Descuento", SqlDbType.Decimal).Value = eHabitacion.Descuento
            command.ExecuteNonQuery()
            MyConnection.Close()
            Return 1
        Catch ex As Exception
            MyConnection.Close()
            Return 0
        End Try
    End Function

    Public Function ModificarHabitacion(ByVal eHabitacion As Habitacion) As Integer
        Dim myCnnString As String
        Dim cnn As New cnnGenerate

        myCnnString = cnn.GeneradorcnnString(AppSettings("cnnString"))
        Dim MyConnection As New SqlConnection(myCnnString)

        Dim lsCommand As String = Resources.rsHabitacion.ModificarHabitacion
        Dim command As New SqlCommand(lsCommand, MyConnection)

        Try
            MyConnection.Open()
            command.Parameters.Add("@IdHabitacion", SqlDbType.Int).Value = eHabitacion.IdHabitacion
            command.Parameters.Add("@NombreHabitacion", SqlDbType.VarChar).Value = eHabitacion.NombreHabitacion
            command.Parameters.Add("@Precio", SqlDbType.Float).Value = eHabitacion.Precio
            command.Parameters.Add("@IdTipoHabitacion", SqlDbType.Int).Value = eHabitacion.IdTipoHabitacion
            command.Parameters.Add("@Descuento", SqlDbType.Decimal).Value = eHabitacion.Descuento
            command.ExecuteNonQuery()
            MyConnection.Close()
            Return 1

        Catch ex As Exception
            MyConnection.Close()
            Return 0

        End Try

    End Function

    Public Function RecuperarHabitacionPorId(ByVal IdHabitacion As String) As DataSet

        Dim myCnnString As String
        Dim cnn As New cnnGenerate

        myCnnString = cnn.GeneradorcnnString(AppSettings("cnnString"))
        Dim MyConnection As New SqlConnection(myCnnString)

        Dim mySelectText As String = Resources.rsHabitacion.RecuperarHabitacionPorId


        Dim myDataAdapter As New SqlDataAdapter(mySelectText, myCnnString)
        Dim ldsMiDataSet As New DataSet

        Try
            myDataAdapter.SelectCommand.Parameters.Add("@IdHabitacion", SqlDbType.Int).Value = IdHabitacion
            myDataAdapter.Fill(ldsMiDataSet)

        Catch ex As Exception

        End Try
        Return ldsMiDataSet

    End Function

    Public Function BuscarHabitacionPorNombre(ByVal NombreHabitacion As String) As DataSet

        Dim myCnnString As String
        Dim cnn As New cnnGenerate

        myCnnString = cnn.GeneradorcnnString(AppSettings("cnnString"))
        Dim MyConnection As New SqlConnection(myCnnString)

        Dim mySelectText As String = Resources.rsHabitacion.BuscarHabitacionPorNombre

        Dim myDataAdapter As New SqlDataAdapter(mySelectText, myCnnString)
        Dim ldsMiDataSet As New DataSet

        Try
            myDataAdapter.SelectCommand.Parameters.Add("@NombreHabitacion", SqlDbType.VarChar).Value = NombreHabitacion
            myDataAdapter.Fill(ldsMiDataSet)

        Catch ex As Exception

        End Try
        Return ldsMiDataSet

    End Function

    Public Function RecuperarEstadoHabitacionesArduino() As DataSet

        Dim myCnnString As String
        Dim cnn As New cnnGenerate

        myCnnString = cnn.GeneradorcnnString(AppSettings("cnnString"))
        Dim MyConnection As New SqlConnection(myCnnString)

        Dim mySelectText As String = Resources.rsHabitacion.RecuperarEstadoHabitacionesArduino

        Dim myDataAdapter As New SqlDataAdapter(mySelectText, myCnnString)
        Dim ldsMiDataSet As New DataSet

        Try
            myDataAdapter.Fill(ldsMiDataSet, "Habitaciones")

        Catch ex As Exception

        End Try
        Return ldsMiDataSet

    End Function

    Public Function RecuperarHabitacionDisponible() As DataSet

        Dim myCnnString As String
        Dim mySelectText As String = Resources.rsHabitacion.RecuperarHabitacionDisponible

        Dim cnn As New cnnGenerate

        myCnnString = cnn.GeneradorcnnString(AppSettings("cnnString"))

        Dim myDataAdapter As New SqlDataAdapter(mySelectText, myCnnString)
        Dim ldsMiDataSet As New DataSet

        Try
            myDataAdapter.Fill(ldsMiDataSet)
        Catch ex As Exception


        End Try
        Return ldsMiDataSet

    End Function

End Class
