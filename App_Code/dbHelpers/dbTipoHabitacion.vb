﻿Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports System.Data

Public Class dbTipoHabitacion

    Public Function RecuperarTipoHabitacion() As DataSet

        Dim myCnnString As String
        Dim mySelectText As String = Resources.rsTipoHabitacion.RecuperarTipoHabitacion

        Dim cnn As New cnnGenerate

        myCnnString = cnn.GeneradorcnnString(AppSettings("cnnString"))

        Dim myDataAdapter As New SqlDataAdapter(mySelectText, myCnnString)
        Dim ldsMiDataSet As New DataSet

        Try
            myDataAdapter.Fill(ldsMiDataSet)
        Catch ex As Exception


        End Try
        Return ldsMiDataSet

    End Function

    Public Function GuardarTipoHabitacion(ByVal eTipoHabitacion As TipoHabitacion) As Integer

        Dim myCnnString As String
        Dim cnn As New cnnGenerate

        myCnnString = cnn.GeneradorcnnString(AppSettings("cnnString"))
        Dim MyConnection As New SqlConnection(myCnnString)

        Dim lsCommandstring As String = Resources.rsTipoHabitacion.GuardarTipoHabitacion

        Dim command As New SqlCommand(lsCommandstring, MyConnection)

        Try
            MyConnection.Open()
            command.Parameters.Add("@NombreHabitacion", SqlDbType.VarChar).Value = eTipoHabitacion.NombreHabitacion
            command.Parameters.Add("@porcentaje_descuento", SqlDbType.Decimal).Value = eTipoHabitacion.Porcentaje_descuento
            command.Parameters.Add("@IdEstado", SqlDbType.Int).Value = eTipoHabitacion.IdEstado
            command.ExecuteNonQuery()
            MyConnection.Close()
            Return 1
        Catch ex As Exception
            MyConnection.Close()
            Return 0
        End Try
    End Function

    Public Function ModificarTipoHabitacion(ByVal eTipoHabitacion As TipoHabitacion) As Integer
        Dim myCnnString As String
        Dim cnn As New cnnGenerate

        myCnnString = cnn.GeneradorcnnString(AppSettings("cnnString"))
        Dim MyConnection As New SqlConnection(myCnnString)

        Dim lsCommand As String = Resources.rsTipoHabitacion.ModificarTipoHabitacion
        Dim command As New SqlCommand(lsCommand, MyConnection)

        Try
            MyConnection.Open()
            command.Parameters.Add("@IdTipoHabitacion", SqlDbType.Int).Value = eTipoHabitacion.IdTipoHabitacion
            command.Parameters.Add("@NombreHabitacion", SqlDbType.VarChar).Value = eTipoHabitacion.NombreHabitacion
            command.Parameters.Add("@porcentaje_descuento", SqlDbType.Decimal).Value = eTipoHabitacion.Porcentaje_descuento
            command.Parameters.Add("@IdEstado", SqlDbType.Int).Value = eTipoHabitacion.IdEstado
            command.ExecuteNonQuery()
            MyConnection.Close()
            Return 1

        Catch ex As Exception
            MyConnection.Close()
            Return 0

        End Try

    End Function

    Public Function RecuperarTipoHabitacionPorId(ByVal IdTipoHabitacion As String) As DataSet

        Dim myCnnString As String
        Dim cnn As New cnnGenerate

        myCnnString = cnn.GeneradorcnnString(AppSettings("cnnString"))
        Dim MyConnection As New SqlConnection(myCnnString)

        Dim mySelectText As String = Resources.rsTipoHabitacion.RecuperarTipoHabitacionPorId

        Dim myDataAdapter As New SqlDataAdapter(mySelectText, myCnnString)
        Dim ldsMiDataSet As New DataSet

        Try
            myDataAdapter.SelectCommand.Parameters.Add("@IdTipoHabitacion", SqlDbType.Int).Value = IdTipoHabitacion
            myDataAdapter.Fill(ldsMiDataSet)

        Catch ex As Exception

        End Try
        Return ldsMiDataSet

    End Function

    Public Function BuscarTipoHabitacionPorNombre(ByVal NombreHabitacion As String) As DataSet

        Dim myCnnString As String
        Dim cnn As New cnnGenerate

        myCnnString = cnn.GeneradorcnnString(AppSettings("cnnString"))
        Dim MyConnection As New SqlConnection(myCnnString)

        Dim mySelectText As String = Resources.rsTipoHabitacion.BuscarTipoHabitacionPorNombre

        Dim myDataAdapter As New SqlDataAdapter(mySelectText, myCnnString)
        Dim ldsMiDataSet As New DataSet

        Try
            myDataAdapter.SelectCommand.Parameters.Add("@NombreHabitacion", SqlDbType.VarChar).Value = NombreHabitacion
            myDataAdapter.Fill(ldsMiDataSet)

        Catch ex As Exception

        End Try
        Return ldsMiDataSet

    End Function


End Class
