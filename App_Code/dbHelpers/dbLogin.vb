﻿Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports System.Data

Public Class dbLogin

    Public Function IngresarAlSistema(eEntidad As LoginSys) As DataSet

        Dim myCnnString As String
        Dim mySelectText As String = Resources.rsLogin.Ingresar

        Dim cnn As New cnnGenerate
        myCnnString = cnn.GeneradorcnnString(AppSettings("cnnString"))

        Dim myDataAdapter As New SqlDataAdapter(mySelectText, myCnnString)

        Dim ldsMiDataSet As New DataSet

        Try

            myDataAdapter.SelectCommand.Parameters.Add("@usuario", SqlDbType.VarChar).Value = eEntidad.usuario
            myDataAdapter.SelectCommand.Parameters.Add("@clave", SqlDbType.VarChar).Value = eEntidad.clave

            myDataAdapter.Fill(ldsMiDataSet)

        Catch ex As Exception

        End Try

        Return ldsMiDataSet

    End Function


End Class
