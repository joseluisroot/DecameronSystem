﻿Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports System.Data

Public Class dbInsumos

    Public Function RecuperarInsumos() As DataSet

        Dim myCnnString As String
        Dim mySelectText As String = Resources.rsInsumos.RecuperarInsumo

        Dim cnn As New cnnGenerate

        myCnnString = cnn.GeneradorcnnString(AppSettings("cnnString"))

        Dim myDataAdapter As New SqlDataAdapter(mySelectText, myCnnString)
        Dim ldsMiDataSet As New DataSet

        Try
            myDataAdapter.Fill(ldsMiDataSet)
        Catch ex As Exception


        End Try
        Return ldsMiDataSet

    End Function

    Public Function GuardarInsumo(ByVal eInsumos As Insumos) As Integer

        Dim myCnnString As String
        Dim cnn As New cnnGenerate

        myCnnString = cnn.GeneradorcnnString(AppSettings("cnnString"))
        Dim MyConmection As New SqlConnection(myCnnString)

        Dim lsCommandstring As String = Resources.rsInsumos.GuardarInsumo

        Dim command As New SqlCommand(lsCommandstring, MyConmection)

        Try
            MyConmection.Open()
            command.Parameters.Add("@Nombre", SqlDbType.VarChar).Value = eInsumos.Nombre
            command.ExecuteNonQuery()
            MyConmection.Close()
            Return 1
        Catch ex As Exception
            MyConmection.Close()
            Return 0
        End Try
    End Function

    Public Function ModificarInsumo(ByVal eInsumos As Insumos) As Integer
        Dim myCnnString As String
        Dim cnn As New cnnGenerate

        myCnnString = cnn.GeneradorcnnString(AppSettings("cnnString"))
        Dim MyConnection As New SqlConnection(myCnnString)

        Dim lsCommand As String = Resources.rsInsumos.ModificarInsumo
        Dim command As New SqlCommand(lsCommand, MyConnection)

        Try
            MyConnection.Open()
            command.Parameters.Add("@IdInsumo", SqlDbType.Int).Value = eInsumos.IdInsumo
            command.Parameters.Add("@Nombre", SqlDbType.VarChar).Value = eInsumos.Nombre
            command.ExecuteNonQuery()
            MyConnection.Close()
            Return 1

        Catch ex As Exception
            MyConnection.Close()
            Return 0

        End Try

    End Function

    Public Function RecuperarInsumoPorId(ByVal IdInsumo As String) As DataSet

        Dim myCnnString As String
        Dim cnn As New cnnGenerate

        myCnnString = cnn.GeneradorcnnString(AppSettings("cnnString"))
        Dim MyConmection As New SqlConnection(myCnnString)

        Dim mySelectText As String = Resources.rsInsumos.RecuperarInsumoPorId

        Dim myDataAdapter As New SqlDataAdapter(mySelectText, myCnnString)
        Dim ldsMiDataSet As New DataSet

        Try
            myDataAdapter.SelectCommand.Parameters.Add("@IdInsumo", SqlDbType.Int).Value = IdInsumo
            myDataAdapter.Fill(ldsMiDataSet)

        Catch ex As Exception

        End Try
        Return ldsMiDataSet

    End Function

    Public Function BuscarInsumosPorNombre(ByVal Nombre As String) As DataSet

        Dim myCnnString As String
        Dim cnn As New cnnGenerate

        myCnnString = cnn.GeneradorcnnString(AppSettings("cnnString"))
        Dim MyConmection As New SqlConnection(myCnnString)

        Dim mySelectText As String = Resources.rsInsumos.BuscarInsumosPorNombre

        Dim myDataAdapter As New SqlDataAdapter(mySelectText, myCnnString)
        Dim ldsMiDataSet As New DataSet

        Try
            myDataAdapter.SelectCommand.Parameters.Add("@Nombre", SqlDbType.VarChar).Value = Nombre
            myDataAdapter.Fill(ldsMiDataSet)

        Catch ex As Exception

        End Try
        Return ldsMiDataSet

    End Function


End Class
