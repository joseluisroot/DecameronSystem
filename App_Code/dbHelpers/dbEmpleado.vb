﻿Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports System.Data

Public Class dbEmpleado
    Public Function RecuperarEmpleado() As DataSet

        Dim myCnnString As String
        Dim mySelectText As String = Resources.rsEmpleado.RecuperarEmpleado

        Dim cnn As New cnnGenerate

        myCnnString = cnn.GeneradorcnnString(AppSettings("cnnString"))

        Dim myDataAdapter As New SqlDataAdapter(mySelectText, myCnnString)
        Dim ldsMiDataSet As New DataSet

        Try
            myDataAdapter.Fill(ldsMiDataSet)
        Catch ex As Exception


        End Try
        Return ldsMiDataSet

    End Function

    Public Function GuardarEmpleado(ByVal eEmpleado As Empleado) As Integer

        Dim myCnnString As String
        Dim cnn As New cnnGenerate

        myCnnString = cnn.GeneradorcnnString(AppSettings("cnnString"))

        Dim MyConnection As New SqlConnection(myCnnString)
        Dim lsCommandstring As String = Resources.rsEmpleado.GuardarEmpleado
        Dim command As New SqlCommand(lsCommandstring, MyConnection)

        Try
            MyConnection.Open()
            command.Parameters.Add("@Nombre", SqlDbType.VarChar).Value = eEmpleado.Nombre
            command.Parameters.Add("@Apellido", SqlDbType.VarChar).Value = eEmpleado.Apellido
            command.Parameters.Add("@Usuario", SqlDbType.VarChar).Value = eEmpleado.Usuario
            command.Parameters.Add("@Clave", SqlDbType.VarChar).Value = eEmpleado.Clave
            command.Parameters.Add("@IdEstado", SqlDbType.Int).Value = eEmpleado.IdEstado
            command.Parameters.Add("@IdPerfil", SqlDbType.Int).Value = eEmpleado.IdPerfil
            command.ExecuteNonQuery()
            MyConnection.Close()
            Return 1
        Catch ex As Exception
            MyConnection.Close()
            Return 0
        End Try
    End Function

    Public Function ModificarEmpleado(ByVal eEmpleado As Empleado) As Integer
        Dim myCnnString As String
        Dim cnn As New cnnGenerate

        myCnnString = cnn.GeneradorcnnString(AppSettings("cnnString"))
        Dim MyConnection As New SqlConnection(myCnnString)

        Dim lsCommand As String = Resources.rsEmpleado.ModificarEmpleado
        Dim command As New SqlCommand(lsCommand, MyConnection)

        Try
            MyConnection.Open()
            command.Parameters.Add("@IdEmpleado", SqlDbType.Int).Value = eEmpleado.IdEmpleado
            command.Parameters.Add("@Nombre", SqlDbType.VarChar).Value = eEmpleado.Nombre
            command.Parameters.Add("@Apellido", SqlDbType.VarChar).Value = eEmpleado.Apellido
            command.Parameters.Add("@Usuario", SqlDbType.VarChar).Value = eEmpleado.Usuario
            command.Parameters.Add("@Clave", SqlDbType.VarChar).Value = eEmpleado.Clave
            command.Parameters.Add("@IdEstado", SqlDbType.Int).Value = eEmpleado.IdEstado
            command.Parameters.Add("@IdPerfil", SqlDbType.Int).Value = eEmpleado.IdPerfil
            command.ExecuteNonQuery()
            MyConnection.Close()
            Return 1

        Catch ex As Exception
            MyConnection.Close()
            Return 0

        End Try

    End Function

    Public Function RecuperarEmpleadoPorId(ByVal IdEmpleado As String) As DataSet

        Dim myCnnString As String
        Dim cnn As New cnnGenerate

        myCnnString = cnn.GeneradorcnnString(AppSettings("cnnString"))
        Dim MyConnection As New SqlConnection(myCnnString)

        Dim mySelectText As String = Resources.rsEmpleado.RecuperarEmpleadoPorId


        Dim myDataAdapter As New SqlDataAdapter(mySelectText, myCnnString)
        Dim ldsMiDataSet As New DataSet

        Try
            myDataAdapter.SelectCommand.Parameters.Add("@IdEmpleado", SqlDbType.Int).Value = IdEmpleado
            myDataAdapter.Fill(ldsMiDataSet)

        Catch ex As Exception

        End Try
        Return ldsMiDataSet

    End Function

    Public Function BuscarEmpleadoPorNombre(ByVal Nombre As String) As DataSet

        Dim myCnnString As String
        Dim cnn As New cnnGenerate

        myCnnString = cnn.GeneradorcnnString(AppSettings("cnnString"))
        Dim MyConnection As New SqlConnection(myCnnString)

        Dim mySelectText As String = Resources.rsEmpleado.BuscarEmpleadoPorNombre

        Dim myDataAdapter As New SqlDataAdapter(mySelectText, myCnnString)
        Dim ldsMiDataSet As New DataSet

        Try
            myDataAdapter.SelectCommand.Parameters.Add("@Nombre", SqlDbType.VarChar).Value = Nombre
            myDataAdapter.Fill(ldsMiDataSet)

        Catch ex As Exception

        End Try
        Return ldsMiDataSet

    End Function
End Class
