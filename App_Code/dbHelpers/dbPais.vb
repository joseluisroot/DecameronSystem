﻿Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports System.Data

Public Class dbPais

    Public Function RecuperarPais() As DataSet

        Dim myCnnString As String
        Dim mySelectText As String = Resources.rsPais.BuscarPais


        Dim cnn As New cnnGenerate

        myCnnString = cnn.GeneradorcnnString(AppSettings("cnnString"))

        Dim myDataAdapter As New SqlDataAdapter(mySelectText, myCnnString)
        Dim ldsMiDataSet As New DataSet

        Try
            myDataAdapter.Fill(ldsMiDataSet)
        Catch ex As Exception


        End Try
        Return ldsMiDataSet

    End Function

    Public Function GuardarMobiliario(ByVal eMobiliario As Mobiliario) As Integer

        Dim myCnnString As String
        Dim cnn As New cnnGenerate

        myCnnString = cnn.GeneradorcnnString(AppSettings("cnnString"))
        Dim MyConmection As New SqlConnection(myCnnString)

        Dim lsCommandstring As String = Resources.rsMobiliario.GuardarMobiliario

        Dim command As New SqlCommand(lsCommandstring, MyConmection)

        Try
            MyConmection.Open()
            command.Parameters.Add("@Nombre", SqlDbType.VarChar).Value = eMobiliario.Nombre
            command.ExecuteNonQuery()
            MyConmection.Close()
            Return 1
        Catch ex As Exception
            MyConmection.Close()
            Return 0
        End Try
    End Function

    Public Function ModificarMobiliario(ByVal eMobiliario As Mobiliario) As Integer
        Dim myCnnString As String
        Dim cnn As New cnnGenerate

        myCnnString = cnn.GeneradorcnnString(AppSettings("cnnString"))
        Dim MyConnection As New SqlConnection(myCnnString)

        Dim lsCommand As String = Resources.rsMobiliario.ModificarMobiliario
        Dim command As New SqlCommand(lsCommand, MyConnection)

        Try
            MyConnection.Open()
            command.Parameters.Add("@IdMobiliario", SqlDbType.Int).Value = eMobiliario.IdMobiliario
            command.Parameters.Add("@Nombre", SqlDbType.VarChar).Value = eMobiliario.Nombre

            command.ExecuteNonQuery()
            MyConnection.Close()
            Return 1

        Catch ex As Exception
            MyConnection.Close()
            Return 0

        End Try

    End Function

    Public Function RecuperarMobiliarioPorId(ByVal IdMobiliario As String) As DataSet

        Dim myCnnString As String
        Dim cnn As New cnnGenerate

        myCnnString = cnn.GeneradorcnnString(AppSettings("cnnString"))
        Dim MyConmection As New SqlConnection(myCnnString)

        Dim mySelectText As String = Resources.rsMobiliario.RecuperarMobiliarioPorId

        Dim myDataAdapter As New SqlDataAdapter(mySelectText, myCnnString)
        Dim ldsMiDataSet As New DataSet

        Try
            myDataAdapter.SelectCommand.Parameters.Add("@IdMobiliario", SqlDbType.Int).Value = IdMobiliario
            myDataAdapter.Fill(ldsMiDataSet)

        Catch ex As Exception

        End Try
        Return ldsMiDataSet

    End Function

    Public Function BuscarPaisNombre(ByVal Nombre As String) As DataSet

        Dim myCnnString As String
        Dim cnn As New cnnGenerate

        myCnnString = cnn.GeneradorcnnString(AppSettings("cnnString"))
        Dim MyConmection As New SqlConnection(myCnnString)

        Dim mySelectText As String = Resources.rsPais.BuscarPais



        Dim myDataAdapter As New SqlDataAdapter(mySelectText, myCnnString)
        Dim ldsMiDataSet As New DataSet

        Try
            myDataAdapter.SelectCommand.Parameters.Add("@Nombre", SqlDbType.VarChar).Value = Nombre
            myDataAdapter.Fill(ldsMiDataSet)

        Catch ex As Exception

        End Try
        Return ldsMiDataSet

    End Function


End Class
