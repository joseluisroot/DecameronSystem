﻿Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports System.Data

Public Class dbTipoPago

    Public Function RecuperarTipoPago() As DataSet

        Dim myCnnString As String
        Dim mySelectText As String = Resources.rsTipoPago.RecuperarTipoPago

        Dim cnn As New cnnGenerate
        myCnnString = cnn.GeneradorcnnString(AppSettings("cnnString"))

        Dim myDataAdapter As New SqlDataAdapter(mySelectText, myCnnString)

        Dim ldsMiDataSet As New DataSet

        Try

            myDataAdapter.Fill(ldsMiDataSet)

        Catch ex As Exception

        End Try

        Return ldsMiDataSet

    End Function

    Public Function GuardarTipoPago(ByVal eTipoPago As TipoPago) As Integer

        Dim myCnnString As String
        Dim cnn As New cnnGenerate

        myCnnString = cnn.GeneradorcnnString(AppSettings("cnnString"))
        Dim myConnection As New SqlConnection(myCnnString)

        Dim lsCommandstring As String = Resources.rsTipoPago.GuardarTipoPago

        Dim command As New SqlCommand(lsCommandstring, myConnection)

        Try
            myConnection.Open()

            command.Parameters.Add("@nombreTipoPago", SqlDbType.VarChar).Value = eTipoPago.nombreTipoPago

            command.ExecuteNonQuery()
            myConnection.Close()

            Return 1

        Catch ex As Exception
            myConnection.Close()
            Return 0
        End Try

    End Function

    Public Function RecuperarTipoPagoPorId(ByVal IdTipoPago As String) As DataSet

        Dim myCnnString As String
        Dim cnn As New cnnGenerate

        myCnnString = cnn.GeneradorcnnString(AppSettings("cnnString"))
        Dim myConnection As New SqlConnection(myCnnString)


        Dim mySelectText As String = Resources.rsTipoPago.RecuperarTipoPagoPorId

        Dim myDataAdapter As New SqlDataAdapter(mySelectText, myCnnString)
        Dim ldsMiDataSet As New DataSet

        Try

            myDataAdapter.SelectCommand.Parameters.Add("@IdTipoPago", SqlDbType.Int).Value = IdTipoPago
            myDataAdapter.Fill(ldsMiDataSet)

        Catch ex As Exception



        End Try

        Return ldsMiDataSet
    End Function

    Public Function BuscarTipoPagoPorNombre(ByVal nombreTipoPago As String) As DataSet

        Dim myCnnString As String
        Dim cnn As New cnnGenerate

        myCnnString = cnn.GeneradorcnnString(AppSettings("cnnString"))
        Dim myConnection As New SqlConnection(myCnnString)

        Dim mySelectText As String = Resources.rsTipoPago.BuscarTipoPagoPorNombre

        Dim myDataAdapter As New SqlDataAdapter(mySelectText, myCnnString)
        Dim ldsMiDataSet As New DataSet

        Try

            myDataAdapter.SelectCommand.Parameters.Add("@nombreTipoPago", SqlDbType.VarChar).Value = nombreTipoPago
            myDataAdapter.Fill(ldsMiDataSet)

        Catch ex As Exception

        End Try

        Return ldsMiDataSet

    End Function

    Public Function ModificarTipoPago(ByVal eTipoPago As TipoPago) As Integer


        Dim myCnnString As String
        Dim cnn As New cnnGenerate

        myCnnString = cnn.GeneradorcnnString(AppSettings("cnnString"))
        Dim myConnection As New SqlConnection(myCnnString)

        Dim lsCommandstring As String = Resources.rsTipoPago.ModificarTipoPago
        Dim command As New SqlCommand(lsCommandstring, myConnection)

        Try
            myConnection.Open()

            command.Parameters.Add("@IdTipoPago", SqlDbType.Int).Value = eTipoPago.IdTipoPago
            command.Parameters.Add("@nombreTipoPago", SqlDbType.VarChar).Value = eTipoPago.nombreTipoPago

            command.ExecuteNonQuery()
            myConnection.Close()

            Return 1

        Catch ex As Exception
            myConnection.Close()
            Return 0
        End Try

    End Function



End Class
