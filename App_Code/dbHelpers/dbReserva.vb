﻿Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports System.Data

Public Class dbReserva

    Public Function RecuperarReserva() As DataSet

        Dim myCnnString As String
        Dim mySelectText As String = Resources.rsReserva.RecuperarReserva

        Dim cnn As New cnnGenerate
        myCnnString = cnn.GeneradorcnnString(AppSettings("cnnString"))

        Dim myDataAdapter As New SqlDataAdapter(mySelectText, myCnnString)
        Dim ldsMiDataSet As New DataSet

        Try
            myDataAdapter.Fill(ldsMiDataSet)

        Catch ex As Exception

        End Try

        Return ldsMiDataSet

    End Function

    Public Function GuardarReserva(ByVal eReserva As Reserva, ByVal eCliente As Cliente) As Integer
        Dim myCnnString As String
        Dim cnn As New cnnGenerate

        myCnnString = cnn.GeneradorcnnString(AppSettings("cnnString"))
        Dim myConnection As New SqlConnection(myCnnString)

        myConnection.Open()

        Dim myTrans As SqlTransaction = myConnection.BeginTransaction()
        Dim Command As SqlCommand = myConnection.CreateCommand()

        Dim newId As Integer
        Dim habDips As Integer
        Dim dsDatos As New DataSet

        Try
            Command.Transaction = myTrans
            Command.CommandText = Resources.rsCliente.GuardarCliente

            Command.Parameters.Add("@Nombre", SqlDbType.VarChar).Value = eCliente.NombreCliente
            Command.Parameters.Add("@Apellidos", SqlDbType.VarChar).Value = eCliente.ApellidosCliente
            Command.Parameters.Add("@Edad", SqlDbType.Int).Value = eCliente.Edad
            Command.Parameters.Add("@FechaIngreso", SqlDbType.DateTime).Value = eCliente.FechaInfreso
            Command.Parameters.Add("@Direccion", SqlDbType.VarChar).Value = eCliente.Direccion
            Command.Parameters.Add("@Telefono", SqlDbType.Int).Value = eCliente.Telefono
            Command.Parameters.Add("@Sexo", SqlDbType.VarChar).Value = eCliente.Sexo
            Command.Parameters.Add("@Email", SqlDbType.VarChar).Value = eCliente.Email
            Command.ExecuteNonQuery()
            Command.Parameters.Clear()

            Command.CommandText = "SELECT MAX(IdCliente) AS Id FROM Cliente"
            newId = Command.ExecuteScalar()
            Command.Parameters.Clear()

            Command.CommandText = Resources.rsReserva.GuardarReserva

            Command.Parameters.Add("@IdCliente", SqlDbType.VarChar).Value = newId
            Command.Parameters.Add("@IdPlan", SqlDbType.VarChar).Value = eReserva.IdPlan
            Command.Parameters.Add("@ocupantes", SqlDbType.Int).Value = eReserva.ocupantes
            Command.Parameters.Add("@fecha_inicio", SqlDbType.DateTime).Value = Date.Now 'eReserva.fecha_inicio
            Command.Parameters.Add("@fecha_final", SqlDbType.VarChar).Value = Date.Now.AddDays(2) 'eReserva.fecha_final
            Command.ExecuteNonQuery()
            Command.Parameters.Clear()

            Command.CommandText = "SELECT TOP 1 * FROM Habitacion WHERE estado_habitacion= '3'"
            habDips = Command.ExecuteScalar()
            Command.Parameters.Clear()

            Command.CommandText = "UPDATE Habitacion SET estado_habitacion = '4' WHERE IdHabitacion=" & habDips
            'Command.Parameters.Add("@IdHabitacion", SqlDbType.VarChar).Value = habDips
            'Command.Parameters.Add("@estado_habitacion", SqlDbType.VarChar).Value = "4"
            Command.ExecuteNonQuery()
            Command.Parameters.Clear()

            myTrans.Commit()
            myConnection.Close()
            Return 1
        Catch ex As Exception
            myTrans.Rollback()
            myConnection.Close()
            MsgBox(ex.ToString, MsgBoxStyle.Exclamation, "Sistema Decameron")
            MsgBox("Error al guardar la información", MsgBoxStyle.Exclamation, "Sistema Decameron")
            Return 0
        End Try
    End Function

    Public Function ModificarReserva(ByVal eReserva As Reserva, ByVal eCliente As Cliente) As Integer
        Dim myCnnString As String
        Dim cnn As New cnnGenerate

        myCnnString = cnn.GeneradorcnnString(AppSettings("cnnString"))
        Dim myConnection As New SqlConnection(myCnnString)

        myConnection.Open()

        Dim myTrans As SqlTransaction = myConnection.BeginTransaction()
        Dim Command As SqlCommand = myConnection.CreateCommand()
        Dim lsCommandstring As String = Resources.rsCliente.GuardarCliente

        Dim dsDatos As New DataSet

        Try
            Command.Transaction = myTrans
            Command.CommandText = Resources.rsCliente.ModificarCliente

            Command.Parameters.Add("@IdCliente", SqlDbType.VarChar).Value = eCliente.IdCliente
            Command.Parameters.Add("@Nombre", SqlDbType.VarChar).Value = eCliente.NombreCliente
            Command.Parameters.Add("@Apellidos", SqlDbType.VarChar).Value = eCliente.ApellidosCliente
            Command.Parameters.Add("@Edad", SqlDbType.Int).Value = eCliente.Edad
            Command.Parameters.Add("@FechaIngreso", SqlDbType.DateTime).Value = eCliente.FechaInfreso
            Command.Parameters.Add("@Direccion", SqlDbType.VarChar).Value = eCliente.Direccion
            Command.Parameters.Add("@Telefono", SqlDbType.Int).Value = eCliente.Telefono
            Command.Parameters.Add("@Sexo", SqlDbType.VarChar).Value = eCliente.Sexo
            Command.Parameters.Add("@Email", SqlDbType.VarChar).Value = eCliente.Email
            Command.ExecuteNonQuery()
            Command.Parameters.Clear()

            Command.CommandText = Resources.rsReserva.GuardarReserva

            Command.Parameters.Add("@idReserva", SqlDbType.VarChar).Value = eReserva.IdReserva
            Command.Parameters.Add("@IdPlan", SqlDbType.VarChar).Value = eReserva.IdPlan
            Command.Parameters.Add("@ocupantes", SqlDbType.Int).Value = eReserva.ocupantes
            Command.Parameters.Add("@fecha_inicio", SqlDbType.DateTime).Value = Date.Now  'eReserva.fecha_inicio
            Command.Parameters.Add("@fecha_final", SqlDbType.VarChar).Value = Date.Now.AddDays(2) 'eReserva.fecha_final
            Command.ExecuteNonQuery()

            myTrans.Commit()
            myConnection.Close()
            Return 1

        Catch ex As Exception
            myTrans.Rollback()
            myConnection.Close()
            Return 0
        End Try

    End Function

    Public Function RecuperarReservaPorId(ByVal idValor As String) As DataSet
        Dim myCnnString As String
        Dim cnn As New cnnGenerate

        myCnnString = cnn.GeneradorcnnString(AppSettings("cnnString"))
        Dim myConnection As New SqlConnection(myCnnString)

        Dim mySelectText As String = Resources.rsReserva.RecuperarReservaPorId

        Dim myDataAdapter As New SqlDataAdapter(mySelectText, myCnnString)
        Dim ldsMiDataSet As New DataSet

        Try
            myDataAdapter.SelectCommand.Parameters.Add("@idReserva", SqlDbType.VarChar).Value = idValor
            myDataAdapter.Fill(ldsMiDataSet)

        Catch ex As Exception

        End Try

        Return ldsMiDataSet
    End Function

    Public Function RecuperarReservaCliente() As DataSet

        Dim myCnnString As String
        Dim mySelectText As String = Resources.rsReserva.RecuperarReservaCliente

        Dim cnn As New cnnGenerate
        myCnnString = cnn.GeneradorcnnString(AppSettings("cnnString"))

        Dim myDataAdapter As New SqlDataAdapter(mySelectText, myCnnString)
        Dim ldsMiDataSet As New DataSet

        Try
            myDataAdapter.Fill(ldsMiDataSet)

        Catch ex As Exception

        End Try

        Return ldsMiDataSet

    End Function

    Public Function RecuperarReservaClientePorId(ByVal idValor As String) As DataSet
        Dim myCnnString As String
        Dim cnn As New cnnGenerate

        myCnnString = cnn.GeneradorcnnString(AppSettings("cnnString"))
        Dim myConnection As New SqlConnection(myCnnString)

        Dim mySelectText As String = Resources.rsReserva.RecuperarReservaClientePorId

        Dim myDataAdapter As New SqlDataAdapter(mySelectText, myCnnString)
        Dim ldsMiDataSet As New DataSet

        Try
            myDataAdapter.SelectCommand.Parameters.Add("@idReserva", SqlDbType.VarChar).Value = idValor
            myDataAdapter.Fill(ldsMiDataSet)

        Catch ex As Exception

        End Try

        Return ldsMiDataSet
    End Function

    Public Function BuscarReservaPorNombre(ByVal Nombre As String) As DataSet

        Dim myCnnString As String
        Dim cnn As New cnnGenerate

        myCnnString = cnn.GeneradorcnnString(AppSettings("cnnString"))
        Dim MyConnection As New SqlConnection(myCnnString)
        Dim mySelectText As String = Resources.rsReserva.BuscarReservaClientePorNombre
        Dim myDataAdapter As New SqlDataAdapter(mySelectText, myCnnString)
        Dim ldsMiDataSet As New DataSet

        Try
            myDataAdapter.SelectCommand.Parameters.Add("@nombre", SqlDbType.VarChar).Value = Nombre
            myDataAdapter.Fill(ldsMiDataSet)

        Catch ex As Exception

        End Try
        Return ldsMiDataSet

    End Function
End Class
