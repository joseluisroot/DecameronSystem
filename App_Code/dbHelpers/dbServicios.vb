﻿Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports System.Data

Public Class dbServicios

    Public Function RecuperarServicios() As DataSet

        Dim myCnnString As String
        Dim mySelectText As String = Resources.rsServicios.RecuperarServicios

        Dim cnn As New cnnGenerate

        myCnnString = cnn.GeneradorcnnString(AppSettings("cnnString"))
        Dim myDataAdapter As New SqlDataAdapter(mySelectText, myCnnString)
        Dim ldsMiDataSet As New DataSet

        Try
            myDataAdapter.Fill(ldsMiDataSet)
        Catch ex As Exception


        End Try
        Return ldsMiDataSet

    End Function

    Public Function GuardarServicios(ByVal eServicios As Servicios) As Integer

        Dim myCnnString As String
        Dim cnn As New cnnGenerate

        myCnnString = cnn.GeneradorcnnString(AppSettings("cnnString"))
        Dim MyConnection As New SqlConnection(myCnnString)
        Dim lsCommandstring As String = Resources.rsServicios.GuardarServicios
        Dim command As New SqlCommand(lsCommandstring, MyConnection)

        Try
            MyConnection.Open()
            command.Parameters.Add("@Nombre", SqlDbType.VarChar).Value = eServicios.Nombre
            command.Parameters.Add("@precio", SqlDbType.Decimal).Value = eServicios.precio
            command.ExecuteNonQuery()
            MyConnection.Close()
            Return 1
        Catch ex As Exception
            MyConnection.Close()
            Return 0
        End Try
    End Function

    Public Function ModificarServicios(ByVal eServicios As Servicios) As Integer
        Dim myCnnString As String
        Dim cnn As New cnnGenerate

        myCnnString = cnn.GeneradorcnnString(AppSettings("cnnString"))
        Dim MyConnection As New SqlConnection(myCnnString)
        Dim lsCommand As String = Resources.rsServicios.ModificarServicio
        Dim command As New SqlCommand(lsCommand, MyConnection)

        Try
            MyConnection.Open()
            command.Parameters.Add("@IdServicio", SqlDbType.Int).Value = eServicios.IdServicio
            command.Parameters.Add("@Nombre", SqlDbType.VarChar).Value = eServicios.Nombre
            command.Parameters.Add("@precio", SqlDbType.Decimal).Value = eServicios.precio
            command.ExecuteNonQuery()
            MyConnection.Close()
            Return 1

        Catch ex As Exception
            MyConnection.Close()
            Return 0

        End Try

    End Function

    Public Function RecuperarServiciosPorId(ByVal IdServicio As Integer) As DataSet

        Dim myCnnString As String
        Dim cnn As New cnnGenerate

        myCnnString = cnn.GeneradorcnnString(AppSettings("cnnString"))
        Dim MyConnection As New SqlConnection(myCnnString)
        Dim mySelectText As String = Resources.rsServicios.RecuperarServiciosPorId
        Dim myDataAdapter As New SqlDataAdapter(mySelectText, myCnnString)
        Dim ldsMiDataSet As New DataSet

        Try
            myDataAdapter.SelectCommand.Parameters.Add("@IdServicio", SqlDbType.Int).Value = IdServicio
            myDataAdapter.Fill(ldsMiDataSet)

        Catch ex As Exception

        End Try
        Return ldsMiDataSet

    End Function

    Public Function BuscarServiciosPorNombre(ByVal Nombre As String) As DataSet

        Dim myCnnString As String
        Dim cnn As New cnnGenerate

        myCnnString = cnn.GeneradorcnnString(AppSettings("cnnString"))
        Dim MyConnection As New SqlConnection(myCnnString)
        Dim mySelectText As String = Resources.rsServicios.BuscarServicioPorNombre
        Dim myDataAdapter As New SqlDataAdapter(mySelectText, myCnnString)
        Dim ldsMiDataSet As New DataSet

        Try
            myDataAdapter.SelectCommand.Parameters.Add("@Nombre", SqlDbType.VarChar).Value = Nombre
            myDataAdapter.Fill(ldsMiDataSet)

        Catch ex As Exception

        End Try
        Return ldsMiDataSet

    End Function
End Class
