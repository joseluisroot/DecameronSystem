﻿Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports System.Data

Public Class dbBodega

    Public Function RecuperarBodega() As DataSet

        Dim myCnnString As String
        Dim mySelectText As String = Resources.rsBodega.RecuperarBodega

        Dim cnn As New cnnGenerate

        myCnnString = cnn.GeneradorcnnString(AppSettings("cnnString"))

        Dim myDataAdapter As New SqlDataAdapter(mySelectText, myCnnString)
        Dim ldsMiDataSet As New DataSet

        Try
            myDataAdapter.Fill(ldsMiDataSet)
        Catch ex As Exception


        End Try
        Return ldsMiDataSet

    End Function

    Public Function GuardarBodega(ByVal eBodega As Bodega) As Integer

        Dim myCnnString As String
        Dim cnn As New cnnGenerate

        myCnnString = cnn.GeneradorcnnString(AppSettings("cnnString"))
        Dim MyConnection As New SqlConnection(myCnnString)
        Dim lsCommandstring As String = Resources.rsBodega.GuardarBodega
        Dim command As New SqlCommand(lsCommandstring, MyConnection)

        Try
            MyConnection.Open()
            command.Parameters.Add("@NombreBodega", SqlDbType.VarChar).Value = eBodega.NombreBodega
            command.ExecuteNonQuery()
            MyConnection.Close()
            Return 1
        Catch ex As Exception
            MyConnection.Close()
            Return 0
        End Try
    End Function

    Public Function ModificarBodega(ByVal eBodega As Bodega) As Integer
        Dim myCnnString As String
        Dim cnn As New cnnGenerate

        myCnnString = cnn.GeneradorcnnString(AppSettings("cnnString"))
        Dim MyConnection As New SqlConnection(myCnnString)
        Dim lsCommand As String = Resources.rsBodega.ModificarBodega
        Dim command As New SqlCommand(lsCommand, MyConnection)

        Try
            MyConnection.Open()
            command.Parameters.Add("@NombreBodega", SqlDbType.VarChar).Value = eBodega.NombreBodega
            command.ExecuteNonQuery()
            MyConnection.Close()
            Return 1

        Catch ex As Exception
            MyConnection.Close()
            Return 0

        End Try

    End Function

    Public Function RecuperarBodegaPorId(ByVal IdBodega As Integer) As DataSet

        Dim myCnnString As String
        Dim cnn As New cnnGenerate

        myCnnString = cnn.GeneradorcnnString(AppSettings("cnnString"))
        Dim MyConnection As New SqlConnection(myCnnString)
        Dim mySelectText As String = Resources.rsBodega.RecuperarBodegaPorId
        Dim myDataAdapter As New SqlDataAdapter(mySelectText, myCnnString)
        Dim ldsMiDataSet As New DataSet

        Try
            myDataAdapter.SelectCommand.Parameters.Add("@IdBodeba", SqlDbType.Int).Value = IdBodega
            myDataAdapter.Fill(ldsMiDataSet)

        Catch ex As Exception

        End Try
        Return ldsMiDataSet

    End Function

    Public Function BuscarBodegaPorNombre(ByVal NombreBodega As String) As DataSet

        Dim myCnnString As String
        Dim cnn As New cnnGenerate

        myCnnString = cnn.GeneradorcnnString(AppSettings("cnnString"))
        Dim MyConnection As New SqlConnection(myCnnString)
        Dim mySelectText As String = Resources.rsBodega.BuscarBodegaPorNombre
        Dim myDataAdapter As New SqlDataAdapter(mySelectText, myCnnString)
        Dim ldsMiDataSet As New DataSet

        Try
            myDataAdapter.SelectCommand.Parameters.Add("@NombreBodega", SqlDbType.VarChar).Value = NombreBodega
            myDataAdapter.Fill(ldsMiDataSet)

        Catch ex As Exception

        End Try
        Return ldsMiDataSet

    End Function
End Class
