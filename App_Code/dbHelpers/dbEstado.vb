﻿Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports System.Data

Public Class dbEstado

    Public Function RecuperarEstados() As DataSet

        Dim myCnnString As String
        Dim mySelectText As String = Resources.rsEstado.RecuperarEstados

        Dim cnn As New cnnGenerate
        myCnnString = cnn.GeneradorcnnString(AppSettings("cnnString"))

        Dim myDataAdapter As New SqlDataAdapter(mySelectText, myCnnString)

        Dim ldsMiDataSet As New DataSet

        Try

            myDataAdapter.Fill(ldsMiDataSet)

        Catch ex As Exception

        End Try

        Return ldsMiDataSet

    End Function

    Public Function GuardarEstado(ByVal eEntidad As Estado) As Integer

        Dim myCnnString As String
        Dim cnn As New cnnGenerate

        myCnnString = cnn.GeneradorcnnString(AppSettings("cnnString"))
        Dim myConnection As New SqlConnection(myCnnString)

        Dim lsCommandstring As String = Resources.rsEstado.GuardarEstado

        Dim command As New SqlCommand(lsCommandstring, myConnection)

        Try
            myConnection.Open()

            command.Parameters.Add("@NombreEstado", SqlDbType.VarChar).Value = eEntidad.NombreEstado

            command.ExecuteNonQuery()
            myConnection.Close()

            Return 1

        Catch ex As Exception
            myConnection.Close()
            Return 0
        End Try

    End Function

    Public Function RecuperarEstadoPorId(ByVal IdEstado As String) As DataSet

        Dim myCnnString As String
        Dim cnn As New cnnGenerate

        myCnnString = cnn.GeneradorcnnString(AppSettings("cnnString"))
        Dim myConnection As New SqlConnection(myCnnString)


        Dim mySelectText As String = Resources.rsEstado.RecuperarEstadoPorId

        Dim myDataAdapter As New SqlDataAdapter(mySelectText, myCnnString)
        Dim ldsMiDataSet As New DataSet

        Try

            myDataAdapter.SelectCommand.Parameters.Add("@IdEstado", SqlDbType.Int).Value = IdEstado
            myDataAdapter.Fill(ldsMiDataSet)

        Catch ex As Exception



        End Try

        Return ldsMiDataSet
    End Function

    Public Function BuscarEstadoPorNombre(ByVal NombreEstado As String) As DataSet

        Dim myCnnString As String
        Dim cnn As New cnnGenerate

        myCnnString = cnn.GeneradorcnnString(AppSettings("cnnString"))
        Dim myConnection As New SqlConnection(myCnnString)

        Dim mySelectText As String = Resources.rsEstado.BuscarEstadoPorNombre

        Dim myDataAdapter As New SqlDataAdapter(mySelectText, myCnnString)
        Dim ldsMiDataSet As New DataSet

        Try

            myDataAdapter.SelectCommand.Parameters.Add("@NombreEstado", SqlDbType.VarChar).Value = NombreEstado
            myDataAdapter.Fill(ldsMiDataSet)

        Catch ex As Exception

        End Try

        Return ldsMiDataSet

    End Function

    Public Function ModificarEstado(ByVal eEntidad As Estado) As Integer


        Dim myCnnString As String
        Dim cnn As New cnnGenerate

        myCnnString = cnn.GeneradorcnnString(AppSettings("cnnString"))
        Dim myConnection As New SqlConnection(myCnnString)

        Dim lsCommandstring As String = Resources.rsEstado.ModificarEstado
        Dim command As New SqlCommand(lsCommandstring, myConnection)

        Try
            myConnection.Open()

            command.Parameters.Add("@IdEstado", SqlDbType.Int).Value = eEntidad.IdEstado
            command.Parameters.Add("@NombreEstado", SqlDbType.VarChar).Value = eEntidad.NombreEstado

            command.ExecuteNonQuery()
            myConnection.Close()

            Return 1

        Catch ex As Exception
            myConnection.Close()
            Return 0
        End Try

    End Function



End Class
