﻿Imports Microsoft.VisualBasic

Public Class Plan
#Region "privadas"
    Dim _IdPlan As Integer
    Dim _IdTipoHabitacion As Integer
    Dim _Nombre_plan As String
    Dim _Valor_plan As Decimal
    Dim _Valor_descuento As Decimal
    Dim _Valor_real As Decimal
    Dim _monto_iva As Decimal
    Dim _Valor_total As Decimal
#End Region

#Region "Publicas"
    Public Property IdPlan() As Integer
        Get
            Return _IdPlan
        End Get
        Set(ByVal value As Integer)
            _IdPlan = value
        End Set
    End Property

    Public Property IdTipoHabitacion() As Integer
        Get
            Return _IdTipoHabitacion
        End Get
        Set(ByVal value As Integer)
            _IdTipoHabitacion = value
        End Set
    End Property

    Public Property Nombre_plan() As String
        Get
            Return _Nombre_plan
        End Get
        Set(ByVal value As String)
            _Nombre_plan = value
        End Set
    End Property

    Public Property Valor_plan() As Decimal
        Get
            Return _Valor_plan
        End Get
        Set(ByVal value As Decimal)
            _Valor_plan = value
        End Set
    End Property

    Public Property Valor_descuento() As Decimal
        Get
            Return _Valor_descuento
        End Get
        Set(ByVal value As Decimal)
            _Valor_descuento = value
        End Set
    End Property

    Public Property Valor_real() As Decimal
        Get
            Return _Valor_real
        End Get
        Set(ByVal value As Decimal)
            _Valor_real = value
        End Set
    End Property

    Public Property monto_iva() As Decimal
        Get
            Return _monto_iva
        End Get
        Set(ByVal value As Decimal)
            _monto_iva = value
        End Set
    End Property

    Public Property Valor_total() As Decimal
        Get
            Return _Valor_total
        End Get
        Set(ByVal value As Decimal)
            _Valor_total = value
        End Set
    End Property
#End Region
End Class
