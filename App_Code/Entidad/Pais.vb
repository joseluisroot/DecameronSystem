﻿Imports Microsoft.VisualBasic

Public Class Pais
#Region "privadas"
    Dim _PaisCodigo As String
    Dim _PaisNombre As String

#End Region
#Region "publicas"
    Public Property PaisCodigo() As String

        Get
            Return _PaisCodigo

        End Get
        Set(ByVal value As String)
            _PaisCodigo = value
        End Set
    End Property
    Public Property PaisNombre() As String
        Get
            Return _PaisNombre
        End Get
        Set(ByVal value As String)
            _PaisNombre = value
        End Set
    End Property

#End Region

End Class
