﻿Imports Microsoft.VisualBasic

Public Class Empleado
#Region "privadas"

    Dim _IdEmpleado As Integer
    Dim _Nombre As String
    Dim _Apellido As String
    Dim _Usuario As String
    Dim _Clave As String
    Dim _IdEstado As Integer
    Dim _IdPerfil As Integer

#End Region

#Region "Publicas"
    Public Property IdEmpleado() As Integer
        Get
            Return _IdEmpleado
        End Get
        Set(ByVal value As Integer)
            _IdEmpleado = value
        End Set
    End Property

    Public Property Nombre() As String
        Get
            Return _Nombre
        End Get
        Set(ByVal value As String)
            _Nombre = value
        End Set
    End Property

    Public Property Apellido() As String
        Get
            Return _Apellido
        End Get
        Set(ByVal value As String)
            _Apellido = value
        End Set
    End Property

    Public Property Usuario() As String
        Get
            Return _Usuario
        End Get
        Set(ByVal value As String)
            _Usuario = value
        End Set
    End Property

    Public Property Clave() As String
        Get
            Return _Clave
        End Get
        Set(ByVal value As String)
            _Clave = value
        End Set
    End Property

    Public Property IdEstado() As Integer
        Get
            Return _IdEstado
        End Get
        Set(ByVal value As Integer)
            _IdEstado = value
        End Set
    End Property

    Public Property IdPerfil() As Integer
        Get
            Return _IdPerfil
        End Get
        Set(ByVal value As Integer)
            _IdPerfil = value
        End Set
    End Property
#End Region
End Class
