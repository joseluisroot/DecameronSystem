﻿Imports Microsoft.VisualBasic

Public Class Habitacion
#Region "privadas"

    Dim _IdHabitacion As Integer
    Dim _NombreHabitacion As String
    Dim _Precio As Double
    Dim _IdTipoHabitacion As Integer
    Dim _Descuento As Decimal

#End Region

#Region "Publicas"
    Public Property IdHabitacion() As Integer
        Get
            Return _IdHabitacion
        End Get
        Set(ByVal value As Integer)
            _IdHabitacion = value
        End Set
    End Property

    Public Property Precio() As Double
        Get
            Return _Precio
        End Get
        Set(ByVal value As Double)
            _Precio = value
        End Set
    End Property

    Public Property NombreHabitacion() As String
        Get
            Return _NombreHabitacion
        End Get
        Set(ByVal value As String)
            _NombreHabitacion = value
        End Set
    End Property

    Public Property IdTipoHabitacion() As Integer
        Get
            Return _IdTipoHabitacion
        End Get
        Set(ByVal value As Integer)
            _IdTipoHabitacion = value
        End Set
    End Property

    Public Property Descuento() As Decimal
        Get
            Return _Descuento
        End Get
        Set(ByVal value As Decimal)
            _Descuento = value
        End Set
    End Property
#End Region
End Class
