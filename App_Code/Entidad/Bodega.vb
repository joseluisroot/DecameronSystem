﻿Imports Microsoft.VisualBasic

Public Class Bodega
#Region "privadas"

    Dim _IdBodega As Integer
    Dim _NombreBodega As String

#End Region

#Region "Publicas"
    Public Property IdBodega() As Integer
        Get
            Return _IdBodega
        End Get
        Set(ByVal value As Integer)
            _IdBodega = value
        End Set
    End Property

    Public Property NombreBodega() As String
        Get
            Return _NombreBodega
        End Get
        Set(ByVal value As String)
            _NombreBodega = value
        End Set
    End Property
#End Region
End Class
