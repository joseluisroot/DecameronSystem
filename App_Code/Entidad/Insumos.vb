﻿Imports Microsoft.VisualBasic

Public Class Insumos
#Region "privadas"
    Dim _IdInsumo As Integer
    Dim _Nombre As String
#End Region
#Region "publicas"
    Public Property IdInsumo() As Integer
        Get
            Return _IdInsumo
        End Get
        Set(ByVal value As Integer)
            _IdInsumo = value
        End Set
    End Property
    Public Property Nombre() As String
        Get
            Return _Nombre
        End Get
        Set(ByVal value As String)
            _Nombre = value
        End Set
    End Property

#End Region

End Class
