﻿Imports Microsoft.VisualBasic


Public Class Estado

#Region "privadas"

    Dim _IdEstado As Integer
    Dim _NombreEstado As String

#End Region

#Region "Publicas"

    Public Property IdEstado() As Integer
        Get
            Return _IdEstado
        End Get
        Set(ByVal value As Integer)
            _IdEstado = value
        End Set
    End Property

    Public Property NombreEstado() As String
        Get
            Return _NombreEstado
        End Get
        Set(ByVal value As String)
            _NombreEstado = value
        End Set
    End Property

#End Region

End Class
