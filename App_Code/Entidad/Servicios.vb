﻿Imports Microsoft.VisualBasic

Public Class Servicios
#Region "privadas"

    Dim _IdServicio As Integer
    Dim _Nombre As String
    Dim _precio As Decimal

#End Region

#Region "Publicas"
    Public Property IdServicio() As Integer
        Get
            Return _IdServicio
        End Get
        Set(ByVal value As Integer)
            _IdServicio = value
        End Set
    End Property

    Public Property Nombre() As String
        Get
            Return _Nombre
        End Get
        Set(ByVal value As String)
            _Nombre = value
        End Set
    End Property

    Public Property precio() As Decimal
        Get
            Return _precio
        End Get
        Set(ByVal value As Decimal)
            _precio = value
        End Set
    End Property
#End Region
End Class
