﻿Imports Microsoft.VisualBasic

Public Class LoginSys

#Region "privadas"

    Dim _usuario As String
    Dim _clave As String

#End Region

#Region "Publicas"

    Public Property usuario() As String
        Get
            Return _usuario
        End Get
        Set(ByVal value As String)
            _usuario = value
        End Set
    End Property

    Public Property clave() As String
        Get
            Return _clave
        End Get
        Set(ByVal value As String)
            _clave = value
        End Set
    End Property

#End Region

End Class
