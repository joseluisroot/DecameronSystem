﻿Imports Microsoft.VisualBasic

Public Class TipoHabitacion
#Region "privadas"

    Dim _IdTipoHabitacion As Integer
    Dim _NombreHabitacion As String
    Dim _Porcentaje_descuento As Decimal
    Dim _IdEstado As Integer

#End Region

#Region "Publicas"
    Public Property IdTipoHabitacion() As Integer
        Get
            Return _IdTipoHabitacion
        End Get
        Set(ByVal value As Integer)
            _IdTipoHabitacion = value
        End Set
    End Property

    Public Property NombreHabitacion() As String
        Get
            Return _NombreHabitacion
        End Get
        Set(ByVal value As String)
            _NombreHabitacion = value
        End Set
    End Property

    Public Property Porcentaje_descuento() As Decimal
        Get
            Return _Porcentaje_descuento
        End Get
        Set(ByVal value As Decimal)
            _Porcentaje_descuento = value
        End Set
    End Property

    Public Property IdEstado() As Integer
        Get
            Return _IdEstado
        End Get
        Set(ByVal value As Integer)
            _IdEstado = value
        End Set
    End Property
#End Region

End Class
