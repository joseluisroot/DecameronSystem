﻿Imports Microsoft.VisualBasic

Public Class Reserva
#Region "privadas"
    Dim _IdReserva As Integer
    Dim _IdPlan As Integer
    Dim _ocupantes As Integer
    Dim _fecha_inicio As Date
    Dim _fecha_final As Date
#End Region

#Region "Publicas"
    Public Property IdReserva() As Integer
        Get
            Return _IdReserva
        End Get
        Set(ByVal value As Integer)
            _IdReserva = value
        End Set
    End Property

    Public Property IdPlan() As Integer
        Get
            Return _IdPlan
        End Get
        Set(ByVal value As Integer)
            _IdPlan = value
        End Set
    End Property

    Public Property ocupantes() As Integer
        Get
            Return _ocupantes
        End Get
        Set(ByVal value As Integer)
            _ocupantes = value
        End Set
    End Property

    Public Property fecha_inicio() As Date
        Get
            Return _fecha_inicio
        End Get
        Set(ByVal value As Date)
            _fecha_inicio = value
        End Set
    End Property

    Public Property fecha_final() As Date
        Get
            Return _fecha_final
        End Get
        Set(ByVal value As Date)
            _fecha_final = value
        End Set
    End Property
#End Region
End Class
