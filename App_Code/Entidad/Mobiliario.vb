﻿Imports Microsoft.VisualBasic

Public Class Mobiliario
#Region "privadas"
    Dim _IdMobiliaro As Integer
    Dim _Nombre As String
#End Region
#Region "publicas"
    Public Property IdMobiliario() As Integer
        Get
            Return _IdMobiliaro
        End Get
        Set(ByVal value As Integer)
            _IdMobiliaro = value
        End Set
    End Property
    Public Property Nombre() As String
        Get
            Return _Nombre
        End Get
        Set(ByVal value As String)
            _Nombre = value
        End Set
    End Property

#End Region

End Class
