﻿Imports Microsoft.VisualBasic
Public Class Cliente

#Region "privadas"
    Dim _IdCliente As Integer
    Dim _NombreCliente As String
    Dim _ApellidosCliente As String
    Dim _Sexo As String
    Dim _Edad As Integer
    Dim _FechaIngreso As Date
    Dim _Direccion As String
    Dim _Telefono As Integer
    Dim _Email As String
#End Region

#Region "Publicas"

    Public Property IdCliente() As Integer
        Get
            Return _IdCliente
        End Get
        Set(ByVal value As Integer)
            _IdCliente = value
        End Set
    End Property

    Public Property NombreCliente() As String
        Get
            Return _NombreCliente
        End Get
        Set(ByVal value As String)
            _NombreCliente = value
        End Set
    End Property

    Public Property ApellidosCliente() As String
        Get
            Return _ApellidosCliente
        End Get
        Set(ByVal value As String)
            _ApellidosCliente = value
        End Set
    End Property

    Public Property Sexo() As String
        Get
            Return _Sexo
        End Get
        Set(ByVal value As String)
            _Sexo = value
        End Set
    End Property

    Public Property Edad() As Integer
        Get
            Return _Edad
        End Get
        Set(ByVal value As Integer)
            _Edad = value
        End Set
    End Property

    Public Property FechaInfreso() As DateTime
        Get
            Return _FechaIngreso
        End Get
        Set(ByVal value As DateTime)
            _FechaIngreso = value
        End Set
    End Property

    Public Property Direccion() As String
        Get
            Return _Direccion
        End Get
        Set(ByVal value As String)
            _Direccion = value
        End Set
    End Property

    Public Property Telefono() As Integer
        Get
            Return _Telefono
        End Get
        Set(ByVal value As Integer)
            _Telefono = value
        End Set
    End Property

    Public Property Email() As String
        Get
            Return _Email
        End Get
        Set(ByVal value As String)
            _Email = value
        End Set
    End Property
#End Region
End Class