﻿Imports Microsoft.VisualBasic


Public Class TipoPago

#Region "privadas"

    Dim _IdTipoPago As Integer
    Dim _nombreTipoPago As String

#End Region

#Region "Publicas"

    Public Property IdTipoPago() As Integer
        Get
            Return _IdTipoPago
        End Get
        Set(ByVal value As Integer)
            _IdTipoPago = value
        End Set
    End Property

    Public Property nombreTipoPago() As String
        Get
            Return _nombreTipoPago
        End Get
        Set(ByVal value As String)
            _nombreTipoPago = value
        End Set
    End Property

#End Region

End Class
