﻿Imports System.Data
Partial Class _Default
    Inherits cBase

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Me.finSession()

        If Me.IsPostBack = False Then

            'CargarDatos

            Dim dsDatos As New DataSet
            Dim cControl As New dbBodega
            dsDatos = cControl.RecuperarBodega

            If dsDatos.Tables(0).Rows.Count > 0 Then
                LlenarGrid(dsDatos)
            Else
                Me.lblCant.Text = dsDatos.Tables(0).Rows.Count & " Bodegas"
            End If

            Iniciar()
            DeshabilitarForm()
        End If
    End Sub

    Sub reiniciar()
        Me.btnCancelar.Visible = False
        Me.btnModificar.Visible = False
        Me.btnGuardar.Visible = False
    End Sub

    Sub LlenarGrid(ByVal Datos As DataSet)

        If Datos.Tables(0).Rows.Count > 0 Then
            Me.dgDatos.Columns.Clear()
            Me.dgDatos.DataSource = Datos.Tables(0)

            Dim btnSel As New CommandField
            btnSel.ShowSelectButton = True
            btnSel.SelectText = "Seleccionar"
            Me.dgDatos.Columns.Add(btnSel)

            Dim IdBodega As New BoundField
            IdBodega.DataField = "IdBodega"
            IdBodega.HeaderText = "Codigo de bodega"
            Me.dgDatos.Columns.Add(IdBodega)

            Dim NombreBodega As New BoundField
            NombreBodega.DataField = "NombreBodega"
            NombreBodega.HeaderText = "Nombre de Bodega"
            Me.dgDatos.Columns.Add(NombreBodega)

            Me.dgDatos.DataBind()
            Me.lblCant.Text = Datos.Tables(0).Rows.Count & "Bodegas"
        End If

    End Sub

    Sub Iniciar()
        Me.btnGuardar.Visible = False
        Me.btnCancelar.Visible = False
        Me.btnModificar.Visible = False
        Me.btnBuscarNombre.Visible = False
    End Sub

    Sub HabilitaForm()
        Me.txtIdBodega.ReadOnly = True
        Me.txtNombre.Enabled = True
    End Sub

    Sub DeshabilitarForm()
        Me.txtIdBodega.ReadOnly = True
        Me.txtNombre.Enabled = False
    End Sub

    Sub LimpiarForm()
        Me.txtIdBodega.Text = ""
        Me.txtNombre.Text = ""
    End Sub

    Protected Sub btnAgregar_Click(sender As Object, e As System.EventArgs) Handles btnAgregar.Click
        Me.Session.Item("Accion") = "Agregar"
        Me.btnAgregar.Visible = False
        Me.btnGuardar.Visible = True
        Me.btnCancelar.Visible = True
        Me.btnBuscar.Visible = False
        HabilitaForm()
    End Sub

    Protected Sub btnGuardar_Click(sender As Object, e As System.EventArgs) Handles btnGuardar.Click
        Dim lEntidad As New Bodega
        Dim cControl As New dbBodega
        Dim dsDatos As New DataSet

        If Me.Session.Item("Accion") = "Agregar" Then
            If String.IsNullOrEmpty(Me.hfBodega.Value) = True Then
                lEntidad.NombreBodega = Me.txtNombre.Text
                If cControl.GuardarBodega(lEntidad) > 0 Then
                    DeshabilitarForm()
                    Iniciar()
                    LlenarGrid(cControl.RecuperarBodega)

                    Me.LimpiarForm()
                    Me.Session.Item("Accion") = ""

                    Me.btnAgregar.Visible = True
                    Me.btnBuscar.Visible = True
                Else

                End If

            End If

        ElseIf Me.Session.Item("Accion") = "Modificar" Then

            lEntidad.IdBodega = Me.txtIdBodega.Text
            lEntidad.NombreBodega = Me.txtNombre.Text

            If cControl.ModificarBodega(lEntidad) > 0 Then
                DeshabilitarForm()
                Iniciar()
                LlenarGrid(cControl.RecuperarBodega)

                Me.LimpiarForm()
                Me.Session.Item("Accion") = ""

                Me.btnAgregar.Visible = True
                Me.btnBuscar.Visible = True
            Else
            End If

        End If
    End Sub

    Protected Sub btnModificar_Click(sender As Object, e As System.EventArgs) Handles btnModificar.Click
        Me.Session.Item("Accion") = "Modificar"

        Me.HabilitaForm()
        Me.txtIdBodega.ReadOnly = True
        Me.btnGuardar.Visible = True
        Me.btnCancelar.Visible = True
        Me.btnModificar.Visible = False
        Me.btnBuscarNombre.Visible = False
    End Sub

    Protected Sub btnCancelar_Click(sender As Object, e As System.EventArgs) Handles btnCancelar.Click
        Select Case Me.Session.Item("Accion")
            Case "Buscar"

                Me.btnAgregar.Visible = True
                Me.btnBuscar.Visible = True

                Me.btnBuscarNombre.Visible = False
                Me.btnCancelar.Visible = False

                Me.txtNombre.Enabled = False

                Dim cControl As New dbBodega
                Me.LlenarGrid(cControl.RecuperarBodega)

            Case "Modificar"
                Me.btnModificar.Visible = False
                Me.btnCancelar.Visible = False

                Me.btnGuardar.Visible = False
                Me.btnBuscarNombre.Visible = False

                Me.btnAgregar.Visible = True
                Me.btnBuscar.Visible = True

                Me.LimpiarForm()
                Me.DeshabilitarForm()

                Dim cControl As New dbBodega
                Me.LlenarGrid(cControl.RecuperarBodega)

            Case "Agregar"

                Me.DeshabilitarForm()

                Me.btnAgregar.Visible = True
                Me.btnBuscar.Visible = True

                Me.btnGuardar.Visible = False
                Me.btnCancelar.Visible = False

            Case "Seleccion"

                Me.btnModificar.Visible = False
                Me.btnCancelar.Visible = False
                Me.btnBuscarNombre.Visible = False
                Me.btnAgregar.Visible = True
                Me.btnBuscar.Visible = True

                Dim cControl As New dbBodega
                Me.LlenarGrid(cControl.RecuperarBodega)
                Me.LimpiarForm()
                Me.DeshabilitarForm()

        End Select
    End Sub

    Protected Sub btnBuscarNombre_Click(sender As Object, e As System.EventArgs) Handles btnBuscarNombre.Click
        Dim dsDatos As New DataSet
        Dim cControl As New dbBodega

        dsDatos = cControl.BuscarBodegaPorNombre(Me.txtNombre.Text)
        If dsDatos.Tables(0).Rows.Count > 0 Then
            Me.LlenarGrid(dsDatos)
            Me.lblCant.Text = dsDatos.Tables(0).Rows.Count & " Bodegas"
        Else
            Me.dgDatos.EmptyDataText = "Datos no encontrado con esos parametros"
            Me.dgDatos.Columns.Clear()
            Me.dgDatos.DataSource = Nothing
            Me.dgDatos.DataBind()
        End If

        Me.LimpiarForm()
        Me.btnModificar.Visible = False
    End Sub

    Protected Sub btnBuscar_Click(sender As Object, e As System.EventArgs) Handles btnBuscar.Click
        Me.Session.Item("Accion") = "Buscar"

        Me.btnBuscarNombre.Visible = True
        Me.txtNombre.Enabled = True
        Me.btnCancelar.Visible = True
        Me.btnBuscar.Visible = False
        Me.btnAgregar.Visible = False
    End Sub
End Class
