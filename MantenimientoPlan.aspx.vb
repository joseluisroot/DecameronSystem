﻿Imports System.Data
Partial Class _Default
    Inherits cBase
    Dim lnIdPlan As Integer = 0
    Dim dsPlanes As New DataSet With {.DataSetName = "dsPlanes"}
    Dim dtPlanes As New DataTable With {.TableName = "dtPlanes"}
    Dim dtTipoHabitacion As New DataTable With {.TableName = "dtTipoHabitacion"}

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Try
            Me.finSession()
            dtPlanes.Rows.Clear()
            dtTipoHabitacion.Rows.Clear()

            Dim cCtrlPlan As New dbPlan
            Dim cCtrlTipoHabitacion As New dbTipoHabitacion
            dtPlanes = DirectCast(cCtrlPlan.RecuperarPlan.Tables(0), DataTable)
            dtTipoHabitacion = DirectCast(cCtrlTipoHabitacion.RecuperarTipoHabitacion.Tables(0), DataTable)

            If Me.IsPostBack = False Then
                'CargarDatos

                dsPlanes = cCtrlPlan.RecuperarPlan

                If dsPlanes.Tables(0).Rows.Count > 0 Then
                    LlenarGrid(dsPlanes)
                Else
                    MsgBox("No hay planes ingresados", MsgBoxStyle.Exclamation, "Sistema de Reserva Decameron")
                End If

                If dtTipoHabitacion.Rows.Count > 0 Then
                    With dtlTipoHabitacion
                        .DataSource = dtTipoHabitacion
                        .DataValueField = "IdTipoHabitacion"
                        .DataTextField = "NombreHabitacion"
                        .DataBind()
                    End With
                Else
                    MsgBox("No hay Tipos de habitación disponible. Consulte al administrador", MsgBoxStyle.Exclamation, "Sistema de Reserva Decameron")
                End If

                Iniciar()
                DeshabilitarForm()
            End If
        Catch ex As Exception
            MsgBox(ex.ToString, MsgBoxStyle.Exclamation, "Sistema de Reserva Decameron")
            MsgBox("Error al cargar el formulario", MsgBoxStyle.Exclamation, "Sistema de Reserva Decameron")
        End Try
    End Sub

    Sub reiniciar()
        Me.btnCancelar.Visible = False
        Me.btnDeschabilitar.Visible = False
        Me.btnModificar.Visible = False
        Me.btnGuardar.Visible = False
    End Sub

    Sub LlenarGrid(ByVal Datos As DataSet)
        If Datos.Tables(0).Rows.Count > 0 Then
            Me.dgvPlanes.Columns.Clear()
            Me.dgvPlanes.DataSource = Datos.Tables(0)

            Dim btnSel As New CommandField
            btnSel.ShowSelectButton = True
            btnSel.SelectText = "Seleccionar"
            Me.dgvPlanes.Columns.Add(btnSel)

            Dim idPlan As New BoundField
            idPlan.DataField = "idPlan"
            idPlan.HeaderText = "Codigo del Plan"
            Me.dgvPlanes.Columns.Add(idPlan)

            Dim idTipoHabitacion As New BoundField
            idTipoHabitacion.DataField = "idTipoHabitacion"
            idTipoHabitacion.HeaderText = "idTipoHabitacion"
            idTipoHabitacion.Visible = False
            Me.dgvPlanes.Columns.Add(idTipoHabitacion)

            Dim NombreHabitacion As New BoundField
            NombreHabitacion.DataField = "NombreHabitacion"
            NombreHabitacion.HeaderText = "Tipo habitación"
            Me.dgvPlanes.Columns.Add(NombreHabitacion)

            Dim Nombre_plan As New BoundField
            Nombre_plan.DataField = "Nombre_plan"
            Nombre_plan.HeaderText = "Plan"
            Me.dgvPlanes.Columns.Add(Nombre_plan)

            Dim Valor_plan As New BoundField
            Valor_plan.DataField = "Valor_plan"
            Valor_plan.HeaderText = "Valor plan"
            Me.dgvPlanes.Columns.Add(Valor_plan)

            Dim Valor_descuento As New BoundField
            Valor_descuento.DataField = "Valor_descuento"
            Valor_descuento.HeaderText = "Descuento autorizado"
            Me.dgvPlanes.Columns.Add(Valor_descuento)

            Dim Valor_real As New BoundField
            Valor_real.DataField = "Valor_real"
            Valor_real.HeaderText = "Valor neto"
            Me.dgvPlanes.Columns.Add(Valor_real)

            Dim monto_iva As New BoundField
            monto_iva.DataField = "monto_iva"
            monto_iva.HeaderText = "I.V.A"
            Me.dgvPlanes.Columns.Add(monto_iva)

            Dim Valor_total As New BoundField
            Valor_total.DataField = "Valor_total"
            Valor_total.HeaderText = "Total plan"
            Me.dgvPlanes.Columns.Add(Valor_total)

            Me.dgvPlanes.DataBind()
        End If
    End Sub

    Sub Iniciar()
        Me.btnGuardar.Visible = False
        Me.btnCancelar.Visible = False
        Me.btnModificar.Visible = False
        Me.btnDeschabilitar.Visible = False
        Me.btnBuscarNombre.Visible = False
    End Sub

    Sub HabilitaForm()
        Me.txtNombre_plan.ReadOnly = False
        Me.txtValor_plan.ReadOnly = False
        Me.dtlTipoHabitacion.Enabled = True
        Me.dgvPlanes.Enabled = True
    End Sub

    Sub DeshabilitarForm()
        'Me.txtIdPlan.ReadOnly = False
        Me.txtNombre_plan.ReadOnly = True
        Me.txtValor_plan.ReadOnly = True
        Me.txtValor_descuento.ReadOnly = True
        Me.txtValor_real.ReadOnly = True
        Me.txtValor_total.ReadOnly = True
        Me.dtlTipoHabitacion.Enabled = False
        Me.dgvPlanes.Enabled = True
    End Sub

    Sub LimpiarForm()
        Me.txtNombre_plan.Text = ""
        Me.txtValor_plan.Text = ""
        Me.txtValor_descuento.Text = ""
        Me.txtValor_real.Text = ""
        Me.txtmonto_iva.Text = ""
        Me.txtValor_total.Text = ""
        Me.dtlTipoHabitacion.SelectedIndex = 0
    End Sub

    Sub Calcular()
        Try
            Dim lnXcDescuento As Decimal = 0
            Dim lnXcIVA As Decimal = 13
            Dim lnValorPlan As Decimal = 0
            Dim lnValorDescuento As Decimal = 0
            Dim lnValorReal As Decimal = 0
            Dim lnMontoIva As Decimal = 0
            Dim lnValorTotal As Decimal = 0
            Dim lnIndex As Integer = 0

            lnIndex = dtlTipoHabitacion.SelectedIndex
            lnValorPlan = Val(Me.txtValor_plan.Text)
            lnXcDescuento = If(lnIndex > -1, _
                            dtTipoHabitacion.Rows(lnIndex).Item("porcentaje_descuento"), 0)
            lnValorDescuento = (lnValorPlan * lnXcDescuento) / 100
            lnValorReal = lnValorPlan - lnValorDescuento
            lnMontoIva = (lnValorReal * lnXcIVA) / 100
            lnValorTotal = lnValorReal + lnMontoIva

            Me.txtValor_descuento.Text = lnValorDescuento
            Me.txtValor_real.Text = lnValorReal
            Me.txtmonto_iva.Text = lnMontoIva
            Me.txtValor_total.Text = lnValorTotal
        Catch ex As Exception
            MsgBox(ex.ToString, MsgBoxStyle.Exclamation, "Error de cálculo")
        End Try
    End Sub

    Protected Sub btnAgregar_Click(sender As Object, e As System.EventArgs) Handles btnAgregar.Click
        Me.Session.Item("Accion") = "Agregar"

        Me.btnAgregar.Visible = False
        Me.btnGuardar.Visible = True
        Me.btnCancelar.Visible = True
        Me.btnBuscar.Visible = False

        HabilitaForm()
    End Sub

    Protected Sub btnModificar_Click(sender As Object, e As System.EventArgs) Handles btnModificar.Click
        Me.Session.Item("Accion") = "Modificar"

        Me.HabilitaForm()
        Me.txtIdPlan.ReadOnly = True
        Me.btnGuardar.Visible = True
        Me.btnCancelar.Visible = True
        Me.btnModificar.Visible = False
        Me.btnBuscarNombre.Visible = False
    End Sub

    Protected Sub btnGuardar_Click(sender As Object, e As System.EventArgs) Handles btnGuardar.Click
        Dim lEntidad As New Plan
        Dim cCtrlPlan As New dbPlan
        Dim dsDatos As New DataSet

        If Me.Session.Item("Accion") = "Agregar" Then
            lEntidad.IdTipoHabitacion = Me.dtlTipoHabitacion.SelectedValue
            lEntidad.Nombre_plan = Me.txtNombre_plan.Text
            lEntidad.Valor_plan = Val(Me.txtValor_plan.Text)
            lEntidad.Valor_descuento = Val(Me.txtValor_descuento.Text)
            lEntidad.Valor_real = Val(Me.txtValor_real.Text)
            lEntidad.monto_iva = Val(Me.txtmonto_iva.Text)
            lEntidad.Valor_total = Val(Me.txtValor_total.Text)

            If cCtrlPlan.GuardarPlan(lEntidad) > 0 Then
                MsgBox("Registro guardado exitosamente", MsgBoxStyle.Information, "Sistema Decameron")

                DeshabilitarForm()
                Iniciar()

                LlenarGrid(cCtrlPlan.RecuperarPlan)

                Me.LimpiarForm()
                Me.Session.Item("Accion") = ""

                Me.btnAgregar.Visible = True
                Me.btnBuscar.Visible = True
            End If

        ElseIf Me.Session.Item("Accion") = "Modificar" Then


            lEntidad.IdPlan = Me.txtIdPlan.Text

            If cCtrlPlan.ModificarPlan(lEntidad) > 0 Then

                DeshabilitarForm()
                Iniciar()

                LlenarGrid(cCtrlPlan.RecuperarPlan)

                Me.LimpiarForm()
                Me.Session.Item("Accion") = ""

                Me.btnAgregar.Visible = True
                Me.btnBuscar.Visible = True



            Else



            End If

        End If
    End Sub

    Protected Sub btnCancelar_Click(sender As Object, e As System.EventArgs) Handles btnCancelar.Click
        Select Case Me.Session.Item("Accion")
            Case "Buscar"

                Me.btnAgregar.Visible = True
                Me.btnBuscar.Visible = True

                Me.btnBuscarNombre.Visible = False
                Me.btnCancelar.Visible = False

                Me.txtNombre_plan.Enabled = False
                Me.txtValor_plan.Enabled = False

                Dim cControl As New dbPlan
                Me.LlenarGrid(cControl.RecuperarPlan)

            Case "Modificar"

                Me.btnModificar.Visible = False
                Me.btnCancelar.Visible = False

                Me.btnGuardar.Visible = False
                Me.btnBuscarNombre.Visible = False


                Me.btnAgregar.Visible = True
                Me.btnBuscar.Visible = True

                Me.LimpiarForm()
                Me.DeshabilitarForm()

                Dim cControl As New dbPlan
                Me.LlenarGrid(cControl.RecuperarPlan)

            Case "Agregar"

                Me.DeshabilitarForm()

                Me.btnAgregar.Visible = True
                Me.btnBuscar.Visible = True

                Me.btnGuardar.Visible = False
                Me.btnCancelar.Visible = False

            Case "Seleccion"

                Me.btnModificar.Visible = False
                Me.btnCancelar.Visible = False
                Me.btnBuscarNombre.Visible = False

                Me.btnAgregar.Visible = True
                Me.btnBuscar.Visible = True

                Dim cControl As New dbPlan
                Me.LlenarGrid(cControl.RecuperarPlan)

                Me.LimpiarForm()
                Me.DeshabilitarForm()
        End Select
    End Sub

    Protected Sub btnBuscar_Click(sender As Object, e As System.EventArgs) Handles btnBuscar.Click

    End Sub

    Protected Sub dtlTipoHabitacion_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles dtlTipoHabitacion.SelectedIndexChanged
        Dim lnIndex As Integer = 0
        lnIndex = dtlTipoHabitacion.SelectedIndex
        If lnIndex > -1 Then
            Calcular()
        End If
    End Sub

    Protected Sub dgvPlanes_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles dgvPlanes.SelectedIndexChanged
        Dim dsDatos As New DataSet
        Dim cControl As New dbPlan

        Me.Session.Item("Accion") = "Seleccion"

        dsDatos = cControl.RecuperarPlanPorid(Me.dgvPlanes.SelectedRow.Cells(1).Text)

        If dsDatos.Tables(0).Rows.Count > 0 Then

            Me.LlenarGrid(dsDatos)

            'llenar Datos para Modificar
            Me.txtIdPlan.Text = dsDatos.Tables(0).Rows(0).Item("idPlan").ToString
            Me.txtNombre_plan.Text = dsDatos.Tables(0).Rows(0).Item("Nombre_plan").ToString
            Me.txtValor_plan.Text = dsDatos.Tables(0).Rows(0).Item("Valor_plan").ToString
            Me.txtValor_descuento.Text = dsDatos.Tables(0).Rows(0).Item("Valor_descuento").ToString
            Me.txtValor_real.Text = dsDatos.Tables(0).Rows(0).Item("Valor_real").ToString
            Me.txtmonto_iva.Text = dsDatos.Tables(0).Rows(0).Item("monto_iva").ToString
            Me.txtValor_total.Text = dsDatos.Tables(0).Rows(0).Item("Valor_total").ToString
            Me.dtlTipoHabitacion.SelectedValue = dsDatos.Tables(0).Rows(0).Item("idTipoHabitacion").ToString

            'Me.lblCant.Text = dsDatos.Tables(0).Rows.Count & " Servicios"

            Me.btnAgregar.Visible = False
            Me.btnBuscar.Visible = False
            Me.btnGuardar.Visible = False

            Me.btnModificar.Visible = True
            Me.btnCancelar.Visible = True

            Me.btnBuscarNombre.Visible = False

        End If

        Me.DeshabilitarForm()
    End Sub

    Protected Sub txtValor_plan_TextChanged(sender As Object, e As System.EventArgs) Handles txtValor_plan.TextChanged
        Me.Calcular()
    End Sub
End Class
