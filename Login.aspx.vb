﻿
Imports System.Data

Partial Class Login
    Inherits FunctionKit

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Me.txtUsuario.Focus()
        End If
    End Sub

    Protected Sub btnIngresar_Click(sender As Object, e As System.EventArgs) Handles btnIngresar.Click

        Dim dsDatos As New DataSet
        Dim eEntidad As New LoginSys
        Dim cControl As New dbLogin

        eEntidad.usuario = Me.txtUsuario.Text
        eEntidad.clave = GetSHA1(Me.txtClave.Text)
        'eEntidad.clave = (Me.txtClave.Text)
        dsDatos = cControl.IngresarAlSistema(eEntidad)

        If dsDatos.Tables(0).Rows.Count > 0 Then


            Me.Session("NombreUsuario") = dsDatos.Tables(0).Rows(0).Item("Nombre")

            Me.Session("activa") = "si"

            Response.Redirect("Principal.aspx")

        Else

            MsgBox("Error al realizar el login", MsgBoxStyle.Information, "Mensaje")

        End If



    End Sub
End Class
