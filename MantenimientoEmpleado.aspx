﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="MantenimientoEmpleado.aspx.vb" Inherits="MantenimientoEmpleado" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <table id="TableForm" align="center" cellpadding="2" cellspacing="0">
        <tr>
            <td class="Tema">
                Mantenimiento Habitacion</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                <asp:HiddenField ID="hfEmpleado" runat="server" />
            </td>
            <td colspan="2">
                <asp:Button ID="btnAgregar" runat="server" CssClass="css3button" 
                    Text="Agregar" />
                <asp:Button ID="btnGuardar" runat="server" CssClass="css3button" Text="Guardar" 
                    ValidationGroup="1" />
                <asp:Button ID="btnModificar" runat="server" CssClass="css3button" 
                    Text="Modificar" />
                <asp:Button ID="btnCancelar" runat="server" CssClass="css3button" 
                    Text="Cancelar" />
                <asp:Button ID="btnDeschabilitar" runat="server" CssClass="css3button" 
                    Text="Deshabilitar" />
                <asp:Button ID="btnBuscar" runat="server" CssClass="css3button" Text="Buscar" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="celdaForm" colspan="2" rowspan="3">
                <div class="redondesdas">
                    <table id="TableForm0" align="center" cellpadding="2" cellspacing="0">
                        <tr>
                            <td>
                                Codigo: 
                            </td>
                            <td>
                                <asp:TextBox ID="txtIdEmpleado" runat="server" ReadOnly="True"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Nombre :<asp:RequiredFieldValidator ID="RequiredFieldValidator1" 
                                    runat="server" ControlToValidate="txtNombre" 
                                    ErrorMessage="Favor Ingresar Nombre " ValidationGroup="1" 
                                    ForeColor="Red">*</asp:RequiredFieldValidator>
                            </td>
                            <td>
                                <asp:TextBox ID="txtNombre" runat="server"></asp:TextBox>
                                &nbsp;<asp:Button ID="btnBuscarNombre" runat="server" CssClass="css3button" 
                                    Text="Buscar" />
                            </td>
                        </tr>

                        <tr>
                            <td>
                                Apellido:<asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                                    ControlToValidate="txtApellido" ErrorMessage="Favor Ingrese El Apellido" 
                                    ForeColor="Red" ValidationGroup="1">*</asp:RequiredFieldValidator>
                            </td>
                            <td>
                                <asp:TextBox ID="txtApellido" runat="server" ValidationGroup="1"></asp:TextBox>
                            </td>
                        </tr>
                       
                        <tr>
                            <td>
                                Usuario:<asp:RequiredFieldValidator 
                                    ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtUsuario" 
                                    ErrorMessage="Favor Ingrese Usuario" ForeColor="Red" ValidationGroup="1">*</asp:RequiredFieldValidator>
                            </td>
                            <td>
                                <asp:TextBox ID="txtUsuario" runat="server" ValidationGroup="1"></asp:TextBox>
                            </td>
                        </tr>
                       
                        <tr>
                            <td>
                                Clave:
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" 
                                    ControlToValidate="txtClave" ErrorMessage="Favor Ingrese Clave" ForeColor="Red" 
                                    ValidationGroup="1">*</asp:RequiredFieldValidator>
                            </td>
                            <td>
                                <asp:TextBox ID="txtClave" runat="server" ValidationGroup="1"></asp:TextBox>
                            </td>
                        </tr>
                       
                        <tr>
                            <td>
                                Estado:<asp:RequiredFieldValidator ID="RequiredFieldValidator6" 
                                    runat="server" ControlToValidate="ddlbEstado" 
                                    ErrorMessage="Por Favor Seleccione  Estado" ForeColor="Red" 
                                    ValidationGroup="1" InitialValue="0">*</asp:RequiredFieldValidator>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlbEstado" runat="server" ValidationGroup="1">
                                </asp:DropDownList>
                            </td>
                        </tr>
                       
                        <tr>
                            <td>
                                Perfil:
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" 
                                    ControlToValidate="ddlbPerfil" ErrorMessage="Favor Seleccione Perfil" 
                                    ForeColor="Red" ValidationGroup="1" InitialValue="0">*</asp:RequiredFieldValidator>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlbPerfil" runat="server" ValidationGroup="1">
                                </asp:DropDownList>
                            </td>
                        </tr>
                       
                        <tr>
                            <td>
                                Cantidadd</td>
                            <td>
                                <asp:Label ID="lblCant" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td colspan="4" style="text-align: center; margin-left: 80px">
                * Campos requeridos.              * Campos requeridos.<br />
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" 
                    ShowMessageBox="True" ShowSummary="False" ValidationGroup="1" />
            </td>
        </tr>
        <tr>
            <td colspan="4" style="text-align: center; margin-left: 80px">
                <asp:GridView ID="dgDatos" runat="server" AllowPaging="True" 
                    AutoGenerateColumns="False" CssClass="mGrid">
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
    </table>
    <br />
</asp:Content>

