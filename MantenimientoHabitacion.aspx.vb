﻿Imports System.Data

Partial Class MantenimientoHabitacion
    Inherits cBase
    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        Me.finSession()

        If Me.IsPostBack = False Then

            'CargarDatos

            Dim dsDatos As New DataSet
            Dim cControl As New dbHabitacion


            dsDatos = cControl.RecuperarHabitacion


            If dsDatos.Tables(0).Rows.Count > 0 Then
                LlenarGrid(dsDatos)
            Else
                Me.lblCant.Text = dsDatos.Tables(0).Rows.Count & "Habitacion"
            End If

            Iniciar()
            DeshabilitarForm()
            llenarTipoHabitacon()

        End If


    End Sub

    Sub llenarTipoHabitacon()


        Dim cControl As New dbTipoHabitacion
        Dim dsDatos As New DataSet

        dsDatos = cControl.RecuperarTipoHabitacion()

        If dsDatos.Tables(0).Rows.Count > 0 Then

            Me.ddlbTipoHabitacion.DataSource = dsDatos.Tables(0)
            Me.ddlbTipoHabitacion.DataTextField = "NombreHabitacion"
            Me.ddlbTipoHabitacion.DataValueField = "IdTipoHabitacion"
            Me.ddlbTipoHabitacion.DataBind()
        End If
        Me.ddlbTipoHabitacion.Items.Insert("0", New ListItem("Seleccionar...", "0"))



    End Sub


    Sub reiniciar()

        Me.btnCancelar.Visible = False
        Me.btnDeschabilitar.Visible = False
        Me.btnModificar.Visible = False
        Me.btnGuardar.Visible = False


    End Sub


    Sub LlenarGrid(ByVal Datos As DataSet)


        If Datos.Tables(0).Rows.Count > 0 Then

            Me.dgDatos.Columns.Clear()
            Me.dgDatos.DataSource = Datos.Tables(0)

            Dim btnSel As New CommandField
            btnSel.ShowSelectButton = True
            btnSel.SelectText = "Seleccionar"
            Me.dgDatos.Columns.Add(btnSel)

            Dim IdHabitacion As New BoundField
            IdHabitacion.DataField = "IdHabitacion"
            IdHabitacion.HeaderText = "Codigo de Habitacion"
            Me.dgDatos.Columns.Add(IdHabitacion)

            Dim NombreHabitacion As New BoundField
            NombreHabitacion.DataField = "NombreHabitacion"
            NombreHabitacion.HeaderText = "Nombre de Tipo Habitacion"
            Me.dgDatos.Columns.Add(NombreHabitacion)

            Dim Precio As New BoundField
            Precio.DataField = "Precio"
            Precio.HeaderText = "Precio $"
            Me.dgDatos.Columns.Add(Precio)

            Dim TipoHabitacion As New BoundField
            TipoHabitacion.DataField = "TipoHabitacion"
            TipoHabitacion.HeaderText = "Tipo Habitacion"
            Me.dgDatos.Columns.Add(TipoHabitacion)

            Dim Descuento As New BoundField
            Descuento.DataField = "Descuento"
            Descuento.HeaderText = "Descuento"
            Me.dgDatos.Columns.Add(Descuento)

            Me.dgDatos.DataBind()
            Me.lblCant.Text = Datos.Tables(0).Rows.Count & "  Habitaciones"
        End If

    End Sub

    Sub Iniciar()

        Me.btnGuardar.Visible = False
        Me.btnCancelar.Visible = False
        Me.btnModificar.Visible = False
        Me.btnDeschabilitar.Visible = False
        Me.btnBuscarNombre.Visible = False

    End Sub


    Sub HabilitaForm()

        Me.ddlbTipoHabitacion.Enabled = True
        Me.txtIdHabitacion.ReadOnly = True
        Me.txtNombreHabitacion.Enabled = True
        Me.txtPrecio.Enabled = True
        Me.txtDescuento.Enabled = True

    End Sub

    Sub DeshabilitarForm()

        Me.ddlbTipoHabitacion.Enabled = False
        Me.txtIdHabitacion.ReadOnly = True
        Me.txtNombreHabitacion.Enabled = False
        Me.txtPrecio.Enabled = False
        Me.txtDescuento.Enabled = False
    End Sub

    Sub LimpiarForm()

        Me.txtIdHabitacion.Text = ""
        Me.txtNombreHabitacion.Text = ""
        Me.txtPrecio.Text = ""
        Me.txtDescuento.Text = ""
        Me.ddlbTipoHabitacion.ClearSelection()



    End Sub

    Protected Sub btnAgregar_Click(sender As Object, e As System.EventArgs) Handles btnAgregar.Click
        Me.Session.Item("Accion") = "Agregar"

        Me.btnAgregar.Visible = False
        Me.btnGuardar.Visible = True
        Me.btnCancelar.Visible = True

        Me.btnBuscar.Visible = False

        HabilitaForm()
    End Sub

    Protected Sub btnCancelar_Click(sender As Object, e As System.EventArgs) Handles btnCancelar.Click

        Select Case Me.Session.Item("Accion")
            Case "Buscar"

                Me.btnAgregar.Visible = True
                Me.btnBuscar.Visible = True

                Me.btnBuscarNombre.Visible = False
                Me.btnCancelar.Visible = False

                Me.txtNombreHabitacion.Enabled = False

                Dim cControl As New dbHabitacion
                Me.LlenarGrid(cControl.RecuperarHabitacion)

            Case "Modificar"


                Me.btnModificar.Visible = False
                Me.btnCancelar.Visible = False

                Me.btnGuardar.Visible = False
                Me.btnBuscarNombre.Visible = False


                Me.btnAgregar.Visible = True
                Me.btnBuscar.Visible = True

                Me.LimpiarForm()
                Me.DeshabilitarForm()

                Dim cControl As New dbHabitacion
                Me.LlenarGrid(cControl.RecuperarHabitacion)

            Case "Agregar"

                Me.DeshabilitarForm()

                Me.btnAgregar.Visible = True
                Me.btnBuscar.Visible = True

                Me.btnGuardar.Visible = False
                Me.btnCancelar.Visible = False

            Case "Seleccion"

                Me.btnModificar.Visible = False
                Me.btnCancelar.Visible = False
                Me.btnBuscarNombre.Visible = False

                Me.btnAgregar.Visible = True
                Me.btnBuscar.Visible = True

                Dim cControl As New dbHabitacion
                Me.LlenarGrid(cControl.RecuperarHabitacion)



                Me.LimpiarForm()
                Me.DeshabilitarForm()

        End Select

    End Sub

    Protected Sub btnModificar_Click(sender As Object, e As System.EventArgs) Handles btnModificar.Click
        Me.Session.Item("Accion") = "Modificar"

        Me.HabilitaForm()

        Me.txtIdHabitacion.ReadOnly = True

        Me.btnGuardar.Visible = True
        Me.btnCancelar.Visible = True

        Me.btnModificar.Visible = False
        Me.btnBuscarNombre.Visible = False
    End Sub

    Protected Sub btnBuscar_Click(sender As Object, e As System.EventArgs) Handles btnBuscar.Click

        Me.Session.Item("Accion") = "Buscar"

        Me.btnBuscarNombre.Visible = True
        Me.txtNombreHabitacion.Enabled = True
        Me.btnCancelar.Visible = True

        Me.btnBuscar.Visible = False
        Me.btnAgregar.Visible = False

    End Sub

    Protected Sub btnBuscarNombre_Click(sender As Object, e As System.EventArgs) Handles btnBuscarNombre.Click

        Dim dsDatos As New DataSet
        Dim cControl As New dbHabitacion

        dsDatos = cControl.BuscarHabitacionPorNombre(Me.txtNombreHabitacion.Text)

        If dsDatos.Tables(0).Rows.Count > 0 Then

            Me.LlenarGrid(dsDatos)
            Me.lblCant.Text = dsDatos.Tables(0).Rows.Count & " Habitacion"
        Else

            Me.dgDatos.EmptyDataText = "Datos no encontrado con esos parametros"
            Me.dgDatos.Columns.Clear()
            Me.dgDatos.DataSource = Nothing
            Me.dgDatos.DataBind()

            'Mensaje

        End If

        Me.LimpiarForm()
        Me.btnModificar.Visible = False

    End Sub

    Protected Sub btnGuardar_Click(sender As Object, e As System.EventArgs) Handles btnGuardar.Click

        Dim lEntidad As New Habitacion
        Dim cControl As New dbHabitacion
        Dim dsDatos As New DataSet

        If Me.Session.Item("Accion") = "Agregar" Then
            If String.IsNullOrEmpty(Me.hfHabitacion.Value) = True Then

                lEntidad.IdTipoHabitacion = Me.ddlbTipoHabitacion.SelectedValue
                lEntidad.NombreHabitacion = Me.txtNombreHabitacion.Text
                lEntidad.Precio = Me.txtPrecio.Text
                lEntidad.Descuento = 0

                If cControl.GuardarHabitacion(lEntidad) > 0 Then

                    DeshabilitarForm()
                    Iniciar()

                    LlenarGrid(cControl.RecuperarHabitacion)

                    Me.LimpiarForm()
                    Me.Session.Item("Accion") = ""

                    Me.btnAgregar.Visible = True
                    Me.btnBuscar.Visible = True


                Else

                End If

            End If


        ElseIf Me.Session.Item("Accion") = "Modificar" Then

            lEntidad.IdHabitacion = Me.txtIdHabitacion.Text
            lEntidad.NombreHabitacion = Me.txtNombreHabitacion.Text
            lEntidad.Precio = Me.txtPrecio.Text
            lEntidad.IdTipoHabitacion = Me.ddlbTipoHabitacion.Text
            lEntidad.Descuento = Me.txtDescuento.Text

            If cControl.ModificarHabitacion(lEntidad) > 0 Then


                DeshabilitarForm()
                Iniciar()

                LlenarGrid(cControl.RecuperarHabitacion)

                Me.LimpiarForm()
                Me.Session.Item("Accion") = ""

                Me.btnAgregar.Visible = True
                Me.btnBuscar.Visible = True



            Else



            End If

        End If

    End Sub


    Protected Sub dgDatos_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles dgDatos.SelectedIndexChanged

        Dim dsDatos As New DataSet
        Dim cControl As New dbHabitacion

        Me.Session.Item("Accion") = "Seleccion"

        dsDatos = cControl.RecuperarHabitacionPorId(Me.dgDatos.SelectedRow.Cells(1).Text)

        If dsDatos.Tables(0).Rows.Count > 0 Then

            Me.LlenarGrid(dsDatos)

            'llenar Datos para Modificar
            Me.txtIdHabitacion.Text = dsDatos.Tables(0).Rows(0).Item("IdHabitacion").ToString
            Me.txtNombreHabitacion.Text = dsDatos.Tables(0).Rows(0).Item("NombreHabitacion").ToString
            Me.txtPrecio.Text = dsDatos.Tables(0).Rows(0).Item("Precio").ToString
            Me.ddlbTipoHabitacion.Text = dsDatos.Tables(0).Rows(0).Item("IdTipoHabitacion").ToString
            Me.txtDescuento.Text = dsDatos.Tables(0).Rows(0).Item("Descuento").ToString

            Me.lblCant.Text = dsDatos.Tables(0).Rows.Count & " Habitacion"

            Me.btnAgregar.Visible = False
            Me.btnBuscar.Visible = False
            Me.btnGuardar.Visible = False

            Me.btnModificar.Visible = True
            Me.btnCancelar.Visible = True

            Me.btnBuscarNombre.Visible = False

        End If

        Me.DeshabilitarForm()

    End Sub
End Class
