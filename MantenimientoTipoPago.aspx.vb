﻿Imports System.Data

Partial Class MantenimientoTipoPago
    Inherits cBase

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        Me.finSession()

        If Me.IsPostBack = False Then

            'CargarDatos

            Dim dsDatos As New DataSet
            Dim cControl As New dbTipoPago

            dsDatos = cControl.RecuperarTipoPago

            If dsDatos.Tables(0).Rows.Count > 0 Then
                LlenarGrid(dsDatos)
            Else
                Me.lblCant.Text = dsDatos.Tables(0).Rows.Count & " Tipo Pago"
            End If

            Iniciar()
            DeshabilitarForm()

        End If


    End Sub

    Sub reiniciar()

        Me.btnCancelar.Visible = False
        Me.btnDeschabilitar.Visible = False
        Me.btnModificar.Visible = False
        Me.btnGuardar.Visible = False


    End Sub


    Sub LlenarGrid(ByVal Datos As DataSet)

        If Datos.Tables(0).Rows.Count > 0 Then

            Me.dgDatos.Columns.Clear()
            Me.dgDatos.DataSource = Datos.Tables(0)

            Dim btnSel As New CommandField
            btnSel.ShowSelectButton = True
            btnSel.SelectText = "Seleccionar"
            Me.dgDatos.Columns.Add(btnSel)

            Dim IdTipoPago As New BoundField
            IdTipoPago.DataField = "IdTipoPago"
            IdTipoPago.HeaderText = "Codigo Tipo Pago"
            Me.dgDatos.Columns.Add(IdTipoPago)

            Dim nombreTipoPago As New BoundField
            nombreTipoPago.DataField = "nombreTipoPago"
            nombreTipoPago.HeaderText = "Nombre Tipo Pago"
            Me.dgDatos.Columns.Add(nombreTipoPago)

            Me.dgDatos.DataBind()
            Me.lblCant.Text = Datos.Tables(0).Rows.Count & " Tipo Pago"
        End If

    End Sub

    Sub Iniciar()

        Me.btnGuardar.Visible = False
        Me.btnCancelar.Visible = False
        Me.btnModificar.Visible = False
        Me.btnDeschabilitar.Visible = False
        Me.btnBuscarNombre.Visible = False

    End Sub


    Sub HabilitaForm()

        Me.txtIdTipoPago.ReadOnly = True
        Me.txtNombreTipoPago.Enabled = True


    End Sub

    Sub DeshabilitarForm()

        Me.txtIdTipoPago.ReadOnly = True
        Me.txtNombreTipoPago.Enabled = False

    End Sub

    Sub LimpiarForm()

        Me.txtIdTipoPago.Text = ""
        Me.txtNombreTipoPago.Text = ""

    End Sub

    Protected Sub btnAgregar_Click(sender As Object, e As System.EventArgs) Handles btnAgregar.Click
        Me.Session.Item("Accion") = "Agregar"

        Me.btnAgregar.Visible = False
        Me.btnGuardar.Visible = True
        Me.btnCancelar.Visible = True

        Me.btnBuscar.Visible = False

        HabilitaForm()
    End Sub

    Protected Sub btnCancelar_Click(sender As Object, e As System.EventArgs) Handles btnCancelar.Click

        Select Case Me.Session.Item("Accion")
            Case "Buscar"

                Me.btnAgregar.Visible = True
                Me.btnBuscar.Visible = True

                Me.btnBuscarNombre.Visible = False
                Me.btnCancelar.Visible = False

                Me.txtNombreTipoPago.Enabled = False

                Dim cControl As New dbTipoPago
                Me.LlenarGrid(cControl.RecuperarTipoPago)

            Case "Modificar"


                Me.btnModificar.Visible = False
                Me.btnCancelar.Visible = False

                Me.btnGuardar.Visible = False
                Me.btnBuscarNombre.Visible = False


                Me.btnAgregar.Visible = True
                Me.btnBuscar.Visible = True

                Me.LimpiarForm()
                Me.DeshabilitarForm()

                Dim cControl As New dbTipoPago
                Me.LlenarGrid(cControl.RecuperarTipoPago)

            Case "Agregar"

                Me.DeshabilitarForm()

                Me.btnAgregar.Visible = True
                Me.btnBuscar.Visible = True

                Me.btnGuardar.Visible = False
                Me.btnCancelar.Visible = False

            Case "Seleccion"

                Me.btnModificar.Visible = False
                Me.btnCancelar.Visible = False
                Me.btnBuscarNombre.Visible = False

                Me.btnAgregar.Visible = True
                Me.btnBuscar.Visible = True

                Dim cControl As New dbTipoPago
                Me.LlenarGrid(cControl.RecuperarTipoPago)

                Me.LimpiarForm()
                Me.DeshabilitarForm()

        End Select

    End Sub

    Protected Sub btnModificar_Click(sender As Object, e As System.EventArgs) Handles btnModificar.Click
        Me.Session.Item("Accion") = "Modificar"

        Me.HabilitaForm()

        Me.txtIdTipoPago.ReadOnly = True

        Me.btnGuardar.Visible = True
        Me.btnCancelar.Visible = True

        Me.btnModificar.Visible = False
        Me.btnBuscarNombre.Visible = False
    End Sub

    Protected Sub btnBuscar_Click(sender As Object, e As System.EventArgs) Handles btnBuscar.Click

        Me.Session.Item("Accion") = "Buscar"

        Me.btnBuscarNombre.Visible = True
        Me.txtNombreTipoPago.Enabled = True
        Me.btnCancelar.Visible = True

        Me.btnBuscar.Visible = False
        Me.btnAgregar.Visible = False

    End Sub

    Protected Sub btnBuscarNombre_Click(sender As Object, e As System.EventArgs) Handles btnBuscarNombre.Click

        Dim dsDatos As New DataSet
        Dim cControl As New dbTipoPago

        dsDatos = cControl.BuscarTipoPagoPorNombre(Me.txtNombreTipoPago.Text)

        If dsDatos.Tables(0).Rows.Count > 0 Then

            Me.LlenarGrid(dsDatos)
            Me.lblCant.Text = dsDatos.Tables(0).Rows.Count & " Tipo Pago"
        Else

            Me.dgDatos.EmptyDataText = "Datos no encontrado con esos parametros"
            Me.dgDatos.Columns.Clear()
            Me.dgDatos.DataSource = Nothing
            Me.dgDatos.DataBind()

            'Mensaje

        End If

        Me.LimpiarForm()
        Me.btnModificar.Visible = False

    End Sub

    Protected Sub btnGuardar_Click(sender As Object, e As System.EventArgs) Handles btnGuardar.Click

        Dim lEntidad As New TipoPago
        Dim cControl As New dbTipoPago
        Dim dsDatos As New DataSet

        If Me.Session.Item("Accion") = "Agregar" Then
            If String.IsNullOrEmpty(Me.hfTipoPago.Value) = True Then

                lEntidad.nombreTipoPago = Me.txtNombreTipoPago.Text

                If cControl.GuardarTipoPago(lEntidad) > 0 Then

                    DeshabilitarForm()
                    Iniciar()

                    LlenarGrid(cControl.RecuperarTipoPago)

                    Me.LimpiarForm()
                    Me.Session.Item("Accion") = ""

                    Me.btnAgregar.Visible = True
                    Me.btnBuscar.Visible = True


                Else



                End If

            End If

        ElseIf Me.Session.Item("Accion") = "Modificar" Then

            lEntidad.IdTipoPago = Me.txtIdTipoPago.Text
            lEntidad.nombreTipoPago = Me.txtNombreTipoPago.Text

            If cControl.ModificarTipoPago(lEntidad) > 0 Then


                DeshabilitarForm()
                Iniciar()

                LlenarGrid(cControl.RecuperarTipoPago)

                Me.LimpiarForm()
                Me.Session.Item("Accion") = ""

                Me.btnAgregar.Visible = True
                Me.btnBuscar.Visible = True



            Else



            End If

        End If

    End Sub


    Protected Sub dgDatos_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles dgDatos.SelectedIndexChanged

        Dim dsDatos As New DataSet
        Dim cControl As New dbTipoPago

        Me.Session.Item("Accion") = "Seleccion"

        dsDatos = cControl.RecuperarTipoPagoPorId(Me.dgDatos.SelectedRow.Cells(1).Text)

        If dsDatos.Tables(0).Rows.Count > 0 Then

            Me.LlenarGrid(dsDatos)

            'llenar Datos para Modificar
            Me.txtIdTipoPago.Text = dsDatos.Tables(0).Rows(0).Item("IdTipoPago").ToString
            Me.txtNombreTipoPago.Text = dsDatos.Tables(0).Rows(0).Item("nombreTipoPago").ToString

            Me.lblCant.Text = dsDatos.Tables(0).Rows.Count & " Tipo Pago"

            Me.btnAgregar.Visible = False
            Me.btnBuscar.Visible = False
            Me.btnGuardar.Visible = False

            Me.btnModificar.Visible = True
            Me.btnCancelar.Visible = True

            Me.btnBuscarNombre.Visible = False

        End If

        Me.DeshabilitarForm()

    End Sub
End Class
