﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="MantenimientoPais.aspx.vb" Inherits="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <asp:Label ID="Label1" runat="server" Text="Seleccione Pais" CssClass="clear"></asp:Label>
    <br id="leftPan" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <asp:DropDownList ID="NombrePais" runat="server" AutoPostBack="True" DataSourceID="NombrePaisa" DataTextField="PaisNombre" DataValueField="PaisCodigo">
    </asp:DropDownList>
    <asp:SqlDataSource ID="NombrePaisa" runat="server" ConnectionString="<%$ ConnectionStrings:DecameronConnectionString %>" SelectCommand="SELECT [PaisNombre], [PaisCodigo] FROM [Pais] ORDER BY [PaisNombre]"></asp:SqlDataSource>
    <br id="leftPan" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <asp:GridView ID="dgbPaisesCiudad" runat="server" AutoGenerateColumns="False" DataSourceID="InfPaises" style="margin-left: 147px; margin-top: 5px" Width="256px">
        <Columns>
            <asp:BoundField DataField="CiudadNombre" HeaderText="CiudadNombre" SortExpression="CiudadNombre" />
        </Columns>
    </asp:GridView>
    <asp:SqlDataSource ID="InfPaises" runat="server" ConnectionString="<%$ ConnectionStrings:DecameronConnectionString %>" SelectCommand="SELECT [CiudadNombre] FROM [Ciudad] WHERE ([PaisCodigo] = @PaisCodigo)">
        <SelectParameters>
            <asp:ControlParameter ControlID="NombrePais" Name="PaisCodigo" PropertyName="SelectedValue" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>
    <br />
    <br />
    <br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <asp:Label ID="Label2" runat="server" Text="Detalles Pais "></asp:Label>
    <br />
    <br id="leftPan" />
    <asp:GridView ID="Detalles" runat="server" AutoGenerateColumns="False" DataSourceID="DetallesPais" style="margin-left: 158px">
        <Columns>
            <asp:BoundField DataField="PaisNombre" HeaderText="PaisNombre" SortExpression="PaisNombre" />
            <asp:BoundField DataField="PaisContinente" HeaderText="PaisContinente" SortExpression="PaisContinente" />
            <asp:BoundField DataField="PaisRegion" HeaderText="PaisRegion" SortExpression="PaisRegion" />
            <asp:BoundField DataField="PaisNombreLocal" HeaderText="PaisNombreLocal" SortExpression="PaisNombreLocal" />
        </Columns>
    </asp:GridView>
    <asp:SqlDataSource ID="DetallesPais" runat="server" ConnectionString="<%$ ConnectionStrings:DecameronConnectionString %>" SelectCommand="SELECT [PaisNombre], [PaisContinente], [PaisRegion], [PaisNombreLocal] FROM [Pais] WHERE ([PaisCodigo] = @PaisCodigo)">
        <SelectParameters>
            <asp:ControlParameter ControlID="NombrePais" Name="PaisCodigo" PropertyName="SelectedValue" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>
    <br />
</asp:Content>

<asp:Content ID="Content2" runat="server" contentplaceholderid="HeadContent">
    <style type="text/css">
        .nuevoEstilo1 {
            position: fixed;
            z-index: auto;
        }
    </style>
</asp:Content>


