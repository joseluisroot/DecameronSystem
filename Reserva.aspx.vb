﻿Imports System.Data
Partial Class _Default
    Inherits cBase
    Dim dtPlanes As New DataTable With {.TableName = "dtPlanes"}
    Dim dtHabitaciones As New DataTable With {.TableName = "dtHabitaciones"}
    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Try
            Me.finSession()
            If Me.IsPostBack = False Then
                'CargarDatos
                dtPlanes.Rows.Clear()
                dtHabitaciones.Rows.Clear()

                Dim dsDatos As New DataSet
                Dim cCtrlCliente As New dbCliente
                Dim cCtrlReserva As New dbReserva
                Dim cCtrlPlan As New dbPlan
                dtPlanes = DirectCast(cCtrlPlan.RecuperarPlan.Tables(0), DataTable)
<<<<<<< HEAD
                dtHabitaciones = DirectCast(cCtrlHabitaciones.RecuperarHabitacionDisponible.Tables(0), DataTable)

                If dtHabitaciones.Rows.Count = 0 Then
                    'MsgBox("No hay habitaciones disponibles en este momento", MsgBoxStyle.Exclamation, "Sistema de Reserva Decameron")
                    'Response.Redirect("Principal.aspx", False)
                End If
=======
>>>>>>> origin/master

                If dtPlanes.Rows.Count > 0 Then
                    With ddlPromociones
                        .DataSource = dtPlanes
                        .DataValueField = "idPlan"
                        .DataTextField = "Nombre_plan"
                        .DataBind()
                    End With
                Else
                    'MsgBox("No hay planes disponibles. Consulte al administrador", MsgBoxStyle.Exclamation, "Sistema de Reserva Decameron")
                    'Response.Redirect("Principal.aspx", False)
                End If
                dsDatos = cCtrlReserva.RecuperarReservaCliente
                If dsDatos.Tables(0).Rows.Count > 0 Then
                    LlenarGrid(dsDatos)
                Else
                    'MsgBox("No hay registros", MsgBoxStyle.Exclamation, "Sistema de Reserva Decameron")
                End If
                Iniciar()
                DeshabilitarForm()
            End If
        Catch ex As Exception
            MsgBox(ex.ToString, MsgBoxStyle.Exclamation, "Sistema de Reserva Decameron")
            MsgBox("Error al cargar el formulario", MsgBoxStyle.Exclamation, "Sistema de Reserva Decameron")
        End Try
    End Sub

    Sub reiniciar()
        'Me.btnAgregar.Visible = False
        'Me.btnModificar.Visible = False
        'Me.btnEliminar.Visible = False
        Me.btnCancelar.Visible = False
        Me.btnModificar.Visible = False
        Me.btnGuardar.Visible = False
        Me.btnBuscar.Visible = False
    End Sub

    Sub LlenarGrid(ByVal Datos As DataSet)
        Try
            If Datos.Tables(0).Rows.Count > 0 Then
                Me.dgvReservas.Columns.Clear()
                Me.dgvReservas.DataSource = Datos.Tables(0)

                Dim btnSel As New CommandField
                btnSel.ShowSelectButton = True
                btnSel.SelectText = "Seleccionar"
                Me.dgvReservas.Columns.Add(btnSel)

                Dim IdReserva As New BoundField
                IdReserva.DataField = "idReserva"
                IdReserva.HeaderText = "Codigo de Reserva"
                IdReserva.ControlStyle.Width = 150
                Me.dgvReservas.Columns.Add(IdReserva)

                Dim idPlan As New BoundField
                idPlan.DataField = "idPlan"
                idPlan.HeaderText = "Codigo cliente"
                idPlan.Visible = False
                Me.dgvReservas.Columns.Add(idPlan)

                Dim ocupantes As New BoundField
                ocupantes.DataField = "ocupantes"
                ocupantes.HeaderText = "ocupantes"
                ocupantes.Visible = False
                Me.dgvReservas.Columns.Add(ocupantes)

                Dim fecha_inicio As New BoundField
                fecha_inicio.DataField = "fecha_inicio"
                fecha_inicio.HeaderText = "fecha_inicio"
                fecha_inicio.Visible = False
                Me.dgvReservas.Columns.Add(fecha_inicio)

                Dim fecha_final As New BoundField
                fecha_final.DataField = "fecha_final"
                fecha_final.HeaderText = "fecha_final"
                fecha_final.Visible = False
                Me.dgvReservas.Columns.Add(fecha_final)

                Dim idCliente As New BoundField
                idCliente.DataField = "IdCliente"
                idCliente.HeaderText = "Codigo de Cliente"
                idCliente.ControlStyle.Width = 150
                Me.dgvReservas.Columns.Add(idCliente)

                Dim Nombre As New BoundField
                Nombre.DataField = "Nombre"
                Nombre.HeaderText = "Nombre"
                Nombre.ControlStyle.Width = 500
                Me.dgvReservas.Columns.Add(Nombre)

                Dim Apellidos As New BoundField
                Apellidos.DataField = "Apellidos"
                Apellidos.HeaderText = "Apellidos"
                Apellidos.ControlStyle.Width = 500
                Me.dgvReservas.Columns.Add(Apellidos)

                Dim Edad As New BoundField
                Edad.DataField = "Edad"
                Edad.HeaderText = "Edad cliente"
                Edad.Visible = False
                Me.dgvReservas.Columns.Add(Edad)

                Dim Sexo As New BoundField
                Sexo.DataField = "Sexo"
                Sexo.HeaderText = "Sexo"
                Sexo.Visible = False
                Me.dgvReservas.Columns.Add(Sexo)

                Dim FechaIngreso As New BoundField
                FechaIngreso.DataField = "FechaIngreso"
                FechaIngreso.HeaderText = "Fecha ingreso"
                FechaIngreso.Visible = False
                Me.dgvReservas.Columns.Add(FechaIngreso)

                Dim Direccion As New BoundField
                Direccion.DataField = "Direccion"
                Direccion.HeaderText = "Direccion"
                Direccion.Visible = False
                Me.dgvReservas.Columns.Add(Direccion)

                Dim Telefono As New BoundField
                Telefono.DataField = "Telefono"
                Telefono.HeaderText = "Telefono"
                Telefono.ControlStyle.Width = 100
                Me.dgvReservas.Columns.Add(Telefono)

                Dim Email As New BoundField
                Email.DataField = "Email"
                Email.HeaderText = "Email"
                Email.Visible = False
                Me.dgvReservas.Columns.Add(Email)

                Me.dgvReservas.DataBind()
            End If
        Catch ex As Exception
            MsgBox(ex.ToString, MsgBoxStyle.Exclamation, "Error de envio a cuadricula")
        End Try
    End Sub

    Sub Iniciar()
        Me.btnGuardar.Visible = False
        Me.btnCancelar.Visible = False
        Me.btnModificar.Visible = False
        Me.btnBuscarNombre.Visible = False
    End Sub

    Sub HabilitaForm()
        'CLIENTE
        Me.txtNombreCliente.ReadOnly = False
        Me.txtApellidosCliente.ReadOnly = False
        Me.txtEdad.ReadOnly = False
        Me.txtDireccion.ReadOnly = False
        Me.txtTelefono.ReadOnly = False
        Me.txtSexo.ReadOnly = False
        Me.txtEmail.ReadOnly = False

        Me.txtNombreCliente.Enabled = True
        Me.txtApellidosCliente.Enabled = True
        Me.txtEdad.Enabled = True
        Me.txtDireccion.Enabled = True
        Me.txtTelefono.Enabled = True
        Me.txtSexo.Enabled = True
        Me.txtEmail.Enabled = True

        'RESERVA
        Me.txtidReserva.ReadOnly = True
        Me.txtOcupantes.ReadOnly = False
        Me.txtOcupantes.Enabled = True
        'Me.calenDesde.Enabled = False
        'Me.calenHasta.Enabled = False
        Me.ddlPromociones.Enabled = True
        Me.dgvReservas.Enabled = True
    End Sub

    Sub DeshabilitarForm()
        'CLIENTE
        Me.txtNombreCliente.ReadOnly = True
        Me.txtApellidosCliente.ReadOnly = True
        Me.txtEdad.ReadOnly = True
        Me.txtDireccion.ReadOnly = True
        Me.txtTelefono.ReadOnly = True
        Me.txtEdad.ReadOnly = True
        Me.txtEmail.ReadOnly = True
        'RESERVA
        Me.txtidReserva.ReadOnly = True
        Me.txtOcupantes.ReadOnly = True
        'Me.calenDesde.Enabled = False
        'Me.calenHasta.Enabled = False
        Me.ddlPromociones.Enabled = False
        'Me.dgvReservas.Enabled = False
    End Sub

    Sub LimpiarForm()
        Me.txtNombreCliente.Text = ""
        Me.txtApellidosCliente.Text = ""
        Me.txtEdad.Text = ""
        Me.txtDireccion.Text = ""
        Me.txtTelefono.Text = ""
        Me.txtSexo.Text = ""
        Me.txtEmail.Text = ""
        'RESERVA
        Me.txtOcupantes.Text = ""
        'Me.calenDesde.SelectedDate = Date.Now
        'Me.calenHasta.SelectedDate = Date.Now
        Me.ddlPromociones.SelectedIndex = 0
    End Sub

    Protected Sub btnAgregar_Click(sender As Object, e As System.EventArgs) Handles btnAgregar.Click
        Me.Session.Item("Accion") = "Agregar"
        Me.btnAgregar.Visible = False
        Me.btnGuardar.Visible = True
        Me.btnCancelar.Visible = True
        Me.btnBuscar.Visible = False
        HabilitaForm()
    End Sub

    Protected Sub btnModificar_Click(sender As Object, e As System.EventArgs) Handles btnModificar.Click
        Me.Session.Item("Accion") = "Modificar"

        Me.HabilitaForm()
        Me.txtidReserva.ReadOnly = True
        Me.btnGuardar.Visible = True
        Me.btnCancelar.Visible = True
        Me.btnModificar.Visible = False
        Me.btnBuscarNombre.Visible = False
    End Sub

    Protected Sub btnGuardar_Click(sender As Object, e As System.EventArgs) Handles btnGuardar.Click
        Dim lEntidad As New Cliente
        Dim lEntidadReserva As New Reserva
        Dim cCtrlCliente As New dbCliente
        Dim cCtrlReserva As New dbReserva
        Dim dsDatos As New DataSet

        Dim cCtrlHabitaciones As New dbHabitacion
        dtHabitaciones = DirectCast(cCtrlHabitaciones.RecuperarHabitacionDisponible.Tables(0), DataTable)

        If dtHabitaciones.Rows.Count = 0 Then
            MsgBox("No hay habitaciones disponibles en este momento", MsgBoxStyle.Exclamation, "Sistema de Reserva Decameron")
            Response.Redirect("Principal.aspx", False)
        End If


        If Me.Session.Item("Accion") = "Agregar" Then
            lEntidad.NombreCliente = Me.txtNombreCliente.Text
            lEntidad.ApellidosCliente = Me.txtApellidosCliente.Text
            lEntidad.Edad = Me.txtEdad.Text
            lEntidad.Direccion = Me.txtDireccion.Text
            lEntidad.Telefono = Me.txtTelefono.Text
            lEntidad.Sexo = Me.txtSexo.Text
            lEntidad.Email = Me.txtEmail.Text
            lEntidad.FechaInfreso = Date.Now
            lEntidadReserva.ocupantes = Me.txtOcupantes.Text
            lEntidadReserva.IdPlan = Me.ddlPromociones.SelectedValue


            If cCtrlReserva.GuardarReserva(lEntidadReserva, lEntidad) > 0 Then
                MsgBox("Registro guardado exitosamente", MsgBoxStyle.Information, "Sistema Decameron")
                DeshabilitarForm()
                Iniciar()
                LlenarGrid(cCtrlReserva.RecuperarReservaCliente)
                Me.LimpiarForm()
                Me.Session.Item("Accion") = ""
                Me.btnAgregar.Visible = True
                Me.btnBuscar.Visible = True
            End If


        ElseIf Me.Session.Item("Accion") = "Modificar" Then
            If cCtrlReserva.ModificarReserva(lEntidadReserva, lEntidad) > 0 Then
                DeshabilitarForm()
                Iniciar()
                LlenarGrid(cCtrlReserva.RecuperarReservaCliente)

                Me.LimpiarForm()
                Me.Session.Item("Accion") = ""
                Me.btnAgregar.Visible = True
                Me.btnBuscar.Visible = True
            Else

            End If

        End If
    End Sub

    Protected Sub btnCancelar_Click(sender As Object, e As System.EventArgs) Handles btnCancelar.Click
        Select Case Me.Session.Item("Accion")
            Case "Buscar"

                Me.btnAgregar.Visible = True
                Me.btnBuscar.Visible = True

                Me.btnBuscarNombre.Visible = False
                Me.btnCancelar.Visible = False

                Me.txtNombreCliente.Enabled = False

                Dim cControl As New dbReserva
                Me.LlenarGrid(cControl.RecuperarReservaCliente)

            Case "Modificar"
                Me.btnModificar.Visible = False
                Me.btnCancelar.Visible = False

                Me.btnGuardar.Visible = False
                Me.btnBuscarNombre.Visible = False

                Me.btnAgregar.Visible = True
                Me.btnBuscar.Visible = True

                Me.LimpiarForm()
                Me.DeshabilitarForm()

                Dim cControl As New dbReserva
                Me.LlenarGrid(cControl.RecuperarReservaCliente)

            Case "Agregar"

                Me.DeshabilitarForm()

                Me.btnAgregar.Visible = True
                Me.btnBuscar.Visible = True

                Me.btnGuardar.Visible = False
                Me.btnCancelar.Visible = False

            Case "Seleccion"

                Me.btnModificar.Visible = False
                Me.btnCancelar.Visible = False
                Me.btnBuscarNombre.Visible = False
                Me.btnAgregar.Visible = True
                Me.btnBuscar.Visible = True

                Dim cControl As New dbReserva
                Me.LlenarGrid(cControl.RecuperarReservaCliente)
                Me.LimpiarForm()
                Me.DeshabilitarForm()
        End Select
    End Sub

    Protected Sub btnBuscar_Click(sender As Object, e As System.EventArgs) Handles btnBuscar.Click
        Dim dsDatos As New DataSet
        Dim cControl As New dbReserva

        dsDatos = cControl.BuscarReservaPorNombre(Me.txtNombreCliente.Text)
        If dsDatos.Tables(0).Rows.Count > 0 Then
            Me.LlenarGrid(dsDatos)
        Else
            Me.dgvReservas.EmptyDataText = "Datos no encontrado con esos parametros"
            Me.dgvReservas.Columns.Clear()
            Me.dgvReservas.DataSource = Nothing
            Me.dgvReservas.DataBind()
        End If

        Me.LimpiarForm()
        Me.btnModificar.Visible = False
    End Sub

    Protected Sub btnBuscarNombre_Click(sender As Object, e As System.EventArgs) Handles btnBuscarNombre.Click
        Me.Session.Item("Accion") = "Buscar"

        Me.btnBuscarNombre.Visible = True
        Me.txtNombreCliente.Enabled = True
        Me.btnCancelar.Visible = True
        Me.btnBuscar.Visible = False
        Me.btnAgregar.Visible = False
    End Sub

    Protected Sub dgvReservas_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles dgvReservas.SelectedIndexChanged
        Dim dsDatos As New DataSet
        Dim cControl As New dbReserva

        Me.Session.Item("Accion") = "Seleccion"

        dsDatos = cControl.RecuperarReservaClientePorId(Me.dgvReservas.SelectedRow.Cells(1).Text)

        If dsDatos.Tables(0).Rows.Count > 0 Then

            Me.LlenarGrid(dsDatos)

            'CLIENTE
            Me.txtNombreCliente.Text = dsDatos.Tables(0).Rows(0).Item("Nombre").ToString
            Me.txtApellidosCliente.Text = dsDatos.Tables(0).Rows(0).Item("Apellidos").ToString
            Me.txtEdad.Text = dsDatos.Tables(0).Rows(0).Item("Edad").ToString
            Me.txtDireccion.Text = dsDatos.Tables(0).Rows(0).Item("Direccion").ToString
            Me.txtTelefono.Text = dsDatos.Tables(0).Rows(0).Item("Telefono").ToString
            Me.txtSexo.Text = dsDatos.Tables(0).Rows(0).Item("Sexo").ToString
            Me.txtEmail.Text = dsDatos.Tables(0).Rows(0).Item("Email").ToString
            'RESERVA
            Me.txtidReserva.Text = dsDatos.Tables(0).Rows(0).Item("idReserva").ToString
            Me.ddlPromociones.SelectedValue = dsDatos.Tables(0).Rows(0).Item("idPlan").ToString
            Me.txtOcupantes.Text = dsDatos.Tables(0).Rows(0).Item("ocupantes").ToString


            Me.btnAgregar.Visible = False
            Me.btnBuscar.Visible = False
            Me.btnGuardar.Visible = False

            Me.btnModificar.Visible = True
            Me.btnCancelar.Visible = True

            Me.btnBuscarNombre.Visible = False

        End If

        Me.DeshabilitarForm()
    End Sub
End Class
