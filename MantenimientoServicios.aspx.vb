﻿Imports System.Data
Partial Class _Default
    Inherits cBase

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Me.finSession()

        If Me.IsPostBack = False Then

            'CargarDatos

            Dim dsDatos As New DataSet
            Dim cControl As New dbServicios
            dsDatos = cControl.RecuperarServicios

            If dsDatos.Tables(0).Rows.Count > 0 Then
                LlenarGrid(dsDatos)
            Else
                Me.lblCant.Text = dsDatos.Tables(0).Rows.Count & " Servicios"
            End If

            Iniciar()
            DeshabilitarForm()
        End If
    End Sub

    Sub reiniciar()
        Me.btnCancelar.Visible = False
        Me.btnModificar.Visible = False
        Me.btnGuardar.Visible = False
    End Sub

    Sub LlenarGrid(ByVal Datos As DataSet)

        If Datos.Tables(0).Rows.Count > 0 Then
            Me.dgDatos.Columns.Clear()
            Me.dgDatos.DataSource = Datos.Tables(0)

            Dim btnSel As New CommandField
            btnSel.ShowSelectButton = True
            btnSel.SelectText = "Seleccionar"
            Me.dgDatos.Columns.Add(btnSel)

            Dim IdServicio As New BoundField
            IdServicio.DataField = "IdServicio"
            IdServicio.HeaderText = "Codigo de servicio"
            Me.dgDatos.Columns.Add(IdServicio)

            Dim Nombre As New BoundField
            Nombre.DataField = "Nombre"
            Nombre.HeaderText = "Nombre del servicio"
            Me.dgDatos.Columns.Add(Nombre)

            Dim precio As New BoundField
            precio.DataField = "precio"
            precio.HeaderText = "valor del servicio"
            Me.dgDatos.Columns.Add(precio)

            Me.dgDatos.DataBind()
            Me.lblCant.Text = Datos.Tables(0).Rows.Count & " Servicios"
        End If

    End Sub

    Sub Iniciar()
        Me.btnGuardar.Visible = False
        Me.btnCancelar.Visible = False
        Me.btnModificar.Visible = False
        Me.btnBuscarNombre.Visible = False
    End Sub

    Sub HabilitaForm()
        Me.txtIdServicio.ReadOnly = True
        Me.txtNombre.Enabled = True
        Me.txtPrecio.Enabled = True
    End Sub

    Sub DeshabilitarForm()
        Me.txtIdServicio.ReadOnly = True
        Me.txtNombre.Enabled = False
        Me.txtPrecio.Enabled = False
    End Sub

    Sub LimpiarForm()
        Me.txtIdServicio.Text = ""
        Me.txtNombre.Text = ""
        Me.txtPrecio.Text = ""
    End Sub

    Protected Sub btnAgregar_Click(sender As Object, e As System.EventArgs) Handles btnAgregar.Click
        Me.Session.Item("Accion") = "Agregar"
        Me.btnAgregar.Visible = False
        Me.btnGuardar.Visible = True
        Me.btnCancelar.Visible = True
        Me.btnBuscar.Visible = False
        HabilitaForm()
    End Sub

    Protected Sub btnGuardar_Click(sender As Object, e As System.EventArgs) Handles btnGuardar.Click
        Dim lEntidad As New Servicios
        Dim cControl As New dbServicios
        Dim dsDatos As New DataSet

        If Me.Session.Item("Accion") = "Agregar" Then
            If String.IsNullOrEmpty(Me.hfServicios.Value) = True Then
                lEntidad.Nombre = Me.txtNombre.Text
                lEntidad.precio = Val(Me.txtPrecio.Text)
                If cControl.GuardarServicios(lEntidad) > 0 Then
                    DeshabilitarForm()
                    Iniciar()
                    LlenarGrid(cControl.RecuperarServicios)

                    Me.LimpiarForm()
                    Me.Session.Item("Accion") = ""

                    Me.btnAgregar.Visible = True
                    Me.btnBuscar.Visible = True
                Else

                End If

            End If

        ElseIf Me.Session.Item("Accion") = "Modificar" Then

            lEntidad.IdServicio = Me.txtIdServicio.Text
            lEntidad.Nombre = Me.txtNombre.Text
            lEntidad.precio = Val(Me.txtPrecio.Text)

            If cControl.ModificarServicios(lEntidad) > 0 Then
                DeshabilitarForm()
                Iniciar()
                LlenarGrid(cControl.RecuperarServicios)

                Me.LimpiarForm()
                Me.Session.Item("Accion") = ""

                Me.btnAgregar.Visible = True
                Me.btnBuscar.Visible = True
            Else
            End If

        End If
    End Sub

    Protected Sub btnModificar_Click(sender As Object, e As System.EventArgs) Handles btnModificar.Click
        Me.Session.Item("Accion") = "Modificar"

        Me.HabilitaForm()
        Me.txtIdServicio.ReadOnly = True
        Me.btnGuardar.Visible = True
        Me.btnCancelar.Visible = True
        Me.btnModificar.Visible = False
        Me.btnBuscarNombre.Visible = False
    End Sub

    Protected Sub btnCancelar_Click(sender As Object, e As System.EventArgs) Handles btnCancelar.Click
        Select Case Me.Session.Item("Accion")
            Case "Buscar"

                Me.btnAgregar.Visible = True
                Me.btnBuscar.Visible = True

                Me.btnBuscarNombre.Visible = False
                Me.btnCancelar.Visible = False

                Me.txtNombre.Enabled = False

                Dim cControl As New dbServicios
                Me.LlenarGrid(cControl.RecuperarServicios)

            Case "Modificar"
                Me.btnModificar.Visible = False
                Me.btnCancelar.Visible = False

                Me.btnGuardar.Visible = False
                Me.btnBuscarNombre.Visible = False

                Me.btnAgregar.Visible = True
                Me.btnBuscar.Visible = True

                Me.LimpiarForm()
                Me.DeshabilitarForm()

                Dim cControl As New dbServicios
                Me.LlenarGrid(cControl.RecuperarServicios)

            Case "Agregar"

                Me.DeshabilitarForm()

                Me.btnAgregar.Visible = True
                Me.btnBuscar.Visible = True

                Me.btnGuardar.Visible = False
                Me.btnCancelar.Visible = False

            Case "Seleccion"

                Me.btnModificar.Visible = False
                Me.btnCancelar.Visible = False
                Me.btnBuscarNombre.Visible = False
                Me.btnAgregar.Visible = True
                Me.btnBuscar.Visible = True

                Dim cControl As New dbServicios
                Me.LlenarGrid(cControl.RecuperarServicios)
                Me.LimpiarForm()
                Me.DeshabilitarForm()
        End Select
    End Sub

    Protected Sub btnBuscar_Click(sender As Object, e As System.EventArgs) Handles btnBuscar.Click
        Dim dsDatos As New DataSet
        Dim cControl As New dbServicios

        dsDatos = cControl.BuscarServiciosPorNombre(Me.txtNombre.Text)
        If dsDatos.Tables(0).Rows.Count > 0 Then
            Me.LlenarGrid(dsDatos)
            Me.lblCant.Text = dsDatos.Tables(0).Rows.Count & " Servicios"
        Else
            Me.dgDatos.EmptyDataText = "Datos no encontrado con esos parametros"
            Me.dgDatos.Columns.Clear()
            Me.dgDatos.DataSource = Nothing
            Me.dgDatos.DataBind()
        End If

        Me.LimpiarForm()
        Me.btnModificar.Visible = False
    End Sub

    Protected Sub btnBuscarNombre_Click(sender As Object, e As System.EventArgs) Handles btnBuscarNombre.Click
        Me.Session.Item("Accion") = "Buscar"

        Me.btnBuscarNombre.Visible = True
        Me.txtNombre.Enabled = True
        Me.btnCancelar.Visible = True
        Me.btnBuscar.Visible = False
        Me.btnAgregar.Visible = False
    End Sub

    Protected Sub dgDatos_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles dgDatos.SelectedIndexChanged
        Dim dsDatos As New DataSet
        Dim cControl As New dbServicios

        Me.Session.Item("Accion") = "Seleccion"

        dsDatos = cControl.RecuperarServiciosPorId(Me.dgDatos.SelectedRow.Cells(1).Text)

        If dsDatos.Tables(0).Rows.Count > 0 Then

            Me.LlenarGrid(dsDatos)

            'llenar Datos para Modificar
            Me.txtIdServicio.Text = dsDatos.Tables(0).Rows(0).Item("IdServicio").ToString
            Me.txtNombre.Text = dsDatos.Tables(0).Rows(0).Item("Nombre").ToString
            Me.txtPrecio.Text = dsDatos.Tables(0).Rows(0).Item("precio").ToString

            Me.lblCant.Text = dsDatos.Tables(0).Rows.Count & " Servicios"

            Me.btnAgregar.Visible = False
            Me.btnBuscar.Visible = False
            Me.btnGuardar.Visible = False

            Me.btnModificar.Visible = True
            Me.btnCancelar.Visible = True

            Me.btnBuscarNombre.Visible = False

        End If

        Me.DeshabilitarForm()
    End Sub
End Class