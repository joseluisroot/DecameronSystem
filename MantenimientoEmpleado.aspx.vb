﻿Imports System.Data

Partial Class MantenimientoEmpleado
    Inherits cBase
    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        Me.finSession()

        If Me.IsPostBack = False Then

            'CargarDatos

            Dim dsDatos As New DataSet
            Dim cControl As New dbEmpleado


            dsDatos = cControl.RecuperarEmpleado


            If dsDatos.Tables(0).Rows.Count > 0 Then
                LlenarGrid(dsDatos)
            Else
                Me.lblCant.Text = dsDatos.Tables(0).Rows.Count & " Empleado"
            End If

            Iniciar()
            DeshabilitarForm()
            llenarEstado()
            llenarPerfil()

        End If


    End Sub


    Sub llenarPerfil()


        Dim cControl As New dbPerfil
        Dim dsDatos As New DataSet

        dsDatos = cControl.RecuperarPerfil()

        If dsDatos.Tables(0).Rows.Count > 0 Then

            Me.ddlbPerfil.DataSource = dsDatos.Tables(0)
            Me.ddlbPerfil.DataTextField = "NombrePerfil"
            Me.ddlbPerfil.DataValueField = "IdPerfil"
            Me.ddlbPerfil.DataBind()
        End If
        Me.ddlbPerfil.Items.Insert("0", New ListItem("Seleccionar...", "0"))

    End Sub

    Sub llenarEstado()


        Dim cControl As New dbEstado
        Dim dsDatos As New DataSet

        dsDatos = cControl.RecuperarEstados()

        If dsDatos.Tables(0).Rows.Count > 0 Then

            Me.ddlbEstado.DataSource = dsDatos.Tables(0)
            Me.ddlbEstado.DataTextField = "NombreEstado"
            Me.ddlbEstado.DataValueField = "IdEstado"
            Me.ddlbEstado.DataBind()
        End If
        Me.ddlbEstado.Items.Insert("0", New ListItem("Seleccionar...", "0"))

    End Sub


    Sub reiniciar()

        Me.btnCancelar.Visible = False
        Me.btnDeschabilitar.Visible = False
        Me.btnModificar.Visible = False
        Me.btnGuardar.Visible = False


    End Sub


    Sub LlenarGrid(ByVal Datos As DataSet)


        If Datos.Tables(0).Rows.Count > 0 Then

            Me.dgDatos.Columns.Clear()
            Me.dgDatos.DataSource = Datos.Tables(0)

            Dim btnSel As New CommandField
            btnSel.ShowSelectButton = True
            btnSel.SelectText = "Seleccionar"
            Me.dgDatos.Columns.Add(btnSel)

            Dim IdEmpleado As New BoundField
            IdEmpleado.DataField = "IdEmpleado"
            IdEmpleado.HeaderText = "Codigo de Empleado"
            Me.dgDatos.Columns.Add(IdEmpleado)

            Dim Nombre As New BoundField
            Nombre.DataField = "Nombre"
            Nombre.HeaderText = "Nombre Empleado"
            Me.dgDatos.Columns.Add(Nombre)

            Dim Apellido As New BoundField
            Apellido.DataField = "Apellido"
            Apellido.HeaderText = "Apellido"
            Me.dgDatos.Columns.Add(Apellido)

            Dim Usuario As New BoundField
            Usuario.DataField = "Usuario"
            Usuario.HeaderText = "Usuario"
            Me.dgDatos.Columns.Add(Usuario)

            Dim Clave As New BoundField
            Clave.DataField = "Clave"
            Clave.HeaderText = "Clave"
            Me.dgDatos.Columns.Add(Clave)


            Dim IdPerfil As New BoundField
            IdPerfil.DataField = "Perfil"
            IdPerfil.HeaderText = "Pefil"
            Me.dgDatos.Columns.Add(IdPerfil)

            Me.dgDatos.DataBind()
            Me.lblCant.Text = Datos.Tables(0).Rows.Count & "  Empleados"
        End If

    End Sub

    Sub Iniciar()

        Me.btnGuardar.Visible = False
        Me.btnCancelar.Visible = False
        Me.btnModificar.Visible = False
        Me.btnDeschabilitar.Visible = False
        Me.btnBuscarNombre.Visible = False

    End Sub


    Sub HabilitaForm()

        Me.txtIdEmpleado.ReadOnly = True
        Me.txtNombre.Enabled = True
        Me.txtApellido.Enabled = True
        Me.txtUsuario.Enabled = True
        Me.txtClave.Enabled = True
        Me.ddlbEstado.Enabled = True
        Me.ddlbPerfil.Enabled = True

    End Sub

    Sub DeshabilitarForm()

        Me.txtIdEmpleado.ReadOnly = True
        Me.txtNombre.Enabled = False
        Me.txtApellido.Enabled = False
        Me.txtClave.Enabled = False
        Me.txtUsuario.Enabled = False
        Me.ddlbEstado.Enabled = False
        Me.ddlbPerfil.Enabled = False
    End Sub

    Sub LimpiarForm()

        Me.txtIdEmpleado.Text = ""
        Me.txtNombre.Text = ""
        Me.txtApellido.Text = ""
        Me.txtUsuario.Text = ""
        Me.txtClave.Text = ""
        Me.ddlbEstado.ClearSelection()
        Me.ddlbPerfil.ClearSelection()

    End Sub

    Protected Sub btnAgregar_Click(sender As Object, e As System.EventArgs) Handles btnAgregar.Click
        Me.Session.Item("Accion") = "Agregar"

        Me.btnAgregar.Visible = False
        Me.btnGuardar.Visible = True
        Me.btnCancelar.Visible = True

        Me.btnBuscar.Visible = False

        HabilitaForm()
    End Sub

    Protected Sub btnCancelar_Click(sender As Object, e As System.EventArgs) Handles btnCancelar.Click

        Select Case Me.Session.Item("Accion")
            Case "Buscar"

                Me.btnAgregar.Visible = True
                Me.btnBuscar.Visible = True

                Me.btnBuscarNombre.Visible = False
                Me.btnCancelar.Visible = False

                Me.txtNombre.Enabled = False

                Dim cControl As New dbEmpleado
                Me.LlenarGrid(cControl.RecuperarEmpleado)

            Case "Modificar"


                Me.btnModificar.Visible = False
                Me.btnCancelar.Visible = False

                Me.btnGuardar.Visible = False
                Me.btnBuscarNombre.Visible = False


                Me.btnAgregar.Visible = True
                Me.btnBuscar.Visible = True

                Me.LimpiarForm()
                Me.DeshabilitarForm()

                Dim cControl As New dbEmpleado
                Me.LlenarGrid(cControl.RecuperarEmpleado)

            Case "Agregar"

                Me.DeshabilitarForm()

                Me.btnAgregar.Visible = True
                Me.btnBuscar.Visible = True

                Me.btnGuardar.Visible = False
                Me.btnCancelar.Visible = False

            Case "Seleccion"

                Me.btnModificar.Visible = False
                Me.btnCancelar.Visible = False
                Me.btnBuscarNombre.Visible = False

                Me.btnAgregar.Visible = True
                Me.btnBuscar.Visible = True

                Dim cControl As New dbEmpleado
                Me.LlenarGrid(cControl.RecuperarEmpleado)



                Me.LimpiarForm()
                Me.DeshabilitarForm()

        End Select

    End Sub

    Protected Sub btnModificar_Click(sender As Object, e As System.EventArgs) Handles btnModificar.Click
        Me.Session.Item("Accion") = "Modificar"

        Me.HabilitaForm()

        Me.txtIdEmpleado.ReadOnly = True

        Me.btnGuardar.Visible = True
        Me.btnCancelar.Visible = True

        Me.btnModificar.Visible = False
        Me.btnBuscarNombre.Visible = False
    End Sub

    Protected Sub btnBuscar_Click(sender As Object, e As System.EventArgs) Handles btnBuscar.Click

        Me.Session.Item("Accion") = "Buscar"

        Me.btnBuscarNombre.Visible = True
        Me.txtNombre.Enabled = True
        Me.btnCancelar.Visible = True

        Me.btnBuscar.Visible = False
        Me.btnAgregar.Visible = False

    End Sub

    Protected Sub btnBuscarNombre_Click(sender As Object, e As System.EventArgs) Handles btnBuscarNombre.Click

        Dim dsDatos As New DataSet
        Dim cControl As New dbEmpleado

        dsDatos = cControl.BuscarEmpleadoPorNombre(Me.txtNombre.Text)

        If dsDatos.Tables(0).Rows.Count > 0 Then

            Me.LlenarGrid(dsDatos)
            Me.lblCant.Text = dsDatos.Tables(0).Rows.Count & " Empleados"
        Else

            Me.dgDatos.EmptyDataText = "Datos no encontrado con esos parametros"
            Me.dgDatos.Columns.Clear()
            Me.dgDatos.DataSource = Nothing
            Me.dgDatos.DataBind()

            'Mensaje

        End If

        Me.LimpiarForm()
        Me.btnModificar.Visible = False

    End Sub

    Protected Sub btnGuardar_Click(sender As Object, e As System.EventArgs) Handles btnGuardar.Click

        Dim lEntidad As New Empleado
        Dim cControl As New dbEmpleado
        Dim dsDatos As New DataSet

        If Me.Session.Item("Accion") = "Agregar" Then
            If String.IsNullOrEmpty(Me.hfEmpleado.Value) = True Then

                lEntidad.Nombre = Me.txtNombre.Text
                lEntidad.Apellido = Me.txtApellido.Text
                lEntidad.Usuario = Me.txtUsuario.Text
                lEntidad.Clave = Me.txtClave.Text
                lEntidad.IdEstado = Me.ddlbEstado.SelectedValue
                lEntidad.IdPerfil = Me.ddlbPerfil.SelectedValue

                If cControl.GuardarEmpleado(lEntidad) > 0 Then

                    DeshabilitarForm()
                    Iniciar()

                    LlenarGrid(cControl.RecuperarEmpleado)

                    Me.LimpiarForm()
                    Me.Session.Item("Accion") = ""

                    Me.btnAgregar.Visible = True
                    Me.btnBuscar.Visible = True

                Else

                End If

            End If


        ElseIf Me.Session.Item("Accion") = "Modificar" Then

            lEntidad.IdEmpleado = Me.txtIdEmpleado.Text
            lEntidad.Nombre = Me.txtNombre.Text
            lEntidad.Apellido = Me.txtApellido.Text
            lEntidad.Usuario = Me.txtUsuario.Text
            lEntidad.Clave = Me.txtClave.Text
            lEntidad.IdEstado = Me.ddlbEstado.Text
            lEntidad.IdPerfil = Me.ddlbPerfil.Text

            If cControl.ModificarEmpleado(lEntidad) > 0 Then


                DeshabilitarForm()
                Iniciar()

                LlenarGrid(cControl.RecuperarEmpleado)

                Me.LimpiarForm()
                Me.Session.Item("Accion") = ""

                Me.btnAgregar.Visible = True
                Me.btnBuscar.Visible = True

            Else

            End If

        End If

    End Sub


    Protected Sub dgDatos_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles dgDatos.SelectedIndexChanged

        Dim dsDatos As New DataSet
        Dim cControl As New dbEmpleado

        Me.Session.Item("Accion") = "Seleccion"

        dsDatos = cControl.RecuperarEmpleadoPorId(Me.dgDatos.SelectedRow.Cells(1).Text)

        If dsDatos.Tables(0).Rows.Count > 0 Then

            Me.LlenarGrid(dsDatos)

            'llenar Datos para Modificar
            Me.txtIdEmpleado.Text = dsDatos.Tables(0).Rows(0).Item("IdEmpleado").ToString
            Me.txtNombre.Text = dsDatos.Tables(0).Rows(0).Item("Nombre").ToString
            Me.txtApellido.Text = dsDatos.Tables(0).Rows(0).Item("Apellido").ToString
            Me.txtUsuario.Text = dsDatos.Tables(0).Rows(0).Item("Usuario").ToString
            Me.txtClave.Text = dsDatos.Tables(0).Rows(0).Item("Clave").ToString
            Me.ddlbEstado.Text = dsDatos.Tables(0).Rows(0).Item("IdEstado").ToString
            Me.ddlbPerfil.Text = dsDatos.Tables(0).Rows(0).Item("IdPerfil").ToString

            Me.lblCant.Text = dsDatos.Tables(0).Rows.Count & " Empleados"

            Me.btnAgregar.Visible = False
            Me.btnBuscar.Visible = False
            Me.btnGuardar.Visible = False

            Me.btnModificar.Visible = True
            Me.btnCancelar.Visible = True

            Me.btnBuscarNombre.Visible = False

        End If

        Me.DeshabilitarForm()

    End Sub
End Class
