﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="Reserva.aspx.vb" Inherits="_Default" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <p>
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
    </p>
    <p>
        <br />
        <table align="center" 
            style="width: 1085px; border-style: solid; border-width: 1px; right: 100%; left: 100%;">
            <tr>
                <td style="font-size: xx-large; text-align: center;" class="style6" 
                    colspan="14">
        <table align="center" 
            
                        style="width: 642px; border-style: hidden; border-width: 1px; right: 100%; left: 100%; margin-left: 441px;">
            <tr>
               <td class="style5" width="85">
                    <asp:Button ID="btnAgregar" runat="server" Text="Agregar" Width="95px" />
                </td>
                <td class="style5">
                    <asp:Button ID="btnModificar" runat="server" Text="Modificar" Width="95px" />
                </td>
                <td class="style5" width="85">
                    <asp:Button ID="btnGuardar" runat="server" Text="Guardar" Width="95px" />
                </td>
                <td class="style5" width="85">
                    <asp:Button ID="btnCancelar" runat="server" Text="Cancelar" Width="95px" />
                </td>
                <td class="style5">
                    <asp:Button ID="btnBuscar" runat="server" Text="Buscar" Width="85px" />
                </td>
                </tr>
        </table>
                </td>
            </tr>
            <tr>
                <td style="font-size: xx-large; text-align: center;" class="style6" 
                    colspan="14">
                <asp:HiddenField ID="hfReservas" runat="server" />
                </td>
            </tr>
            <tr>
                <td style="font-size: xx-large; text-align: center;" class="style6">
                    </td>
                <td colspan="12" style="font-size: xx-large; text-align: center;" 
                    class="style7">
                    INFORMACIÓN GENERAL CLIENTE</td>
                <td class="style8">
                    </td>
            </tr>
            <tr>
                <td style="width: 207px; height: 29px;">
                </td>
                <td style="width: 226px; height: 29px;">
                    &nbsp;</td>
                <td colspan="4" style="height: 29px;">
                </td>
                <td class="style23">
                    &nbsp;</td>
                <td class="style19" colspan="5">
                </td>
                <td style="width: 202px; height: 29px;">
                </td>
            </tr>
            <tr>
                <td style="width: 207px; height: 29px;">
                    &nbsp;</td>
                <td style="width: 226px; height: 29px;">
                    &nbsp;</td>
                <td colspan="4" style="height: 29px;">
                    &nbsp;</td>
                <td class="style23">
                    &nbsp;</td>
                <td class="style34" colspan="2">
                    &nbsp;</td>
                <td class="style29" colspan="2">
                    &nbsp;</td>
                <td style="width: 129px; height: 29px;">
                    &nbsp;</td>
                <td style="width: 202px; height: 29px;">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="width: 207px; height: 34px;">
                </td>
                <td style="width: 226px; height: 34px;" class="style9">
                    Nombres:</td>
                <td colspan="6" class="style28">
                    <asp:TextBox ID="txtNombreCliente" runat="server" Height="19px" Width="424px"></asp:TextBox>
                &nbsp;<asp:Button ID="btnBuscarNombre" runat="server" Text="Buscar" Width="85px" />
                </td>
                <td colspan="2" style="height: 34px">
                    &nbsp;</td>
                <td colspan="2" style="height: 34px">
                    &nbsp;</td>
                <td style="width: 202px; height: 34px;">
                </td>
            </tr>
            <tr>
                <td style="width: 207px; height: 23px;">
                </td>
                <td style="width: 226px; height: 23px;" class="style9">
                </td>
                <td colspan="10" style="height: 23px">
                    &nbsp;</td>
                <td style="width: 202px; height: 23px;">
                </td>
            </tr>
            <tr>
                <td class="style10">
                    </td>
                <td class="style11">
                    Apellidos:</td>
                <td colspan="10" class="style12">
                    <asp:TextBox ID="txtApellidosCliente" runat="server" Width="423px"></asp:TextBox>
                </td>
                <td class="style13">
                    </td>
            </tr>
            <tr>
                <td class="style39">
                    </td>
                <td class="style40">
                    </td>
                <td colspan="10" class="style41">
                    &nbsp;</td>
                <td class="style42">
                    </td>
            </tr>
            <tr>
                <td class="style35">
                    </td>
                <td class="style36">
                    Edad:</td>
                <td colspan="5" class="style37">
                    <asp:TextBox ID="txtEdad" runat="server" Height="19px" Width="52px"></asp:TextBox>
                &nbsp;&nbsp;
                    <br />
                </td>
                <td colspan="5" class="style37">
                    &nbsp;</td>
                <td class="style38">
                    </td>
            </tr>
            <tr>
                <td style="width: 207px">
                    &nbsp;</td>
                <td style="width: 226px">
                    &nbsp;</td>
                <td colspan="10">
                    &nbsp;</td>
                <td style="width: 202px">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="width: 207px; height: 28px;">
                </td>
                <td style="width: 226px; height: 28px; text-align: right;">
                    Dirección</td>
                <td colspan="4" style="height: 28px;">
                    <asp:TextBox ID="txtDireccion" runat="server" Height="84px" Rows="5" 
                        Width="417px"></asp:TextBox>
                </td>
                <td class="style24">
                    &nbsp;</td>
                <td class="style20" colspan="2">
                </td>
                <td class="style30" colspan="2">
                </td>
                <td style="width: 129px; height: 28px;">
                </td>
                <td style="width: 202px; height: 28px;">
                </td>
            </tr>
            <tr>
                <td style="width: 207px; height: 21px;">
                </td>
                <td style="width: 226px; height: 21px;">
                </td>
                <td align="center" colspan="2" style="height: 21px;">
                    &nbsp;</td>
                <td align="center" colspan="2" style="height: 21px;">
                </td>
                <td align="center" class="style25">
                </td>
                <td align="center" class="style16" colspan="2">
                </td>
                <td align="center" class="style31" colspan="2">
                </td>
                <td align="center" style="height: 21px">
                </td>
                <td style="width: 202px; height: 21px;">
                </td>
            </tr>
            <tr>
                <td style="width: 207px; height: 21px;">
                </td>
                <td style="width: 226px; height: 21px; text-align: right;">
                    Teléfono:</td>
                <td align="center" class="style4">
                    <asp:TextBox ID="txtTelefono" runat="server"></asp:TextBox>
                </td>
                <td align="center" class="style15">
                    &nbsp;</td>
                <td align="center" style="width: 128px; height: 21px; text-align: right;">
                    Sexo:</td>
                <td align="center" class="style1">
                    <asp:TextBox ID="txtSexo" runat="server" MaxLength="1" Width="31px"></asp:TextBox>
                </td>
                <td align="center" class="style25">
                    &nbsp;</td>
                <td align="center" class="style16" colspan="2">
                </td>
                <td align="center" class="style31" colspan="2">
                </td>
                <td align="center" style="height: 21px">
                </td>
                <td style="width: 202px; height: 21px;">
                </td>
            </tr>
            <tr>
                <td style="width: 207px; height: 21px;">
                </td>
                <td style="width: 226px; height: 21px;">
                </td>
                <td align="center" colspan="2" style="height: 21px;">
                    &nbsp;</td>
                <td align="center" colspan="2" style="height: 21px;">
                    &nbsp;</td>
                <td align="center" class="style25">
                </td>
                <td align="center" class="style16" colspan="2">
                </td>
                <td align="center" class="style31" colspan="2">
                </td>
                <td align="center" style="height: 21px">
                </td>
                <td style="width: 202px; height: 21px;">
                </td>
            </tr>
            <tr>
                <td style="width: 207px; height: 26px;">
                </td>
                <td style="width: 226px; height: 26px; text-align: right;">
                    Email:</td>
                <td align="center" colspan="4" style="height: 26px; text-align: left;">
                    <asp:TextBox ID="txtEmail" runat="server" Width="418px"></asp:TextBox>
                </td>
                <td align="center" class="style26">
                </td>
                <td class="style21" colspan="2">
                </td>
                <td class="style32" colspan="2">
                </td>
                <td style="width: 129px; height: 26px;">
                </td>
                <td style="width: 202px; height: 26px;">
                </td>
            </tr>
            <tr>
                <td style="width: 207px; height: 21px;">
                </td>
                <td style="width: 226px; height: 21px;">
                </td>
                <td align="center" colspan="4" style="height: 21px; text-align: left;">
                    &nbsp;</td>
                <td align="center" class="style25">
                </td>
                <td align="center" class="style16" colspan="2">
                </td>
                <td align="center" class="style31" colspan="2">
                </td>
                <td align="center" style="height: 21px">
                </td>
                <td style="width: 202px; height: 21px;">
                </td>
            </tr>
            <tr>
                <td style="width: 207px; height: 42px;">
                </td>
                <td colspan="12" style="height: 42px; font-size: x-large; text-align: center;">
                    RESERVA</td>
            </tr>
            <tr>
                <td style="width: 207px; height: 21px;">
                </td>
                <td style="width: 226px; height: 21px; text-align: right;">
                    IdReserva:</td>
                <td align="center" colspan="4" style="height: 21px; text-align: left;">
                    <asp:TextBox ID="txtidReserva" runat="server" Width="119px"></asp:TextBox>
                </td>
                <td align="center" colspan="6" style="height: 21px;">
                    &nbsp;</td>
                <td style="width: 202px; height: 21px;">
                </td>
            </tr>
            <tr>
                <td style="width: 207px; height: 21px;">
                    &nbsp;</td>
                <td style="width: 226px; height: 21px;">
                    &nbsp;</td>
                <td align="center" colspan="4" style="height: 21px;">
                    &nbsp;</td>
                <td align="center" colspan="6" style="height: 21px;">
                    &nbsp;</td>
                <td style="width: 202px; height: 21px;">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="width: 207px; height: 21px;">
                </td>
                <td style="width: 226px; height: 21px; text-align: right;">
                    Promociones vigentes:</td>
                <td align="center" colspan="4" style="height: 21px; text-align: left;">
                    <asp:DropDownList ID="ddlPromociones" runat="server" Height="16px" 
                        Width="241px">
                    </asp:DropDownList>
                </td>
                <td align="center" class="style25">
                    &nbsp;</td>
                <td align="center" style="height: 21px;" colspan="5">
                    &nbsp;</td>
                <td style="width: 202px; height: 21px;">
                </td>
            </tr>
            <tr>
                <td class="style5">
                </td>
                <td class="style14">
                </td>
                <td align="center" colspan="4" class="style15">
                </td>
                <td align="center" class="style25">
                </td>
                <td align="center" class="style16" colspan="2">
                    &nbsp;</td>
                <td align="center" class="style31" colspan="2">
                </td>
                <td align="center" class="style15">
                    &nbsp;</td>
                <td class="style18">
                </td>
            </tr>
            <tr>
                <td style="width: 207px; height: 21px;">
                </td>
                <td style="width: 226px; height: 21px; text-align: right;">
                    Ocupantes:</td>
                <td align="center" class="style4">
                    <asp:TextBox ID="txtOcupantes" runat="server" Width="119px" 
                        style="text-align: left"></asp:TextBox>
                </td>
                <td align="center" class="style15">
                    &nbsp;</td>
                <td align="center" style="width: 128px; height: 21px;">
                    &nbsp;</td>
                <td align="center" class="style1">
                    &nbsp;</td>
                <td align="center" class="style25">
                    &nbsp;</td>
                <td align="center" class="style16" colspan="2">
                    &nbsp;</td>
                <td align="center" class="style31" colspan="2">
                </td>
                <td align="center" style="height: 21px">
                    &nbsp;</td>
                <td style="width: 202px; height: 21px;">
                </td>
            </tr>
            <tr>
                <td style="width: 207px; height: 21px;">
                </td>
                <td style="width: 226px; height: 21px;">
                </td>
                <td align="center" colspan="2" style="height: 21px;">
                    &nbsp;</td>
                <td align="center" colspan="2" style="height: 21px;">
                </td>
                <td align="center" class="style25">
                </td>
                <td align="center" class="style16" colspan="2">
                </td>
                <td align="center" class="style31" colspan="2">
                </td>
                <td align="center" style="height: 21px">
                </td>
                <td style="width: 202px; height: 21px;">
                </td>
            </tr>
            <tr>
                <td style="width: 207px; height: 21px;">
                </td>
                <td style="width: 226px; height: 21px;">
                </td>
                <td align="center" colspan="4" style="height: 21px;">
                </td>
                <td align="center" class="style25">
                </td>
                <td align="center" class="style16" colspan="2">
                    &nbsp;</td>
                <td align="center" class="style31" colspan="2">
                    &nbsp;</td>
                <td align="center" style="height: 21px">
                </td>
                <td style="width: 202px; height: 21px;">
                </td>
            </tr>
            <tr>
                <td style="width: 207px; height: 206px;">
                </td>
                <td colspan="3" style="height: 206px;">
                    &nbsp;</td>
                <td colspan="3" style="height: 206px;">
                    <asp:GridView ID="dgvReservas" runat="server" Height="50px" Width="525px" 
                        AutoGenerateColumns="False" style="text-align: center">
                    </asp:GridView>
                </td>
                <td colspan="5" style="height: 206px;">
                    &nbsp;</td>
                <td style="width: 202px; height: 206px;">
                </td>
            </tr>
            <tr>
                <td style="width: 207px; height: 35px;">
                    &nbsp;</td>
                <td style="width: 226px; height: 35px;">
                    &nbsp;</td>
                <td align="center" colspan="4" style="height: 35px;">
                    &nbsp;</td>
                <td align="center" class="style27">
                    &nbsp;</td>
                <td class="style22" colspan="2">
                    &nbsp;</td>
                <td class="style33" colspan="2">
                    &nbsp;</td>
                <td style="width: 129px; height: 35px;">
                    &nbsp;</td>
                <td style="width: 202px; height: 35px;">
                    &nbsp;</td>
            </tr>
        </table>
    </p>
</asp:Content>

<asp:Content ID="Content2" runat="server" contentplaceholderid="HeadContent">
    <style type="text/css">
        .style1
        {
            height: 21px;
            width: 29px;
        }
        .style4
        {
            height: 21px;
            width: 123px;
            text-align: left;
        }
        .style5
        {
            height: 21px;
            width: 170px;
        }
        .style6
        {
            height: 38px;
        }
        .style7
        {
            height: 38px;
        }
        .style8
        {
            width: 202px;
            height: 38px;
        }
        .style9
        {
            text-align: right;
        }
        .style10
        {
            width: 207px;
            height: 26px;
        }
        .style11
        {
            text-align: right;
            width: 226px;
            height: 26px;
        }
        .style12
        {
            height: 26px;
        }
        .style13
        {
            width: 202px;
            height: 26px;
        }
        .style14
        {
            width: 226px;
            height: 21px;
        }
        .style15
        {
            height: 21px;
        }
        .style16
        {
            width: 105px;
            height: 21px;
        }
        .style18
        {
            width: 202px;
            height: 21px;
        }
        .style19
        {
            height: 29px;
        }
        .style20
        {
            height: 28px;
            width: 105px;
        }
        .style21
        {
            height: 26px;
            width: 105px;
        }
        .style22
        {
            height: 35px;
            width: 105px;
        }
        .style23
        {
            height: 29px;
            width: 156px;
        }
        .style24
        {
            height: 28px;
            width: 156px;
        }
        .style25
        {
            height: 21px;
            width: 156px;
        }
        .style26
        {
            height: 26px;
            width: 156px;
        }
        .style27
        {
            height: 35px;
            width: 156px;
        }
        .style28
        {
            height: 34px;
        }
        .style29
        {
            height: 29px;
            width: 88px;
        }
        .style30
        {
            height: 28px;
            width: 88px;
        }
        .style31
        {
            height: 21px;
            width: 88px;
        }
        .style32
        {
            height: 26px;
            width: 88px;
        }
        .style33
        {
            height: 35px;
            width: 88px;
        }
        .style34
        {
            height: 29px;
            width: 105px;
        }
        .style35
        {
            width: 207px;
            height: 36px;
        }
        .style36
        {
            text-align: right;
            width: 226px;
            height: 36px;
        }
        .style37
        {
            height: 36px;
        }
        .style38
        {
            width: 202px;
            height: 36px;
        }
        .style39
        {
            width: 207px;
            height: 18px;
        }
        .style40
        {
            text-align: right;
            width: 226px;
            height: 18px;
        }
        .style41
        {
            height: 18px;
        }
        .style42
        {
            width: 202px;
            height: 18px;
        }
        </style>
</asp:Content>


